#include "EntityBatch.h"
#include "Camera.h"
#include "SDLMusicManager.h"
#include "DummyCircle.h"
#include "Player.h"
#include "RandomNumberManager.h"
#include "BulletEntity.h"
#include "SettingsManager.h"

EntityBatch::EntityBatch() :
	nonEssentialSpeedMod(1)
{
}


EntityBatch::~EntityBatch()
{
}

/*std::shared_ptr<CollidableEntity> EntityBatch::findOldestEntityNotInCamera(const RectangleEntity &cameraRect) {
	//std::vector<std::shared_ptr<CollidableEntity>> entitiesCopy = entities;
	//std::sort(entitiesCopy.begin(), entitiesCopy.end(), [](const std::shared_ptr<CollidableEntity> &e1, const std::shared_ptr<CollidableEntity> &e2) {
	//	return e1->createdTick < e2->createdTick;
	//});
	//std::cout << "getting called" << std::endl;
	for (int i = 0; i < entities.size(); i++) {
		if (!entities[i]->isEssentialEntity()) {
			if (!entities[i]->isPotentiallyColliding(cameraRect)) {
				return entities[i];
			}
		}
	}
	//std::cout << "rip" << std::endl;
	return nullptr;
}*/

void EntityBatch::calculatePotentialLocations() {
	for (auto it : entities) {
		it->calculatePotentialLocation();
		if (it->isEssentialEntity()) {
			it->speedMod = XYSystem(1);
		}
		else {
			it->speedMod = nonEssentialSpeedMod;
		}
		it->spriteBatch->updateSprites();
	}
	for (auto it : unCollidableEntities) {
		it->calculatePotentialLocation();
		it->speedMod = XYSystem(1);
		it->spriteBatch->updateSprites();
	}
}


void EntityBatch::doCollisions(std::shared_ptr<Camera> camera) {
	//std::cout << "starting colls" << std::endl;
	if (entities.size() < 2) return;
	for (unsigned int i = 0; i < entities.size(); i++) {
		if (!entities[i]->isEssentialEntity()) {
			if (!entities[i]->isPotentiallyColliding(camera->getCameraEntity())) {
				entities[i]->flagForDeath = true;
			}
		}
		if (entities[i]->flagForDeath) continue;
		for (unsigned int j = i + 1; j < entities.size(); j++) {
			if (entities[j]->flagForDeath) continue;
			if (entities[i]->isPotentiallyColliding(*entities[j])) {
				entities[i]->resolvePotentialCollision(*entities[j]);
			}
		}
	}
	//std::cout << "ending colls" << std::endl;
}

//bool EntityBatch::isTooFull() {
//	return entities.size() + unCollidableEntities.size() > Constants::ENTITY_COUNT_MAX;
//}

bool EntityBatch::finishMovementsAndKillFlaggedAndWhatNeedsToDie(std::shared_ptr<Camera> camera, bool killOffscreen, std::vector<std::shared_ptr<Slider>> sliders) {
	const int needsToKill = entities.size() + unCollidableEntities.size() - Constants::ENTITY_COUNT_MAX;
	int needsToKillEntities = needsToKill * entities.size() / Constants::ENTITY_COUNT_MAX;
	int needsToKillUnCollidableEntities = needsToKill * unCollidableEntities.size() / Constants::ENTITY_COUNT_MAX;
	if (needsToKill < 1) {
		needsToKillEntities = 0;
		needsToKillUnCollidableEntities = 0;
	}

	RectangleEntity cameraHitbox = camera->getCameraEntity();
	bool essentialEntityExists = false;
	for (auto it = entities.begin(); it != entities.end();) {
		if (needsToKillEntities > 0) { // There is a need to kill an entity
			if (!(*it)->isPotentiallyColliding(cameraHitbox)) { // It isnt in the camera
				if (!(*it)->flagForDeath) { // Not flagged for death
					if (!(*it)->isEssentialEntity()) { // It isnt a player
						(*it)->flagForDeath = true;
						--needsToKillEntities;
					}
				}
			}
		}
		
		if ((*it)->isEssentialEntity()) {
			if (((*it)->potentialLocation.fixInRange(camera->getBottomLeft() + (*it)->radius, camera->getTopRight() - (*it)->radius) != (*it)->potentialLocation)) {
				if (killOffscreen) {
					(*it)->flagForDeath = true;
					SDLMusicManager::getInstance()->playSound("explosion");
				}
				else {
					(*it)->potentialLocation = (*it)->potentialLocation.fixInRange(camera->getBottomLeft() + (*it)->radius, camera->getTopRight() - (*it)->radius);
				}
			}
			for (auto it2 : sliders) {
				if ((*it)->isPotentiallyColliding(*it2->getSliderDot())) {
					it2->moveSlider(**it);
				}
			}
		}
		if ((*it)->flagForDeath) { // Flagged for death
			it = entities.erase(it);
			continue;
		}

		if ((*it)->isEssentialEntity()) essentialEntityExists = true;

		(*it)->completePotentialLocation();
		++it;
	}
	for (auto it = unCollidableEntities.begin(); it != unCollidableEntities.end();) {
		if (needsToKillUnCollidableEntities > 0) { // There is a need to kill an entity
			if ((*it)->potentialLocation.fixInRange(camera->getBottomLeft(), camera->getTopRight()) != (*it)->potentialLocation) {
				if (!(*it)->flagForDeath) { // Not flagged for death
					if (!(*it)->isEssentialEntity()) { // It isnt a player
						(*it)->flagForDeath = true;
						--needsToKillUnCollidableEntities;
					}
				}
			}
		}
		if ((*it)->isEssentialEntity()) {
			if (((*it)->potentialLocation.fixInRange(camera->getBottomLeft(), camera->getTopRight()) != (*it)->potentialLocation)) {
				if (killOffscreen) {
					(*it)->flagForDeath = true;
					SDLMusicManager::getInstance()->playSound("explosion");
				}
				else {
					(*it)->potentialLocation = (*it)->potentialLocation.fixInRange(camera->getBottomLeft(), camera->getTopRight());
				}
			}
		}
		if ((*it)->flagForDeath) { // Flagged for death
			it = unCollidableEntities.erase(it);
			continue;
		}

		if ((*it)->isEssentialEntity()) essentialEntityExists = true;

		(*it)->completePotentialLocation();
		++it;
	}
	return essentialEntityExists;
}

int *EntityBatch::resolveActions(int currentScore, Difficulty diff)
{
	static int scores[4] = {0, 0, 0, 0};
	std::vector<std::shared_ptr<CollidableEntity>> entToAdd;

	for (auto it : entities) {
		if ((it->actionFlags & EntityActionFlags::CREATE_EXPLOSION) == EntityActionFlags::CREATE_EXPLOSION) {
			DummyCircle cir(it->location, it->spriteBatch->sprites[(int)PowerupType::BOMB]->size.x * it->spriteBatch->sprites[(int)PowerupType::BOMB]->sizeMod.x / 2.0, 0, XYSystem());
			for (auto it2 : entities) {
				if (it2->isEssentialEntity() || it2->flagForDeath) {
					//std::cout << "lol" << std::endl;
					continue;
				}
				if (it2->isPotentiallyColliding(cir)) {
					//std::cout << "ripppp" << std::endl;
					it2->flagForDeath = true;
				}
				else {
					//std::cout << "welp" << std::endl;
				}
			}
		}
		if ((it->actionFlags & EntityActionFlags::CREATE_SLOWMO) == EntityActionFlags::CREATE_SLOWMO) {
			DummyCircle cir(it->location, it->spriteBatch->sprites[(int)PowerupType::TIMER]->size.x * it->spriteBatch->sprites[(int)PowerupType::TIMER]->sizeMod.x / 2.0, 0, XYSystem());
			for (auto it2 : entities) {
				if (it2->isEssentialEntity() || it2->flagForDeath) {
					//std::cout << "lol" << std::endl;
					continue;
				}
				if (it2->isPotentiallyColliding(cir)) {
					//std::cout << "ripppp" << std::endl;
					it2->speedMod *= .5;
				}
				else {
					//std::cout << "welp" << std::endl;
				}
			}
		}
		if ((it->actionFlags & EntityActionFlags::REVIVE_PLAYER) == EntityActionFlags::REVIVE_PLAYER) {
			std::vector<int> playersThatDontExist;
			for (int i = 0; i < SettingsManager::getSettings().playerCount; i++) {
				playersThatDontExist.push_back(i);
			}
			for (auto it2 : entities) {
				if (it2->isEssentialEntity()) {
					if (auto c = std::dynamic_pointer_cast<Player>(it2)) {
						auto find = std::find(playersThatDontExist.begin(), playersThatDontExist.end(), c->getPlayerNumber());
						if (find != playersThatDontExist.end()) {
							find = playersThatDontExist.erase(find);
						}
					}
				}
			}
			for (auto it2 : entToAdd) {
				if (it2->isEssentialEntity()) {
					if (auto c = std::dynamic_pointer_cast<Player>(it2)) {
						auto find = std::find(playersThatDontExist.begin(), playersThatDontExist.end(), c->getPlayerNumber());
						if (find != playersThatDontExist.end()) {
							find = playersThatDontExist.erase(find);
						}
					}
				}
			}
			if (playersThatDontExist.size() > 0) {
				int playerIndexToSpawn = RandomNumberManager::getInstance()->generateRandomInt(0, playersThatDontExist.size(), RandomType::playerToRevive);
				entToAdd.push_back(std::make_shared<Player>(playersThatDontExist[playerIndexToSpawn], it->potentialLocation, true, diff));
				it->actionFlags = it->actionFlags | EntityActionFlags::PLAYER_REVIVED_SUCCESSFULLY;
			}
		}
		if ((it->actionFlags & EntityActionFlags::CREATE_BULLETS) == EntityActionFlags::CREATE_BULLETS) {
			for (int i = 0; i < Constants::GUN_DIRECTIONS; i++) {
				double rad = (double)i / (double)Constants::GUN_DIRECTIONS * 2.0 * M_PI;
				entToAdd.push_back(std::make_shared<BulletEntity>(it->location, rad));
			}
		}
		if ((it->actionFlags & EntityActionFlags::ATTACH_DEATH_SCORE) == EntityActionFlags::ATTACH_DEATH_SCORE) {
			if (it->isEssentialEntity()) {
				if (auto c = std::dynamic_pointer_cast<Player>(it)) {
					scores[c->getPlayerNumber()] = currentScore;
				}
			}
		}
		/*
		if ((it->actionFlags & EntityActionFlags::CREATE_REPEL) == EntityActionFlags::CREATE_REPEL) {
			DummyCircle cir(it->location, Constants::COMET_RADIUS * 10, 0, XYSystem());
			for (auto it2 : entities) {
				if (it2->isEssentialEntity() || it2->flagForDeath) {
					//std::cout << "lol" << std::endl;
					continue;
				}
				if (it2->isPotentiallyColliding(cir)) {
					//std::cout << "ripppp" << std::endl;
					//XYSystem accel(it->mass / it2->potentialLocation.distanceToSquared(it->potentialLocation) * 100);
					//it2->speed += accel * 1.0 / (double)Constants::TICK_RATE;
					//it2->potentialLocation = it2->location + it2->speed;
					XYSystem diffVector = it->potentialLocation - it2->potentialLocation;
					diffVector.x = 1.0 / diffVector.x;
					diffVector.y = 1.0 / diffVector.y;
					diffVector *= -.5;

					it2->speed += diffVector;

					int a = 0;
				}
				else {
					//std::cout << "welp" << std::endl;
				}
			}
		}
		*/
		it->clearAction();
	}
	for (auto it : entToAdd) {
		entities.push_back(it);
	}
	return scores;
}

