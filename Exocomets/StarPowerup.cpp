#include "StarPowerup.h"
#include "RandomNumberManager.h"
#include "MainGame.h"

StarPowerup::StarPowerup(std::shared_ptr<Camera> camera) :
	StarPowerup(camera, Difficulty::MAIN_MENU)
{}

StarPowerup::StarPowerup(std::shared_ptr<Camera> camera, Difficulty diff) :
	Powerup(camera, PowerupType::STAR, diff),
	isTwinkling(false)
{
	generateNextTwinkleTime();
}


StarPowerup::~StarPowerup()
{
}

void StarPowerup::calculateCustomPotentialLocation()
{
	checkTwinkle();
}

void StarPowerup::checkTwinkle() {
	if (MainGame::getTickCount() >= _nextTwinkleTick) {
		isTwinkling = !isTwinkling;
		generateNextTwinkleTime();
	}
}

void StarPowerup::generateNextTwinkleTime() {
	double mod;
	if (isTwinkling) {
		mod = RandomNumberManager::getInstance()->generateRandomDouble(Constants::STAR_SIZE_TWINKLE_MOD_MIN, Constants::STAR_SIZE_TWINKLE_MOD_MAX, RandomType::starTwinkleSize);
		_nextTwinkleTick = MainGame::getTickCount() + RandomNumberManager::getInstance()->generateRandomInt(Constants::STAR_TWINKLE_TIME_MIN, Constants::STAR_TWINKLE_TIME_MAX, RandomType::starTwinkleTime);
	}
	else {
		mod = 1;
		_nextTwinkleTick = MainGame::getTickCount() + RandomNumberManager::getInstance()->generateRandomInt(Constants::STAR_NONTWINKLE_TIME_MIN, Constants::STAR_NONTWINKLE_TIME_MAX, RandomType::starNonTwinkleTime);
	}
	spriteBatch->sprites[0]->sizeMod = XYSystem(mod);
}

/*void Shield::resolvePotentialCollision(CollidableEntity &other) {
other.resolvePotentialCollision(*this);
}

void Shield::resolvePotentialCollision(Comet &other) {
other.resolvePotentialCollision(*this);
}

void Shield::resolvePotentialCollision(Star &other) {}
*/

