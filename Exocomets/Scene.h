#pragma once
#include "stdafx.h"
#include "SDLRenderer.h"
#include "Camera.h"
#include "Comet.h"
#include "EntityBatch.h"
#include "Slider.h"
#include "KeyOptionEntity.h"

enum class SceneAction {
	NOTHING = 0,
	DESTROY_SCENE,
	DESTROY_TWO_SCENES,
	CREATE_LOSE_SCENE,
	CREATE_GAME_SCENE,
	CREATE_LEVEL_SELECT_SCENE,
	CREATE_OPTIONS_SCENE,
	CREATE_CREDITS_SCENE,
	CREATE_POWERUP_UNLOCK_SCENE,
	CREATE_POWERUP_UNLOCK_AND_LOSE_SCENE,
	CREATE_CREDITS_AND_LOSE_SCENE,
	CREATE_LEADERBOARDS_SCENE
};

class Scene
{
public:
	Scene(std::shared_ptr<SDLRenderer> rnd, Difficulty diff);
	~Scene();
	const std::string name;
	virtual void init();
	void everyTick();
	virtual void deinit();
	Uint8 colorMod[4];
	void render();
	std::shared_ptr<Camera> camera;
	SceneAction action = SceneAction::NOTHING;
	bool eatKeys;
	bool isActive = false;

	std::vector<std::shared_ptr<EntityBatch>> entityBatches;
	std::vector<std::shared_ptr<SpriteBatch>> spriteBatches;
	std::vector<std::shared_ptr<Slider>> sliders;

	Difficulty difficulty;

	int playerScores[4];

	virtual bool doBackground();

protected:
	virtual void customEveryTick();
	virtual bool killOffscreen();
	int _createdTick;
	int _score;

private:
	std::shared_ptr<SDLRenderer> _rnd;

};

