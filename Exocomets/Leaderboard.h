#pragma once
#include "stdafx.h"
#include "Constants.h"

enum class LeaderboardType {
	TOP_X = 0,
	GLOBAL_SURROUND,
	FRIENDS
};

struct LeaderboardEntry {
	std::string steamName{ "Error!" };
	int scores[6] = { 0, 0, 0, 0, 0, 0 };
	int placing = WINT_MAX;
	bool isPlayer = false;
	// If its equal to the one above
	bool thinLine = false;
};

class Leaderboard
{
public:
	Leaderboard(Difficulty sortByDifficulty, LeaderboardType type);
	~Leaderboard();
	LeaderboardType type;

	void addEntry(LeaderboardEntry entry);

	std::vector<LeaderboardEntry> &getEntries();

	Difficulty getDiff();

private:
	std::vector<LeaderboardEntry> _entries;
	Difficulty _sortByDifficulty;
};

