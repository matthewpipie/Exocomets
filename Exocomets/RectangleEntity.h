#pragma once
#include "CollidableEntity.h"
class RectangleEntity :
	public CollidableEntity
{
public:
	RectangleEntity(XYSystem location, std::vector<XYSystem> relativeHitbox, double mass, XYSystem speed);
	~RectangleEntity();
	virtual void resolvePotentialCollision(CollidableEntity &other) override;
};

