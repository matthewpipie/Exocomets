#include "BackgroundScene.h"

BackgroundScene::BackgroundScene(std::shared_ptr<SDLRenderer> rnd, Difficulty diff, std::shared_ptr<Camera> sceneCamera) :
	Scene(rnd, diff)
{
	camera = sceneCamera;
}

BackgroundScene::~BackgroundScene()
{
}

void BackgroundScene::init()
{
}

void BackgroundScene::onSceneChange(std::shared_ptr<Scene> oldScene, std::shared_ptr<Scene> newScene)
{
	camera = newScene->camera;
	onSceneChangeCustom(oldScene, newScene);
}

void BackgroundScene::onSceneChangeCustom(std::shared_ptr<Scene> oldScene, std::shared_ptr<Scene> newScene)
{
}

void BackgroundScene::deinit()
{
}

void BackgroundScene::customEveryTick()
{
}
