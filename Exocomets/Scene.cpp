#include "Scene.h"
#include "SaveDataManager.h"
#include "SteamManager.h"
#include "MainGame.h"
#include "SettingsManager.h"

Scene::Scene(std::shared_ptr<SDLRenderer> rnd, Difficulty diff) :
	_rnd(rnd),
	camera(std::make_shared<Camera>()),
	_createdTick(MainGame::getTickCount()),
	difficulty(diff),
	action(SceneAction::NOTHING),
	eatKeys(true),
	_score(0)
{
	entityBatches.push_back(std::make_shared<EntityBatch>());
	for (int i = 0; i < sizeof(colorMod) / sizeof(colorMod[0]); i++) {
		colorMod[i] = 255;
	}
	camera->setZoom(Constants::ZOOM_LEVELS[(int)difficulty]);
}


Scene::~Scene()
{
}

void Scene::render() {

	for (auto it : spriteBatches) {
		_rnd->renderSpriteBatch(colorMod, camera, it);
	}
	for (auto it : sliders) {
		_rnd->renderEntity(colorMod, camera, it->getSliderDot());
		_rnd->renderSpriteBatch(colorMod, camera, it->getSliderSpriteBatch());
	}
	for (auto it : entityBatches) {
		for (auto it3 : it->unCollidableEntities) {
			_rnd->renderEntity(colorMod, camera, it3);
		}
		for (auto it2 : it->entities) {
			_rnd->renderEntity(colorMod, camera, it2);
		}
	}
}

void Scene::init()
{
}

void Scene::everyTick() {

	entityBatches[0]->calculatePotentialLocations();

	int *scores = entityBatches[0]->resolveActions(_score, difficulty);
	for (int i = 0; i < 4; i++) {
		if (scores[i] == _score) {
			playerScores[i] = _score;
		}
	}

	entityBatches[0]->doCollisions(camera);

	scores = entityBatches[0]->resolveActions(_score, difficulty);
	for (int i = 0; i < 4; i++) {
		if (scores[i] == _score) {
			playerScores[i] = _score;
		}
	}


	bool customScene = false;
	if (!entityBatches[0]->finishMovementsAndKillFlaggedAndWhatNeedsToDie(camera, killOffscreen(), sliders)) {
		if ((double)_score / Constants::TICK_RATE * Constants::SCORE_MULTIPLIER >= Constants::SCORE_LIMIT) {
			if ((double)_score / Constants::TICK_RATE * Constants::SCORE_MULTIPLIER > SaveDataManager::getCurrentSaveData().highScores[(int)difficulty - 1]) {
				if (SaveDataManager::getCurrentSaveData().highScores[(int)difficulty - 1] < Constants::SCORE_LIMIT) {
					if (difficulty != Difficulty::MAIN_MENU && difficulty < Difficulty::LEVEL_4) {
						SaveDataManager::submitScore(difficulty, (int)((double)_score / Constants::TICK_RATE * Constants::SCORE_MULTIPLIER));
						action = SceneAction::CREATE_POWERUP_UNLOCK_AND_LOSE_SCENE;
						eatKeys = false;
						customScene = true;
					}
					else if (difficulty == Difficulty::LEVEL_6) {
						action = SceneAction::CREATE_CREDITS_AND_LOSE_SCENE;
						eatKeys = false;
						customScene = true;
					}
					switch (difficulty)
					{
					case Difficulty::LEVEL_1:
						SteamManager::getInstance()->progressAchievement(Achievement::BEAT_LVL_1);
						break;
					case Difficulty::LEVEL_2:
						SteamManager::getInstance()->progressAchievement(Achievement::BEAT_LVL_2);
						break;
					case Difficulty::LEVEL_3:
						SteamManager::getInstance()->progressAchievement(Achievement::BEAT_LVL_3);
						break;
					case Difficulty::LEVEL_4:
						SteamManager::getInstance()->progressAchievement(Achievement::BEAT_LVL_4);
						break;
					case Difficulty::LEVEL_5:
						SteamManager::getInstance()->progressAchievement(Achievement::BEAT_LVL_5);
						break;
					case Difficulty::LEVEL_6:
						SteamManager::getInstance()->progressAchievement(Achievement::BEAT_LVL_6);
						break;
					default:
						break;
					}
				}
			}
			if (SettingsManager::getSettings().playerCount == 4) {
				SteamManager::getInstance()->progressAchievement(Achievement::BEAT_4P_GAME);
			}
		}
		if (!customScene) { action = SceneAction::CREATE_LOSE_SCENE; eatKeys = false; }
	}

	camera->keepFollowingFollowEntity();

	customEveryTick();

	//if (SettingsManager::getSettings().starCount != 0) {
	//	if (MainGame::getTickCount() % (((int)Constants::TICK_RATE - SettingsManager::getSettings().starCount * 2) * 2) == 0) {
	//		entityBatches[0]->unCollidableEntities.push_back(std::make_shared<Star>(camera)));
	//	}
	//}
}

void Scene::deinit()
{
}

bool Scene::doBackground()
{
	return true;
}

void Scene::customEveryTick()
{
}

bool Scene::killOffscreen() {
	return false;
}
