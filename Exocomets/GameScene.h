#pragma once
#include "Scene.h"
class GameScene :
	public Scene
{
public:
	GameScene(std::shared_ptr<SDLRenderer> rnd);
	GameScene(std::shared_ptr<SDLRenderer> rnd, Difficulty diff);
	GameScene(std::shared_ptr<SDLRenderer> rnd, Difficulty diff, bool endWithoutPlayers);
	~GameScene();
	void init() override;
	void deinit() override;

	bool doBackground() override;
protected:
	bool killOffscreen() override;
private:
	void customEveryTick() override;
	void addComet();
	void updateScoreSprites();

	bool _powerupsToSpawn[(int)PowerupType::LAST];
	void tryToSpawn(PowerupType powerup);

	int generateFirework(bool first);
	int _nextFirework;
	int _numOfFireworksCompleted;
	int _numOfFireworksTotal;

};

