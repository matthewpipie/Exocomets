#ifdef __APPLE__
#include "MacManager.h"
#include <mach-o/dyld.h>
std::string MacManager::getPathOfExecutableWithSlash()
{
	char buff[9999];
	uint32_t len = 9999;
	_NSGetExecutablePath(buff, &len);
	std::string path(buff);
	return path.substr(0, path.find_last_of("/") + 1);
}

#endif