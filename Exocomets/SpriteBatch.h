#pragma once
#include "stdafx.h"
#include "Sprite.h"
class SpriteBatch
{
public:
	SpriteBatch();
	~SpriteBatch();
	std::vector<std::shared_ptr<Sprite>> sprites;
	Uint8 RGBAColorMod[4];
	XYSystem location;
	Direction rotation;
	int zindex; // 1 = def, 0 = errors, 2+ = paralax
	void updateSprites();
};

