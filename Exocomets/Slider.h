#pragma once
#include "RectangleEntity.h"
#include "SpriteBatch.h"
class Slider
{
public:
	Slider(double minValue, double maxValue, XYSystem centralLoc, double size, bool isInt);
	Slider(double minValue, double maxValue, XYSystem centralLoc, double size, bool isInt, bool invis);
	~Slider();
	std::shared_ptr<RectangleEntity> getSliderDot();
	std::vector<XYSystem> getSliderDotHitbox();
	void moveSlider(CollidableEntity &ent);
	double getValue();
	void setValue(double newVal);
	std::shared_ptr<Sprite> getDotSprite();
	std::shared_ptr<SpriteBatch> getSliderSpriteBatch();
private:
	bool _isInt;
	bool _invis;
	XYSystem _dotLocation;
	const double _minValue;
	const double _maxValue;
	double _size;
	XYSystem _location;
};

