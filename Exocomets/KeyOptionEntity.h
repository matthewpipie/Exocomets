#pragma once
#include "CollidableEntity.h"
class KeyOptionEntity :
	public CollidableEntity
{
public:
	KeyOptionEntity(XYSystem loc);
	void resolvePotentialCollision(CollidableEntity &other) override;
	void resolvePotentialCollision(Player &other) override;
	void calculateCustomPotentialLocation() override;
	~KeyOptionEntity();

	int value;
	bool isActive = true;

private:
	bool _wantsInput;
	bool grace = false;
	bool didCollideLastFrame = false;
};

