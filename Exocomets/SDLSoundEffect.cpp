#include "SDLSoundEffect.h"
#include "ErrorHandler.h"



SDLSoundEffect::SDLSoundEffect(std::string name, std::string path) :
	NAME(name),
	PATH(path),
	sdlMixChunk(generateMixChunk(path))
{
}


SDLSoundEffect::~SDLSoundEffect()
{
	if (sdlMixChunk != nullptr) {
		Mix_FreeChunk(sdlMixChunk);
	}
}


Mix_Chunk *SDLSoundEffect::generateMixChunk(std::string path) {
	Mix_Chunk *temp = Mix_LoadWAV(path.c_str());
	if (temp == nullptr) {
		ErrorHandler::error(ErrorCode::SDL_SOUND_EFFECT, path);
	}
	return temp;
}
