#include "Comet.h"
#include "RandomNumberManager.h"
#include "MainGame.h"

Comet::Comet(XYSystem location, XYSystem speed, Difficulty cometDifficulty, double radius, double mass) :
	_cometDifficulty(cometDifficulty),
	CollidableEntity(location, radius, mass, speed)
{
	addCometSprite();
}

Comet::Comet(std::shared_ptr<Camera> camera, Difficulty cometDifficulty, double radius, double mass, XYSystem speedMP) :
	_cometDifficulty(cometDifficulty),
	CollidableEntity(XYSystem(0,0), radius, mass, XYSystem(0,0))
{
	location = generateOffscreenLocation(camera);
	speed = generateCometSpeed(camera, cometDifficulty, speedMP);
	potentialLocation = location + speed;
	addCometSprite();
}


Comet::~Comet()
{
}

double Comet::getPostCollisionFraction(double v1, double v2, double m1, double m2, double t1, double t2, double p) {
	return (v1 * std::cos(t1 - p) * (m1 - m2) + 2 * m2 * v2 * std::cos(t2 - p)) / (m1 + m2);
}
double Comet::getPostCollisionXSpeed(double v1, double v2, double m1, double m2, double t1, double t2, double p) {
	return getPostCollisionFraction(v1, v2, m1, m2, t1, t2, p) * std::cos(p) + v1 * std::sin(t1 - p) * std::cos(p + PI / 2.0);
}
double Comet::getPostCollisionYSpeed(double v1, double v2, double m1, double m2, double t1, double t2, double p) {
	return getPostCollisionFraction(v1, v2, m1, m2, t1, t2, p) * std::sin(p) + v1 * std::sin(t1 - p) * std::sin(p + PI / 2.0);
}

void Comet::resolvePotentialCollision(Comet &other) {
	Comet &comet1 = *this;
	Comet &comet2 = other;
//	int frameCount = MainGame::getTickCount();
	//std::cout << "coll" << std::endl;

	/*double i = comet1.speed.x - comet2.speed.x;
	double j = comet1._lastCollisionPosition.x - comet1.speed.x * (comet1._lastCollisionTick) - (comet2._lastCollisionPosition.x - comet2.speed.x * (comet2._lastCollisionTick));
	double k = comet1.speed.y - comet2.speed.y;
	double l = comet1._lastCollisionPosition.y - comet1.speed.y * (comet1._lastCollisionTick) - (comet2._lastCollisionPosition.y - comet2.speed.y * (comet2._lastCollisionTick));*/
	double i = std::pow(comet1.radius + comet2.radius, 2.0);
	double j = comet1.location.x - comet2.location.x;
	double k = comet1.speed.x * comet1.speedMod.x - comet2.speed.x * comet2.speedMod.x;
	double l = comet1.location.y - comet2.location.y;
	double m = comet1.speed.y * comet1.speedMod.y - comet2.speed.y * comet2.speedMod.y;

	double a = k * k + m * m;//std::pow(i, 2.0) + std::pow(k, 2.0);
	double b = 2.0 * (j * k + l * m);//2.0 * (i * j + k * l);
	double c = j * j + l * l - i;//std::pow(j, 2.0) + std::pow(l, 2.0) - std::pow(s, 2.0);

	double discriminant = std::pow(b, 2.0) - 4.0 * a * c;
	if (discriminant < 0) {
		std::cout << "FATAL ERROR" << std::endl;
		return;
		//discriminant *= -1;
	}
	double collisionFramePercentage;
	double collisionFramePercentage1 = (-1.0 * b - std::pow(discriminant, 0.5)) / (2.0 * a);
	double collisionFramePercentage2 = (-1.0 * b + std::pow(discriminant, 0.5)) / (2.0 * a);
	//std::cout << "cf1 =" << collisionFramePercentage1 << ", cf2 =" << collisionFramePercentage2 << std::endl;
	if (std::abs(collisionFramePercentage1) < std::abs(collisionFramePercentage2)) {
		collisionFramePercentage = collisionFramePercentage1;
	}
	else {
		collisionFramePercentage = collisionFramePercentage2;
	}
	//std::cout << "currently on frame " << frameCount << ", collision happened on frame " << collisionFramePercentage + frameCount << std::endl;

	XYSystem comet1ShouldCollide;
	XYSystem comet2ShouldCollide;

	comet1ShouldCollide.x = comet1.speed.x * comet1.speedMod.x * collisionFramePercentage + comet1.location.x;
	comet1ShouldCollide.y = comet1.speed.y * comet1.speedMod.y * collisionFramePercentage + comet1.location.y;
	comet2ShouldCollide.x = comet2.speed.x * comet2.speedMod.x * collisionFramePercentage + comet2.location.x;
	comet2ShouldCollide.y = comet2.speed.y * comet2.speedMod.y * collisionFramePercentage + comet2.location.y;

	comet1.potentialLocation = comet1ShouldCollide;
	comet2.potentialLocation = comet2ShouldCollide;

	//Now that the comets are in the right place, do the collision
	double collAngle = std::atan2(comet2.potentialLocation.y - comet1.potentialLocation.y, comet2.potentialLocation.x - comet1.potentialLocation.x);
	double comet1Speed = std::pow(std::pow(comet1.speed.x * comet1.speedMod.x, 2.0) + std::pow(comet1.speed.y * comet1.speedMod.y, 2.0), 0.5);
	double comet2Speed = std::pow(std::pow(comet2.speed.x * comet2.speedMod.x, 2.0) + std::pow(comet2.speed.y * comet2.speedMod.y, 2.0), 0.5);
	double comet1Angle = std::atan2(comet1.speed.y, comet1.speed.x);
	double comet2Angle = std::atan2(comet2.speed.y, comet2.speed.x);

	double finalX1Speed = getPostCollisionXSpeed(comet1Speed, comet2Speed, comet1.mass, comet2.mass, comet1Angle, comet2Angle, collAngle) / comet1.speedMod.x;
	double finalY1Speed = getPostCollisionYSpeed(comet1Speed, comet2Speed, comet1.mass, comet2.mass, comet1Angle, comet2Angle, collAngle) / comet1.speedMod.y;
	double finalX2Speed = getPostCollisionXSpeed(comet2Speed, comet1Speed, comet2.mass, comet1.mass, comet2Angle, comet1Angle, collAngle) / comet2.speedMod.x;
	double finalY2Speed = getPostCollisionYSpeed(comet2Speed, comet1Speed, comet2.mass, comet1.mass, comet2Angle, comet1Angle, collAngle) / comet2.speedMod.y;

	comet1.speed.x = finalX1Speed;
	comet1.speed.y = finalY1Speed;
	comet2.speed.x = finalX2Speed;
	comet2.speed.y = finalY2Speed;

	/*
	comet1.potentialLocation.x += (1.0 - collisionFramePercentage) * comet1.speed.x * comet1.speedMod.x;
	comet1.potentialLocation.y += (1.0 - collisionFramePercentage) * comet1.speed.y * comet1.speedMod.y;
	comet2.potentialLocation.x += (1.0 - collisionFramePercentage) * comet2.speed.x * comet2.speedMod.x;
	comet2.potentialLocation.y += (1.0 - collisionFramePercentage) * comet2.speed.y * comet2.speedMod.y;

	comet1.location = comet1.potentialLocation - comet1.speed.multiplyComponents(comet1.speedMod);
	comet2.location = comet2.potentialLocation - comet2.speed.multiplyComponents(comet2.speedMod);
*/
	comet1.location = comet1ShouldCollide - comet1.speed.multiplyComponents(comet1.speedMod) * collisionFramePercentage;
	comet2.location = comet2ShouldCollide - comet2.speed.multiplyComponents(comet2.speedMod) * collisionFramePercentage;

	comet1.calculatePotentialLocation();
	comet2.calculatePotentialLocation();
}

void Comet::resolvePotentialCollision(Player &other) {
	other.resolvePotentialCollision(*this);
}
void Comet::resolvePotentialCollision(CollidableEntity &other) {
	other.resolvePotentialCollision(*this);
}

XYSystem Comet::generateCometSpeed(std::shared_ptr<Camera> camera, Difficulty cometDifficulty, XYSystem speedMP) {
	/*if (InputManager::getInstance()->isKeyDown(SDLK_RSHIFT)) {
		XYSystem result;
		double angle = RandomNumberManager::getInstance()->generateRandomDouble(0, M_PI / 2.0, RandomType::speedY) + M_PI / 4.0;
		if (location.y > camera->getTopRight().y) {
			angle += M_PI;
		}
		else if (location.x < camera->getBottomLeft().x) {
			angle -= M_PI / 2.0;
		}
		else if (location.x > camera->getTopRight().x) {
			angle += M_PI / 2.0;
		}
		double maxDist = Constants::COMET_SPEED_MAX[(int)cometDifficulty] * std::sqrt(2);
		double minDist = Constants::COMET_SPEED_MIN[(int)cometDifficulty] * std::sqrt(2);;
		double dist = RandomNumberManager::getInstance()->generateRandomDouble(maxDist, minDist, RandomType::speedX) * Constants::COMET_SPEED_MULTIPLIER[(int)cometDifficulty];
		result.x = std::cos(angle) * dist;
		result.y = std::sin(angle) * dist;
		return result.multiplyComponents(speedMP);
	}*/
	XYSystem result;
	result.x = RandomNumberManager::getInstance()->generateRandomDouble(Constants::COMET_SPEED_MIN[(int)cometDifficulty], Constants::COMET_SPEED_MAX[(int)cometDifficulty], RandomType::speedX) * Constants::COMET_SPEED_MULTIPLIER[(int)cometDifficulty];
	result.y = RandomNumberManager::getInstance()->generateRandomDouble(Constants::COMET_SPEED_MIN[(int)cometDifficulty], Constants::COMET_SPEED_MAX[(int)cometDifficulty], RandomType::speedY) * Constants::COMET_SPEED_MULTIPLIER[(int)cometDifficulty];
	result.x *= RandomNumberManager::getInstance()->generateRandomInt(0, 2, RandomType::isSpeedXNegative) == 0 ? -1 : 1;
	result.y *= RandomNumberManager::getInstance()->generateRandomInt(0, 2, RandomType::isSpeedYNegative) == 0 ? -1 : 1;
	if ((location.x > camera->getTopRight().x && result.x > 0) || (location.x < camera->getBottomLeft().x && result.x < 0)) {
		result.x *= -1;
	}
	else if ((location.y > camera->getTopRight().y && result.y > 0) || (location.y < camera->getBottomLeft().y && result.y < 0)) {
		result.y *= -1;
	}
	return result.multiplyComponents(speedMP);
}

XYSystem Comet::generateOffscreenLocation(std::shared_ptr<Camera> camera) {
	int xOrYFirst = RandomNumberManager::getInstance()->generateRandomInt(0, 2, RandomType::generateXOrY);
	XYSystem ret;
	ret.x = RandomNumberManager::getInstance()->generateRandomDouble(camera->getBottomLeft().x, camera->getTopRight().x, RandomType::locationX);
	ret.y = RandomNumberManager::getInstance()->generateRandomDouble(camera->getBottomLeft().y, camera->getTopRight().y, RandomType::locationY);
	if (xOrYFirst == 0) {
		//generate x first
		if (ret.y < camera->getCameraCenter().y) {
			ret.y = camera->getBottomLeft().y - radius;
		}
		else {
			ret.y = camera->getTopRight().y + radius;
		}
	}
	else {
		if (ret.x < camera->getCameraCenter().x) {
			ret.x = camera->getBottomLeft().x - radius;
		}
		else {
			ret.x = camera->getTopRight().x + radius;
		}
	}
	return ret;
}

void Comet::addCometSprite() {
	spriteBatch->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::COMET_PATH_PREFIX + Constants::COMET_PATH + Constants::COMET_PATH_SUFFIX, XYSystem(radius * 2)));
		double initRot = RandomNumberManager::getInstance()->generateRandomDouble(-M_PI, M_PI, RandomType::powerupRotation);
		spriteBatch->sprites.back()->rotation = initRot;
}
