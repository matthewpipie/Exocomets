#pragma once
#include "stdafx.h"
#include "Entity.h"
#include "SpriteBatch.h"
#include "Camera.h"
class SDLRenderer
{
public:
	SDLRenderer(SDL_Renderer* renderer);
	~SDLRenderer();
	void renderEntity(Uint8 extraColorMod[4], std::shared_ptr<Camera> camera, std::shared_ptr<Entity> entity);
	void renderSpriteBatch(Uint8 extraColorMod[4], std::shared_ptr<Camera> camera, std::shared_ptr<SpriteBatch> spriteBatch);
private:
	SDL_Renderer *_renderer;
	void renderSprite(std::shared_ptr<Camera> camera, XYSystem centralLoc, XYSystem size, Direction rotation, bool horizontalFlip, bool verticalFlip, std::string filepath, bool renderFilepathNameInsteadOfFile, Uint8 RGBAColorMod[4], Direction colorModD, bool scaleWithCamera, int zindex);
	void renderSpriteBatch(Uint8 extraColorMod[4], std::shared_ptr<Camera> camera, std::shared_ptr<SpriteBatch> spriteBatch, XYSystem spriteBatchLocation);
};

