#include "LeaderboardsScene.h"
#include "SDLTextCreator.h"
#include "SettingsManager.h"
#include "InputManager.h"
#include "SDLTextCreator.h"
#include "SDLMusicManager.h"


LeaderboardsScene::LeaderboardsScene(std::shared_ptr<SDLRenderer> rnd) :
	Scene(rnd, Difficulty::MAIN_MENU),
	_selectedType(LeaderboardType::GLOBAL_SURROUND),
	_loading(true),
	_selectedDiff(Difficulty::LEVEL_1)
{
	entityBatches.push_back(std::make_shared<EntityBatch>());
	spriteBatches.push_back(std::make_shared<SpriteBatch>());
	spriteBatches.push_back(std::make_shared<SpriteBatch>());
	spriteBatches.push_back(std::make_shared<SpriteBatch>());
	spriteBatches.push_back(std::make_shared<SpriteBatch>());


	entityBatches[1]->entities.push_back(std::make_shared<RectangleEntity>(XYSystem(90, 990), SDLTextCreator::getTextSize("Back", Constants::REGULAR_HEIGHT).getSizeVector(), 0, 0));
	entityBatches[1]->entities[0]->spriteBatch->sprites.push_back(std::make_shared<Sprite>("Back", XYSystem(), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, true));

	entityBatches[1]->entities.push_back(std::make_shared<RectangleEntity>(XYSystem(1700, 940), SDLTextCreator::getTextSize(("Top " + std::to_string(Constants::LEADERBOARD_ENTRY_COUNT) + " Global").c_str(), Constants::REGULAR_HEIGHT).getSizeVector(), 0, 0));
	entityBatches[1]->entities[1]->spriteBatch->sprites.push_back(std::make_shared<Sprite>(("Top " + std::to_string(Constants::LEADERBOARD_ENTRY_COUNT) + " Global").c_str(), XYSystem(), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::DIM_COLOR_MENU, true));

	entityBatches[1]->entities.push_back(std::make_shared<RectangleEntity>(XYSystem(1700, 990), SDLTextCreator::getTextSize("Global", Constants::REGULAR_HEIGHT).getSizeVector(), 0, 0));
	entityBatches[1]->entities[2]->spriteBatch->sprites.push_back(std::make_shared<Sprite>("Global", XYSystem(), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::DIM_COLOR_MENU, true));

	entityBatches[1]->entities.push_back(std::make_shared<RectangleEntity>(XYSystem(1700, 1040), SDLTextCreator::getTextSize("Friends", Constants::REGULAR_HEIGHT).getSizeVector(), 0, 0));
	entityBatches[1]->entities[3]->spriteBatch->sprites.push_back(std::make_shared<Sprite>("Friends", XYSystem(), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::DIM_COLOR_MENU, true));

	spriteBatches[1]->sprites.push_back(std::make_shared<Sprite>("Tap on a level to sort by it.", XYSystem(960, 930), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::DIM_COLOR_MENU, true));
	spriteBatches[1]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::DOT_PATH_PREFIX + Constants::DOT_PATH + Constants::DOT_PATH_SUFFIX, XYSystem(960, 900), XYSystem(1920, 8), Direction(), false, false, Constants::FULL_COLOR, true));

	int ind;
	switch (_selectedType)
	{
	case LeaderboardType::TOP_X:
		ind = 1;
		break;
	case LeaderboardType::GLOBAL_SURROUND:
		ind = 2;
		break;
	case LeaderboardType::FRIENDS:
		ind = 3;
		break;
	}
	for (int i = 0; i < 4; i++) {
		entityBatches[1]->entities[ind]->spriteBatch->sprites[0]->RGBAColorMod[i] = Constants::FULL_COLOR[i];
	}
}


LeaderboardsScene::~LeaderboardsScene()
{
}

void LeaderboardsScene::init() {
	SDLMusicManager::getInstance()->playMusicIfNotPlaying(Constants::MUSIC_NAMES[(int)difficulty]);
	entityBatches[0]->entities.push_back(std::make_shared<Player>(0, XYSystem(960, 990)));

	loadScoreboard();
}

void LeaderboardsScene::deinit()
{
	SteamManager::getInstance()->stopLoadingLB();
}

bool LeaderboardsScene::doBackground()
{
	return false;
}

void LeaderboardsScene::customEveryTick()
{
	entityBatches[1]->entities[0]->calculatePotentialLocation();
	entityBatches[1]->entities[1]->calculatePotentialLocation();
	entityBatches[1]->entities[2]->calculatePotentialLocation();
	entityBatches[1]->entities[3]->calculatePotentialLocation();
	if (SettingsManager::getSettings().controls[0][0] != -1 || InputManager::getInstance()->isMouseDown()) {
		if (entityBatches[1]->entities[0]->isPotentiallyColliding(*entityBatches[0]->entities[0])) {
			action = SceneAction::DESTROY_SCENE;
		}
		if (entityBatches[1]->entities[1]->isPotentiallyColliding(*entityBatches[0]->entities[0])) {
			if (_selectedType != LeaderboardType::TOP_X) {
				_selectedType = LeaderboardType::TOP_X;
				for (int i = 0; i < 4; i++) {
					entityBatches[1]->entities[1]->spriteBatch->sprites[0]->RGBAColorMod[i] = Constants::FULL_COLOR[i];
					entityBatches[1]->entities[2]->spriteBatch->sprites[0]->RGBAColorMod[i] = Constants::DIM_COLOR_MENU[i];
					entityBatches[1]->entities[3]->spriteBatch->sprites[0]->RGBAColorMod[i] = Constants::DIM_COLOR_MENU[i];
				}
				loadScoreboard();
			}
		}
		if (entityBatches[1]->entities[2]->isPotentiallyColliding(*entityBatches[0]->entities[0])) {
			if (_selectedType != LeaderboardType::GLOBAL_SURROUND) {
				_selectedType = LeaderboardType::GLOBAL_SURROUND;
				for (int i = 0; i < 4; i++) {
					entityBatches[1]->entities[2]->spriteBatch->sprites[0]->RGBAColorMod[i] = Constants::FULL_COLOR[i];
					entityBatches[1]->entities[1]->spriteBatch->sprites[0]->RGBAColorMod[i] = Constants::DIM_COLOR_MENU[i];
					entityBatches[1]->entities[3]->spriteBatch->sprites[0]->RGBAColorMod[i] = Constants::DIM_COLOR_MENU[i];
				}
				loadScoreboard();
			}
		}
		if (entityBatches[1]->entities[3]->isPotentiallyColliding(*entityBatches[0]->entities[0])) {
			if (_selectedType != LeaderboardType::FRIENDS) {
				_selectedType = LeaderboardType::FRIENDS;
				for (int i = 0; i < 4; i++) {
					entityBatches[1]->entities[3]->spriteBatch->sprites[0]->RGBAColorMod[i] = Constants::FULL_COLOR[i];
					entityBatches[1]->entities[2]->spriteBatch->sprites[0]->RGBAColorMod[i] = Constants::DIM_COLOR_MENU[i];
					entityBatches[1]->entities[1]->spriteBatch->sprites[0]->RGBAColorMod[i] = Constants::DIM_COLOR_MENU[i];
				}
				loadScoreboard();
			}
		}
	}
	if (InputManager::getInstance()->isKeyJustDown(SDLK_ESCAPE)) {
		eatKeys = false;
		action = SceneAction::DESTROY_SCENE;
	}
	/*if (InputManager::getInstance()->isKeyJustDown(SDLK_1)) {
		_selectedDiff = Difficulty::LEVEL_1;
		loadScoreboard();
	}
	if (InputManager::getInstance()->isKeyJustDown(SDLK_2)) {
		_selectedDiff = Difficulty::LEVEL_2;
		loadScoreboard();
	}
	if (InputManager::getInstance()->isKeyJustDown(SDLK_3)) {
		_selectedDiff = Difficulty::LEVEL_3;
		loadScoreboard();
	}
	if (InputManager::getInstance()->isKeyJustDown(SDLK_4)) {
		_selectedDiff = Difficulty::LEVEL_4;
		loadScoreboard();
	}
	if (InputManager::getInstance()->isKeyJustDown(SDLK_5)) {
		_selectedDiff = Difficulty::LEVEL_5;
		loadScoreboard();
	}
	if (InputManager::getInstance()->isKeyJustDown(SDLK_6)) {
		_selectedDiff = Difficulty::LEVEL_6;
		loadScoreboard();
	}*/

	if (!_loading) {
		if (entityBatches[0]->entities[0]->potentialLocation.y - entityBatches[0]->entities[0]->radius < 904) {
			double x = entityBatches[0]->entities[0]->potentialLocation.x;
			Difficulty oldSD = _selectedDiff;
			if (x > 1680) {
				_selectedDiff = Difficulty::LEVEL_6;
			}
			else if (x > 1440) {
				_selectedDiff = Difficulty::LEVEL_5;
			}
			else if (x > 1200) {
				_selectedDiff = Difficulty::LEVEL_4;
			}
			else if (x > 960) {
				_selectedDiff = Difficulty::LEVEL_3;
			}
			else if (x > 720) {
				_selectedDiff = Difficulty::LEVEL_2;
			}
			else if (x > 480) {
				_selectedDiff = Difficulty::LEVEL_1;
			}
			if (oldSD != _selectedDiff)	loadScoreboard();
		}
	}

	entityBatches[0]->entities[0]->location = entityBatches[0]->entities[0]->location.fixInRange(XYSystem(0, 904) + entityBatches[0]->entities[0]->radius, XYSystem(1920, 1080) - entityBatches[0]->entities[0]->radius);
}

void LeaderboardsScene::loadScoreboard()
{
	_loading = true;
	resetScoreboard();
	spriteBatches[2]->sprites.push_back(std::make_shared<Sprite>("Loading...", XYSystem(960, 540), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, true));
	//std::function<void(Leaderboard)> callback = renderScoreboard;
	SteamManager::getInstance()->loadLeaderboard(_selectedDiff, _selectedType, [&](Leaderboard lb) {renderScoreboard(lb); });
}

void LeaderboardsScene::resetScoreboard()
{
	spriteBatches[0]->sprites.clear();
}

double LeaderboardsScene::buildScoreboardFrame(Leaderboard &lb)
{
	// Remove loading
	spriteBatches[2]->sprites.clear();
	// Remove old entries
	spriteBatches[0]->sprites.clear();

	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::DOT_PATH_PREFIX + Constants::DOT_PATH + Constants::DOT_PATH_SUFFIX, XYSystem(960, 810), XYSystem(1920, 6), Direction(), false, false, Constants::FULL_COLOR, true));

	int highestPlacing = 0;
	std::vector<LeaderboardEntry> entries;
	for (LeaderboardEntry entry : entries) {
		if (entry.placing > highestPlacing) {
			highestPlacing = entry.placing;
		}
	}

	double placingWidth = SDLTextCreator::getTextSize(("00" + std::to_string(highestPlacing) + "00").c_str(), Constants::REGULAR_HEIGHT).x;

	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::DOT_PATH_PREFIX + Constants::DOT_PATH + Constants::DOT_PATH_SUFFIX, XYSystem(placingWidth, 450), XYSystem(4, 900), Direction(), false, false, Constants::FULL_COLOR, true));
	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::DOT_PATH_PREFIX + Constants::DOT_PATH + Constants::DOT_PATH_SUFFIX, XYSystem(480, 450), XYSystem(4, 900), Direction(), false, false, Constants::FULL_COLOR, true));
	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::DOT_PATH_PREFIX + Constants::DOT_PATH + Constants::DOT_PATH_SUFFIX, XYSystem(720, 450), XYSystem(4, 900), Direction(), false, false, Constants::FULL_COLOR, true));
	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::DOT_PATH_PREFIX + Constants::DOT_PATH + Constants::DOT_PATH_SUFFIX, XYSystem(960, 450), XYSystem(4, 900), Direction(), false, false, Constants::FULL_COLOR, true));
	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::DOT_PATH_PREFIX + Constants::DOT_PATH + Constants::DOT_PATH_SUFFIX, XYSystem(1200, 450), XYSystem(4, 900), Direction(), false, false, Constants::FULL_COLOR, true));
	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::DOT_PATH_PREFIX + Constants::DOT_PATH + Constants::DOT_PATH_SUFFIX, XYSystem(1440, 450), XYSystem(4, 900), Direction(), false, false, Constants::FULL_COLOR, true));
	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::DOT_PATH_PREFIX + Constants::DOT_PATH + Constants::DOT_PATH_SUFFIX, XYSystem(1680, 450), XYSystem(4, 900), Direction(), false, false, Constants::FULL_COLOR, true));

	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>("#", XYSystem(placingWidth / 2.0, 855), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, true));
	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>("Player", XYSystem((placingWidth + 480.0) / 2.0, 855), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, true));
	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>("Level 1", XYSystem(600, 855), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, true));
	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>("Level 2", XYSystem(840, 855), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, true));
	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>("Level 3", XYSystem(1080, 855), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, true));
	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>("Level 4", XYSystem(1320, 855), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, true));
	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>("Level 5", XYSystem(1560, 855), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, true));
	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>("Level 6", XYSystem(1800, 855), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, true));

	return placingWidth;

} // maybe pass in leaderboard, wehre it gets parsed, finds longest placement, sets size to that, return longest possible size of player name?

void LeaderboardsScene::renderScoreboard(Leaderboard leaderboard)
{
	double placingWidth = buildScoreboardFrame(leaderboard);
	// rely on steammanager to give us the right leaderboard stuff, so this can just display it
	std::vector<LeaderboardEntry> entries = leaderboard.getEntries();
	Uint8 color[4];
	int thickness;
	std::string name;
	for (unsigned int i = 0; i < entries.size(); i++) {
		if (entries[i].isPlayer) {
			for (int i = 0; i < 4; i++) {
				color[i] = (Uint8)(SettingsManager::getSettings().shipColors[0][i]);
			}
		}
		else {
			for (int i = 0; i < 4; i++) {
				color[i] = (Uint8)(Constants::FULL_COLOR[i]);
			}
		}
		spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>(std::to_string(entries[i].placing), XYSystem(placingWidth / 2.0, 810 - Constants::LEADERBOARD_ENTRY_HEIGHT * (i + .5)), Constants::REGULAR_HEIGHT, Direction(), false, false, color, true));

		spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>(entries[i].steamName, XYSystem((placingWidth + 480.0) / 2.0, 810 - Constants::LEADERBOARD_ENTRY_HEIGHT * (i + .5)), Constants::REGULAR_HEIGHT, Direction(), false, false, color, true));
		if (SDLTextCreator::getTextSize(entries[i].steamName.c_str(), Constants::REGULAR_HEIGHT).x > 476 - placingWidth) {
			spriteBatches[0]->sprites.back()->size.x = 476 - placingWidth;
		}
		spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>(std::to_string(entries[i].scores[0]), XYSystem(600, 810 - Constants::LEADERBOARD_ENTRY_HEIGHT * (i + .5)), Constants::REGULAR_HEIGHT, Direction(), false, false, color, true));
		spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>(std::to_string(entries[i].scores[1]), XYSystem(840, 810 - Constants::LEADERBOARD_ENTRY_HEIGHT * (i + .5)), Constants::REGULAR_HEIGHT, Direction(), false, false, color, true));
		spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>(std::to_string(entries[i].scores[2]), XYSystem(1080, 810 - Constants::LEADERBOARD_ENTRY_HEIGHT * (i + .5)), Constants::REGULAR_HEIGHT, Direction(), false, false, color, true));
		spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>(std::to_string(entries[i].scores[3]), XYSystem(1320, 810 - Constants::LEADERBOARD_ENTRY_HEIGHT * (i + .5)), Constants::REGULAR_HEIGHT, Direction(), false, false, color, true));
		spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>(std::to_string(entries[i].scores[4]), XYSystem(1560, 810 - Constants::LEADERBOARD_ENTRY_HEIGHT * (i + .5)), Constants::REGULAR_HEIGHT, Direction(), false, false, color, true));
		spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>(std::to_string(entries[i].scores[5]), XYSystem(1800, 810 - Constants::LEADERBOARD_ENTRY_HEIGHT * (i + .5)), Constants::REGULAR_HEIGHT, Direction(), false, false, color, true));

		if (entries[i].thinLine) {
			thickness = 1;
		}
		else {
			thickness = 2;
		}
		spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::DOT_PATH_PREFIX + Constants::DOT_PATH + Constants::DOT_PATH_SUFFIX, XYSystem(960, 810 - i * Constants::LEADERBOARD_ENTRY_HEIGHT), XYSystem(1920, thickness), Direction(), false, false, Constants::DIM_COLOR_MENU, true));
	}

	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::POWERUP_PATH_PREFIX + Constants::POWERUP_PATHS[(int)PowerupType::STAR] + Constants::POWERUP_PATH_SUFFIX, XYSystem(450 + (int)leaderboard.getDiff() * 240, 855), XYSystem(25, 25), Direction(), false, false, Constants::FULL_COLOR, true));

	_loading = false;
}

