#pragma once
#include "Constants.h"
#include <fstream>
class SaveDataManager
{
public:
	static void init();
	// returns high score
	static int submitScore(Difficulty difficulty, int score);
	static SaveData getCurrentSaveData();
private:
	static SaveData _currentSaveData;
	static std::string decryptFile(std::ifstream &file);
	static std::string encryptString(std::string input);
	static void writeSaveData();
};

