#pragma once
#include "Scene.h"
class BackgroundScene :
	public Scene
{
public:
	BackgroundScene(std::shared_ptr<SDLRenderer> rnd, Difficulty diff, std::shared_ptr<Camera> sceneCamera);
	~BackgroundScene();
	virtual void init();
	void onSceneChange(std::shared_ptr<Scene> oldScene, std::shared_ptr<Scene> newScene);
	virtual void onSceneChangeCustom(std::shared_ptr<Scene> oldScene, std::shared_ptr<Scene> newScene);
	virtual void deinit();


protected:
	virtual void customEveryTick();
};

