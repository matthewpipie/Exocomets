#pragma once
#include "stdafx.h"
#include "SDLWrapper.h"
enum class KeyState { KEY_UP, KEY_DOWN, KEY_JUST_DOWN, KEY_LOCK };
const int numberOfSDLKeys = 323;

class InputManager
{
public:
	InputManager(InputManager const&) = delete;
	void operator=(InputManager const&) = delete;
	~InputManager();
	static const char* getKeyName(int key);
	static std::shared_ptr<InputManager> getInstance();
	bool isKeyDown(int key);
	bool isKeyJustDown(int key);
	int getLatestPressedKey();
	bool isMouseDown();
	bool isMouseJustDown();
	int getMouseMovementX();
	int getMouseMovementY();
	XYIntSystem getMouseMovement();
	void pollInput();
	bool shouldQuit();
	std::string getDisplayNameFromKey(int key);
	void eatKey(int key);
private:
	InputManager();
	static std::shared_ptr<InputManager> _staticInstance;

	bool _quit;
	XYIntSystem _mouseMovement;
	KeyState _mouseDown;
	KeyState _keysDown[numberOfSDLKeys];
	int _lastPressedKey;

	int rectifyKey(int &key);
	int unRectifyKey(int &key);
	void setButtonsDown();
};