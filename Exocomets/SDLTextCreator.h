#pragma once
#include "SDL_ttfWrapper.h"
#include "stdafx.h"
class SDLTextCreator
{
public:
	static void init();
	static void unload();
	static SDL_Texture* makeText(SDL_Renderer *rnd, const char *text, int fontSize);
	static XYSystem getTextSize(const char *text, double height);
private:
	static TTF_Font* createFont(int fontSize);
	static TTF_Font* getFontFromSize(int fontSize);
	static std::unordered_map<int, TTF_Font*> _fontCache;
};

