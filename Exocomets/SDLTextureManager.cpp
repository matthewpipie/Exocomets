#include "SDLTextureManager.h"
#include "ErrorHandler.h"
#include "SDLTextCreator.h"
#include "Constants.h"
#include "SettingsManager.h"

std::unordered_map<std::string, SDL_Texture*> SDLTextureManager::_textureCache = {};
std::unordered_map<std::string, SDL_Texture*> SDLTextureManager::_textTextureCache = {};
std::map<std::string, SDL_Texture*> SDLTextureManager::_tempTextTextureCache = {};

SDL_Texture *SDLTextureManager::getTexture(const std::string filepath, bool renderFilepathNameInsteadOfFile, SDL_Renderer *rnd) {
	if (renderFilepathNameInsteadOfFile) {
		if (!_textTextureCache.count(filepath)) {
			try {
				if (std::stoi(filepath) > 255) {
					while (_tempTextTextureCache.size() > Constants::TEMP_CACHE_SIZE) {
						SDL_DestroyTexture(_tempTextTextureCache.begin()->second);
						_tempTextTextureCache.erase(_tempTextTextureCache.begin());
					}
					if (!_tempTextTextureCache.count(filepath)) {
						return _tempTextTextureCache[filepath] = SDLTextCreator::makeText(rnd, filepath.c_str(), Constants::FONT_SIZE);
					}
					else {
						return _tempTextTextureCache[filepath];
					}
				}
			}
			catch (...) {}
			return _textTextureCache[filepath] = SDLTextCreator::makeText(rnd, filepath.c_str(), Constants::FONT_SIZE);
		}
		else {
			return _textTextureCache[filepath];
		}
	}
	else {
		if (!_textureCache.count(filepath)) {
			SDL_Texture *testTexture = IMG_LoadTexture(rnd, filepath.c_str());
			if (testTexture == nullptr) {
				ErrorHandler::error(ErrorCode::BAD_FILE_PATH);
			}
			return _textureCache[filepath] = testTexture;
		}
		else {
			return _textureCache[filepath];
		}
	}
}
void SDLTextureManager::init() {
	//Initialize PNG loading
	int imgFlags = IMG_INIT_PNG; // add more filetypes here
	if (!(IMG_Init(imgFlags) & imgFlags)) {
		ErrorHandler::error(ErrorCode::BMP_INIT);
	}
}
void SDLTextureManager::unload() {
	clearCache();
	IMG_Quit();
}
void SDLTextureManager::clearCache() {
	for (auto it = _textureCache.begin(); it != _textureCache.end(); ++it) {
		SDL_DestroyTexture(it->second);
	}
	for (auto it = _textTextureCache.begin(); it != _textTextureCache.end(); ++it) {
		SDL_DestroyTexture(it->second);
	}
	for (auto it = _tempTextTextureCache.begin(); it != _tempTextTextureCache.end(); ++it) {
		SDL_DestroyTexture(it->second);
	}
	_textureCache.clear();
	_textTextureCache.clear();
	_tempTextTextureCache.clear();
}