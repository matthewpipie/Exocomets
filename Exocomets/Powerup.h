#pragma once
#include "CollidableEntity.h"
#include "Camera.h"


class Powerup :
	public CollidableEntity
{
public:
	Powerup(std::shared_ptr<Camera> camera, PowerupType pType);
	Powerup(std::shared_ptr<Camera> camera, PowerupType pType, Difficulty diff);
	Powerup(XYSystem loc, XYSystem speed, PowerupType pType);
	Powerup(PowerupType pType);
	~Powerup();
	void resolvePotentialCollision(CollidableEntity &other) final override;

	void resolvePotentialCollision(Player &other) final override;
	PowerupType powerupType;
protected:
	XYSystem generateLocation(std::shared_ptr<Camera> camera);
	XYSystem generateSpeed(std::shared_ptr<Camera> camera);
	//virtual double generateRadius() = 0;
	//virtual double generateMass() = 0;
};

