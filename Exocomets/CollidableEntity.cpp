#include "CollidableEntity.h"
#include "InputManager.h"
#include "MainGame.h"
#include <iostream>
#include <cstdio>
#include <ctime>

CollidableEntity::CollidableEntity(XYSystem location, double radius, double mass, XYSystem speed) :
	Entity(location, radius, mass, speed) {
}
CollidableEntity::CollidableEntity(XYSystem location, std::vector<XYSystem> relativeHitbox, double mass, XYSystem speed) :
	Entity(location, relativeHitbox, mass, speed)
{
}

void CollidableEntity::resolvePotentialCollision(Comet &other) {
	int a = 0;
}
void CollidableEntity::resolvePotentialCollision(Player &other) {
	int a = 0;
}
void CollidableEntity::resolvePotentialCollision(Powerup &other) {
	int a = 0;
}
void CollidableEntity::resolvePotentialCollision(BulletEntity &other) {
	int a = 0;
}
void CollidableEntity::resolvePotentialCollision(CollidableEntity &other) {
	int a = 0;
}
/*bool CollidableEntity::pointInPolygon(const XYSystem &point, const std::vector<XYSystem> &fakeHitboxes) const {
	int    wn = 0;    // the  winding number counter
					  // loop through all edges of the polygon
	for (int i = 0; i < fakeHitboxes.size() - 1; i++) {   // edge from V[i] to  V[i+1]
		XYSystem realVertex = fakeHitboxes.at(i);
		XYSystem realVertex2 = fakeHitboxes.at(i + 1);
		if (realVertex.y <= point.y) {          // start y <= P.y
			if (realVertex2.y > point.y)      // an upward crossing
				if (point.isLeft(realVertex, realVertex2) > 0)  // P left of  edge
					++wn;            // have  a valid up intersect
		}
		else {                        // start y > P.y (no test needed)
			if (realVertex2.y <= point.y)     // a downward crossing
				if (point.isLeft(realVertex, realVertex2) < 0)  // P right of  edge
					--wn;            // have  a valid down intersect
		}
	}
	return wn != 0;
}

bool CollidableEntity::lineIntersectsCircle(const XYSystem &p1, const XYSystem &p2, const double circleRadius, const XYSystem &circleLocation) const {
	/* This was the original code, but it turns out it only works on lines, not segments (oops)
	double t1 = std::atan2(p2.y - p1.y, p2.x - p1.x);
	double t2 = std::atan2(circle._location.y - p1.y, circle._location.x - p1.x);
	double theta = t2 - t1;
	double closestDistSquared = std::pow(std::sin(theta), 2.0) * p1.distanceToSquared(circle._location);
	return closestDistSquared <= std::pow(circle._radius, 2.0);
	

	if (!circleLocation.lineTangentToLineSegmentThroughPointIntersectsLineSegment(p1, p2)) {
		return false;
	}
	return circleLocation.getTranslationVectorToProjectPointOnToLine(p1, p2).getMagnitudeSquared() <= std::pow(circleRadius, 2.0);
}

bool CollidableEntity::isCircleAndPolygonColliding(const std::vector<XYSystem> &relativePolygonHitbox, const XYSystem &polygonLocation, const double circleRadius, const XYSystem &circleLocation) const {
	/*if (relativePolygonHitbox.size() == 4) {
		XYSystem bottomLeft = relativePolygonHitbox[0];
		XYSystem topRight = relativePolygonHitbox[0];
		for (int i = 0; i < 4; i++) {
			if (relativePolygonHitbox[i].x < bottomLeft.x) {
				bottomLeft.x = relativePolygonHitbox[i].x;
			}
			if (relativePolygonHitbox[i].x > topRight.x) {
				topRight.x = relativePolygonHitbox[i].x;
			}
			if (relativePolygonHitbox[i].y < bottomLeft.y) {
				bottomLeft.y = relativePolygonHitbox[i].y;
			}
			if (relativePolygonHitbox[i].y > topRight.y) {
				topRight.y = relativePolygonHitbox[i].y;
			}
		}
		bool isRectangle = true;
		for (int i = 0; i < 4; i++) {
			if ((relativePolygonHitbox[i].x >= bottomLeft.x) && (relativePolygonHitbox[i].x <= topRight.x) && ((relativePolygonHitbox[i].y == bottomLeft.y) || (relativePolygonHitbox[i].y == topRight.y))) // belongs to top or bottom line
				continue; // continue the 'for' loop
			if (((relativePolygonHitbox[i].x == bottomLeft.x) || (relativePolygonHitbox[i].x == topRight.x)) && (relativePolygonHitbox[i].y >= bottomLeft.y) && (relativePolygonHitbox[i].y <= topRight.y)) // belongs to top or bottom line
				continue; // continue the 'for' loop
			isRectangle = false;
		}
		if (isRectangle) {
			//std::cout << "rectangles!" << std::endl;
			if ((circleLocation.fixInRange(bottomLeft + polygonLocation - circleRadius, topRight + polygonLocation + circleRadius) != circleLocation)) {
				return false;
			}
			else {
				return true;
			}
		}
	}
	// Make a copy of hitbox points
	//return true;
	auto fakeHitboxes = relativePolygonHitbox;	
	
	double biggestMag = 0;
	for (int i = 0; i < fakeHitboxes.size(); i++) {
		if (fakeHitboxes[i].getMagnitudeSquared() > biggestMag) biggestMag = fakeHitboxes[i].getMagnitudeSquared();
	}
	if (!isCircleAndCircleColliding(std::sqrt(biggestMag), polygonLocation, circleRadius, circleLocation)) return false;
	
	// Add location to hitboxes to make them absolute and not relative
	std::for_each(fakeHitboxes.begin(), fakeHitboxes.end(), [polygonLocation](XYSystem &relativePoint) {relativePoint += polygonLocation; });
	// Add the first to the end so that it works nicely with for loops
	fakeHitboxes.push_back(fakeHitboxes.at(0));
	// Start by looking at if center of circle is in polygon
	if (pointInPolygon(circleLocation, fakeHitboxes)) return true;
	// Now go through each two points and see if there's an intersection with the circle
	for (int i = 0; i < fakeHitboxes.size() - 1; i++) {
		if (lineIntersectsCircle(fakeHitboxes.at(i), fakeHitboxes.at(i + 1), circleRadius, circleLocation)) {
			return true;
		}
	}
	return false;

}

bool CollidableEntity::satTestAxisOverlaps(const XYSystem &polygon1Location, const std::vector<XYSystem> &polygon1RelativeHitbox, const XYSystem &polygon2Location, const std::vector<XYSystem> &polygon2RelativeHitbox, const XYSystem &axisPoint1, const XYSystem &axisPoint2) const {
	double minX1, maxX1, minX2, maxX2;
	minX1 = (polygon1Location + polygon1RelativeHitbox.at(0)).getTranslatedPointToLine(axisPoint1, axisPoint2).x;
	maxX1 = (polygon1Location + polygon1RelativeHitbox.at(0)).getTranslatedPointToLine(axisPoint1, axisPoint2).x;
	minX2 = (polygon2Location + polygon2RelativeHitbox.at(0)).getTranslatedPointToLine(axisPoint1, axisPoint2).x;
	maxX2 = (polygon2Location + polygon2RelativeHitbox.at(0)).getTranslatedPointToLine(axisPoint1, axisPoint2).x;
	for (int i = 0; i < polygon1RelativeHitbox.size(); i++) {
		XYSystem realLoc = (polygon1Location + polygon1RelativeHitbox.at(i)).getTranslatedPointToLine(axisPoint1, axisPoint2);
		if (realLoc.x < minX1) {
			minX1 = realLoc.x;
		}
		if (realLoc.x > maxX1) {
			maxX1 = realLoc.x;
		}
	}
	for (int i = 0; i < polygon2RelativeHitbox.size(); i++) {
		XYSystem realLoc = (polygon2Location + polygon2RelativeHitbox.at(i)).getTranslatedPointToLine(axisPoint1, axisPoint2);
		if (realLoc.x < minX2) {
			minX2 = realLoc.x;
		}
		if (realLoc.x > maxX2) {
			maxX2 = realLoc.x;
		}
	}
	return !(maxX1 < minX2 || maxX2 < minX1);
}

bool CollidableEntity::isPolygonAndPolygonColliding(const std::vector<XYSystem> &relativePolygon1Hitbox, const XYSystem &polygon1Location, const std::vector<XYSystem> &relativePolygon2Hitbox, const XYSystem &polygon2Location) const {
	// Make a copy of hitbox points
	auto fakeHitboxes1 = relativePolygon1Hitbox;
	auto fakeHitboxes2 = relativePolygon2Hitbox;

	double biggestMag1 = 0;
	double biggestMag2 = 0;
	for (int i = 0; i < fakeHitboxes1.size(); i++) {
		if (fakeHitboxes1[i].getMagnitudeSquared() > biggestMag1) biggestMag1 = fakeHitboxes1[i].getMagnitudeSquared();
	}
	for (int i = 0; i < fakeHitboxes2.size(); i++) {
		if (fakeHitboxes1[i].getMagnitudeSquared() > biggestMag2) biggestMag2 = fakeHitboxes2[i].getMagnitudeSquared();
	}
	if (!isCircleAndCircleColliding(std::sqrt(biggestMag1), polygon1Location, std::sqrt(biggestMag2), polygon2Location)) return false;

	// Add the first to the end so that it works nicely with for loops
	fakeHitboxes1.push_back(fakeHitboxes1.at(0));
	fakeHitboxes2.push_back(fakeHitboxes2.at(0));
	// Add location to hitboxes to make them absolute and not relative
	std::for_each(fakeHitboxes1.begin(), fakeHitboxes1.end(), [polygon1Location](XYSystem &relativePoint) {relativePoint += polygon1Location; });
	std::for_each(fakeHitboxes2.begin(), fakeHitboxes2.end(), [polygon2Location](XYSystem &relativePoint) {relativePoint += polygon2Location; });
	for (int i = 0; i < fakeHitboxes1.size() - 1; i++) {
		XYSystem point1 = fakeHitboxes1.at(i);
		XYSystem point2 = fakeHitboxes1.at(i + 1);
		XYSystem newPoint2;
		if (point2.y - point1.y == 0) {
			newPoint2 = XYSystem(point1.x, point1.y + 1);
		}
		else {
			// Perpendicular line that crosses through point1
			newPoint2 = XYSystem(point1.x + 1, point1.y + (point1.x - point2.x) / (point2.y - point1.y));
		}
		if (!satTestAxisOverlaps(polygon1Location, relativePolygon1Hitbox, polygon2Location, relativePolygon2Hitbox, point1, newPoint2)) {
			return false;
		}
	}
	for (int i = 0; i < fakeHitboxes2.size() - 1; i++) {
		XYSystem point1 = fakeHitboxes2.at(i);
		XYSystem point2 = fakeHitboxes2.at(i + 1);
		XYSystem newPoint2;
		if (point2.y - point1.y == 0) {
			newPoint2 = XYSystem(point1.x, point1.y + 1);
		}
		else {
			// Perpendicular line that crosses through point1
			newPoint2 = XYSystem(point1.x + 1, point1.y + (point1.x - point2.x) / (point2.y - point1.y));
		}
		if (!satTestAxisOverlaps(polygon1Location, relativePolygon1Hitbox, polygon2Location, relativePolygon2Hitbox, point1, newPoint2)) {
			return false;
		}
	}
	return true;
}
*/
bool CollidableEntity::isCurrentlyColliding(const CollidableEntity& other) const {
	if (type == EntityType::CIRCLE && other.type == EntityType::CIRCLE) {
		return isCircleAndCircleColliding(radius, location, other.radius, other.location);
	}
	else if (type == EntityType::POLYGON && other.type == EntityType::CIRCLE) {
		return isCircleAndPolygonColliding(relativeHitbox, location, other.radius, other.location);
	}
	else if (type == EntityType::CIRCLE && other.type == EntityType::POLYGON) {
		return isCircleAndPolygonColliding(other.relativeHitbox, other.location, radius, location);
	}
	else if (type == EntityType::POLYGON && other.type == EntityType::POLYGON) {
		return isPolygonAndPolygonColliding(relativeHitbox, location, other.relativeHitbox, other.location);
	}

	return false;
}
bool CollidableEntity::isPotentiallyColliding(const CollidableEntity& other) const {
	if (type == EntityType::CIRCLE && other.type == EntityType::CIRCLE) {
		if (!isCircleAndCircleColliding(potentialLocation.distanceTo(location) + radius, potentialLocation, other.potentialLocation.distanceTo(other.location) + other.radius, other.potentialLocation)) { //20988 /35xxx /
		//XYSystem midpt1 = potentialLocation.getMidpoint(location); // 20460
		//XYSystem midpt2 = other.potentialLocation.getMidpoint(other.location); // 33216
		//if (!isCircleAndCircleColliding(midpt1.distanceTo(potentialLocation) + radius, midpt1, midpt2.distanceTo(other.potentialLocation) + other.radius, midpt2)) {
			return false;
		}
		bool moved1 = false;
		std::vector<XYSystem> hitbox1;
		Direction d1;
		if (potentialLocation != location) {
			moved1 = true;
			d1 = Direction(location, potentialLocation);
			XYSystem left(d1.rotateLeftDegrees(90).polarToCartesian(radius));
			XYSystem right(d1.rotateRightDegrees(90).polarToCartesian(radius)); //assume
			hitbox1.push_back(location + left);
			hitbox1.push_back(location + right);
			hitbox1.push_back(potentialLocation + right);
			hitbox1.push_back(potentialLocation + left);
		}

		bool moved2 = false;
		std::vector<XYSystem> hitbox2;
		Direction d2;
		if (other.potentialLocation != other.location) {
			moved2 = true;
			d2 = Direction(other.location, other.potentialLocation);
			XYSystem left(d2.rotateLeftDegrees(90).polarToCartesian(radius));
			XYSystem right(d2.rotateRightDegrees(90).polarToCartesian(radius));
			hitbox2.push_back(other.location + left);
			hitbox2.push_back(other.location + right);
			hitbox2.push_back(other.potentialLocation + right);
			hitbox2.push_back(other.potentialLocation + left);
		}


		return isCircleAndCircleColliding(radius, potentialLocation, other.radius, other.potentialLocation) ||
			(moved1 && !moved2? isCircleAndPolygonColliding(hitbox1, XYSystem(), other.radius, other.potentialLocation) : false) ||
			(!moved2 && moved2? isCircleAndPolygonColliding(hitbox2, XYSystem(), radius, potentialLocation) : false) ||
			(moved1 && moved2 ? isPolygonAndPolygonColliding(hitbox1, XYSystem(), hitbox2, XYSystem()) : false);
	}
	else if (type == EntityType::POLYGON && other.type == EntityType::CIRCLE) {
		bool moved2 = false;
		std::vector<XYSystem> hitbox2;
		Direction d2;
		if (other.potentialLocation != other.location) {
			moved2 = true;
			d2 = Direction(other.location, other.potentialLocation);
			XYSystem left(d2.rotateLeftDegrees(90).polarToCartesian(radius));
			XYSystem right(d2.rotateRightDegrees(90).polarToCartesian(radius));
			hitbox2.push_back(other.location + left);
			hitbox2.push_back(other.location + right);
			hitbox2.push_back(other.potentialLocation + right);
			hitbox2.push_back(other.potentialLocation + left);
		}

		return isCircleAndPolygonColliding(relativeHitbox, potentialLocation, other.radius, other.potentialLocation) ||
			(moved2 ? isPolygonAndPolygonColliding(relativeHitbox, potentialLocation, hitbox2, XYSystem()) : false);
	}
	else if (type == EntityType::CIRCLE && other.type == EntityType::POLYGON) {
		bool moved1 = false;
		std::vector<XYSystem> hitbox1;
		Direction d1;
		if (potentialLocation != location) {
			moved1 = true;
			d1 = Direction(location, potentialLocation);
			XYSystem left(d1.rotateLeftDegrees(90).polarToCartesian(radius));
			XYSystem right(d1.rotateRightDegrees(90).polarToCartesian(radius));
			hitbox1.push_back(location + left);
			hitbox1.push_back(location + right);
			hitbox1.push_back(potentialLocation + right);
			hitbox1.push_back(potentialLocation + left);
		}
		return isCircleAndPolygonColliding(other.relativeHitbox, other.potentialLocation, radius, potentialLocation) ||
			(moved1 ? isPolygonAndPolygonColliding(other.relativeHitbox, other.potentialLocation, hitbox1, XYSystem()) : false);
	}
	else if (type == EntityType::POLYGON && other.type == EntityType::POLYGON) {
		return isPolygonAndPolygonColliding(relativeHitbox, potentialLocation, other.relativeHitbox, other.potentialLocation);
	}

	return false;
}

bool CollidableEntity::isCircleAndCircleColliding(const double circle1Radius, const XYSystem &circle1Location, const double circle2Radius, const XYSystem &circle2Location) const {
	if (std::abs(circle1Location.x - circle2Location.x) > circle1Radius + circle2Radius || std::abs(circle1Location.y - circle2Location.y) > circle1Radius + circle2Radius) return false;
	return circle1Location.distanceToSquared(circle2Location) <= std::pow(circle1Radius + circle2Radius, 2.0);
}

bool CollidableEntity::isCircleAndPolygonColliding(const std::vector<XYSystem> &relativePolygonHitbox, const XYSystem &polygonLocation, const double circleRadius, const XYSystem &circleLocation) const {
	double biggestMag = 0;
	for (unsigned int i = 0; i < relativePolygonHitbox.size(); i++) {
		if (relativePolygonHitbox[i].getMagnitudeSquared() > biggestMag) {
			biggestMag = relativePolygonHitbox[i].getMagnitudeSquared();
		}
	}
	if (!isCircleAndCircleColliding(std::sqrt(biggestMag), polygonLocation, circleRadius, circleLocation)) return false;

	XYSystem closestPointToCircle = relativePolygonHitbox[0];
	for (unsigned int i = 0; i < relativePolygonHitbox.size(); i++) {
		if (circleLocation.distanceToSquared(relativePolygonHitbox[i] + polygonLocation) < circleLocation.distanceToSquared(closestPointToCircle)) {
			closestPointToCircle = relativePolygonHitbox[i] + polygonLocation;
		}
		XYSystem axisVectorPerp;
		if (i == relativePolygonHitbox.size() - 1) {
			axisVectorPerp = relativePolygonHitbox[0] - relativePolygonHitbox[i];
		}
		else {
			axisVectorPerp = relativePolygonHitbox[i + 1] - relativePolygonHitbox[i];
		}
		if (!testSATAxis(XYSystem(-axisVectorPerp.y, axisVectorPerp.x), relativePolygonHitbox, polygonLocation, circleRadius, circleLocation)) {
			return false;
		}
	}
	XYSystem axisVectorPerp = circleLocation - closestPointToCircle;
	if (!testSATAxis(XYSystem(-axisVectorPerp.y, axisVectorPerp.x), relativePolygonHitbox, polygonLocation, circleRadius, circleLocation)) {
		return false;
	}
	return true;
}

bool CollidableEntity::isPolygonAndPolygonColliding(const std::vector<XYSystem> &relativePolygon1Hitbox, const XYSystem &polygon1Location, const std::vector<XYSystem> &relativePolygon2Hitbox, const XYSystem &polygon2Location) const {
	double biggestMag1 = 0;
	for (unsigned int i = 0; i < relativePolygon1Hitbox.size(); i++) {
		if (relativePolygon1Hitbox[i].getMagnitudeSquared() > biggestMag1) {
			biggestMag1 = relativePolygon1Hitbox[i].getMagnitudeSquared();
		}
	}
	double biggestMag2 = 0;
	for (unsigned int i = 0; i < relativePolygon2Hitbox.size(); i++) {
		if (relativePolygon2Hitbox[i].getMagnitudeSquared() > biggestMag2) {
			biggestMag2 = relativePolygon2Hitbox[i].getMagnitudeSquared();
		}
	}
	if (!isCircleAndCircleColliding(std::sqrt(biggestMag1), polygon1Location, std::sqrt(biggestMag2), polygon2Location)) return false;

	for (unsigned int i = 0; i < relativePolygon1Hitbox.size(); i++) {
		XYSystem axisVectorPerp;
		if (i == relativePolygon1Hitbox.size() - 1) {
			axisVectorPerp = relativePolygon1Hitbox[0] - relativePolygon1Hitbox[i];
		}
		else {
			axisVectorPerp = relativePolygon1Hitbox[i + 1] - relativePolygon1Hitbox[i];
		}
		if (!testSATAxis(XYSystem(-axisVectorPerp.y, axisVectorPerp.x), relativePolygon1Hitbox, polygon1Location, relativePolygon2Hitbox, polygon2Location)) {
			return false;
		}
	}
	for (unsigned int i = 0; i < relativePolygon2Hitbox.size(); i++) {
		XYSystem axisVectorPerp;
		if (i == relativePolygon2Hitbox.size() - 1) {
			axisVectorPerp = relativePolygon2Hitbox[0] - relativePolygon2Hitbox[i];
		}
		else {
			axisVectorPerp = relativePolygon2Hitbox[i + 1] - relativePolygon2Hitbox[i];
		}
		if (!testSATAxis(XYSystem(-axisVectorPerp.y, axisVectorPerp.x), relativePolygon1Hitbox, polygon1Location, relativePolygon2Hitbox, polygon2Location)) {
			return false;
		}
	}
	return true;
}

bool CollidableEntity::testSATAxis(XYSystem axisVector, const std::vector<XYSystem>& relativePolygon1Hitbox, const XYSystem & polygon1Location, const std::vector<XYSystem>& relativePolygon2Hitbox, const XYSystem & polygon2Location) const {
	double min1 = std::numeric_limits<double>::max();
	double max1 = 0;
	double min2 = std::numeric_limits<double>::max();
	double max2 = 0;
	double axisVectorAngle = std::atan2(axisVector.y, axisVector.x);
	XYSystem absPt;
	double test;
	for (unsigned int i = 0; i < relativePolygon1Hitbox.size(); i++) {
		absPt = relativePolygon1Hitbox[i] + polygon1Location;
		//theta = std::atan2(absPt.y, absPt.x) - axisVectorAngle;
		test = absPt.getMangitude() * std::cos(std::atan2(absPt.y, absPt.x) - axisVectorAngle);
		if (test < min1) min1 = test;
		if (test > max1) max1 = test;
	}
	for (unsigned int i = 0; i < relativePolygon2Hitbox.size(); i++) {
		absPt = relativePolygon2Hitbox[i] + polygon2Location;
		//theta = std::atan2(absPt.y, absPt.x) - axisVectorAngle;
		test = absPt.getMangitude() * std::cos(std::atan2(absPt.y, absPt.x) - axisVectorAngle);
		if (test < min2) min2 = test;
		if (test > max2) max2 = test;
	}
	if (max1 < min2) return false;
	if (max2 < min1) return false;
	return true;
}

bool CollidableEntity::testSATAxis(XYSystem axisVector, const std::vector<XYSystem>& relativePolygonHitbox, const XYSystem & polygonLocation, double circleRadius, const XYSystem & circleLocation) const {
	double min1 = std::numeric_limits<double>::max();
	double max1 = 0;
	double min2 = std::numeric_limits<double>::max();
	double max2 = 0;
	double axisVectorAngle = std::atan2(axisVector.y, axisVector.x);
	for (unsigned int i = 0; i < relativePolygonHitbox.size(); i++) {
		XYSystem absPt = relativePolygonHitbox[i] + polygonLocation;
		double theta = std::atan2(absPt.y, absPt.x) - axisVectorAngle;
		double test = absPt.getMangitude() * std::cos(theta);
		if (test < min1) min1 = test;
		if (test > max1) max1 = test;
	}
	
	double theta = std::atan2(circleLocation.y, circleLocation.x) - axisVectorAngle;
	double test = circleLocation.getMangitude() * std::cos(theta);
	min2 = test - circleRadius;
	max2 = test + circleRadius;

	if (max1 < min2) return false;
	if (max2 < min1) return false;
	return true;
}
