#pragma once
#include "Scene.h"
class CreditsScene :
	public Scene
{
public:
	CreditsScene(std::shared_ptr<SDLRenderer> renderer);
	~CreditsScene();
protected:
	void customEveryTick() override;
};

