#pragma once
#include "stdafx.h"
#include "SDLWrapper.h"

enum class PowerupType {
	STAR=0,
	SHIELD,
	BOMB,
	TIMER,
	//REPEL,
	GUN,
	LAST
};

enum class Difficulty {
	MAIN_MENU=0,
	LEVEL_1,
	LEVEL_2,
	LEVEL_3,
	LEVEL_4,
	LEVEL_5,
	LEVEL_6
};
enum class WindowMode {
	WINDOWED = 0,
	FULLSCREEN = SDL_WINDOW_FULLSCREEN,
	FULLSCREEN_BORDERLESS = SDL_WINDOW_FULLSCREEN_DESKTOP
};
enum class Colors {
	R=0,
	G,
	B
};
enum class Players {
	PLAYER_1=0,
	PLAYER_2,
	PLAYER_3,
	PLAYER_4
};
enum class Controls {
	LEFT=0,
	DOWN,
	UP,
	RIGHT
};

enum class Achievement {
	BEAT_LVL_1=0,
	BEAT_LVL_2,
	BEAT_LVL_3,
	BEAT_LVL_4,
	BEAT_LVL_5,
	BEAT_LVL_6,
	STARPOWER_1000_NO_POWERUP,
	BEAT_4P_GAME,
	REVIVE_PLAYER,
	LOSE_1000_TIMES,
	RESPECT,
	COLLECT_100_STARS,
	COLLECT_100_SHIELDS,
	COLLECT_100_BOMBS,
	COLLECT_100_TIMERS,
	COLLECT_100_GUNS,
	LAST
};

struct SaveData {
	int highScores[6];
};
struct SaveDataVerification {
	int highScores;
	bool operator==(const SaveDataVerification &O) {
		return highScores == O.highScores;
	}
	bool operator!=(const SaveDataVerification &O) {
		return !operator==(O);
	}
};
struct Settings {
	int shipColors[4][4];
	int playerCount;
	int controls[4][4];
	bool sillySounds;
	int fontColor[4];
	WindowMode windowMode;
	int windowSize[2];
	int desiredFPS;
	double sensMultipliers[4];
	int backgroundColor[4]; // RGBA
	bool enableVSync;
	int musicVolume;
	int soundVolume;
	bool operator==(const Settings &O) {
		return
			shipColors[0][0] == O.shipColors[0][0] &&
			shipColors[0][1] == O.shipColors[0][1] &&
			shipColors[0][2] == O.shipColors[0][2] &&
			shipColors[0][3] == O.shipColors[0][3] &&
			shipColors[1][0] == O.shipColors[1][0] &&
			shipColors[1][1] == O.shipColors[1][1] &&
			shipColors[1][2] == O.shipColors[1][2] &&
			shipColors[1][3] == O.shipColors[1][3] &&
			shipColors[2][0] == O.shipColors[2][0] &&
			shipColors[2][1] == O.shipColors[2][1] &&
			shipColors[2][2] == O.shipColors[2][2] &&
			shipColors[2][3] == O.shipColors[2][3] &&
			shipColors[3][0] == O.shipColors[3][0] &&
			shipColors[3][1] == O.shipColors[3][1] &&
			shipColors[3][2] == O.shipColors[3][2] &&
			shipColors[3][3] == O.shipColors[3][3] &&
			playerCount == O.playerCount &&
			controls[0][0] == O.controls[0][0] &&
			controls[0][1] == O.controls[0][1] &&
			controls[0][2] == O.controls[0][2] &&
			controls[0][3] == O.controls[0][3] &&
			controls[1][0] == O.controls[1][0] &&
			controls[1][1] == O.controls[1][1] &&
			controls[1][2] == O.controls[1][2] &&
			controls[1][3] == O.controls[1][3] &&
			controls[2][0] == O.controls[2][0] &&
			controls[2][1] == O.controls[2][1] &&
			controls[2][2] == O.controls[2][2] &&
			controls[2][3] == O.controls[2][3] &&
			controls[3][0] == O.controls[3][0] &&
			controls[3][1] == O.controls[3][1] &&
			controls[3][2] == O.controls[3][2] &&
			controls[3][3] == O.controls[3][3] &&
			sillySounds == O.sillySounds &&
			fontColor[0] == O.fontColor[0] &&
			fontColor[1] == O.fontColor[1] &&
			fontColor[2] == O.fontColor[2] &&
			fontColor[3] == O.fontColor[3] &&
			windowMode == O.windowMode &&
			windowSize[0] == O.windowSize[0] &&
			windowSize[1] == O.windowSize[1] &&
			desiredFPS == O.desiredFPS &&
			std::abs(sensMultipliers[0] - O.sensMultipliers[0]) < 0.001 &&
			std::abs(sensMultipliers[1] - O.sensMultipliers[1]) < 0.001 &&
			std::abs(sensMultipliers[2] - O.sensMultipliers[2]) < 0.001 &&
			std::abs(sensMultipliers[3] - O.sensMultipliers[3]) < 0.001 &&
			backgroundColor[0] == O.backgroundColor[0] &&
			backgroundColor[1] == O.backgroundColor[1] &&
			backgroundColor[2] == O.backgroundColor[2] &&
			backgroundColor[3] == O.backgroundColor[3] &&
			enableVSync == O.enableVSync &&
			musicVolume == O.musicVolume &&
			soundVolume == O.soundVolume;
	}
};
struct SettingsVerification {
	int shipColors;
	int playerCount;
	int controls;
	int sillySounds;
	int fontColor;
	int windowMode;
	int windowSize;
	int desiredFPS;
	int sensMultipliers;
	int backgroundColor;
	int enableVSync;
	int musicVolume;
	int soundVolume;
	bool operator==(const SettingsVerification &O) {
		return shipColors == O.shipColors &&
			playerCount == O.playerCount &&
			controls == O.controls &&
			sillySounds == O.sillySounds &&
			fontColor == O.fontColor &&
			windowMode == O.windowMode &&
			windowSize == O.windowSize &&
			desiredFPS == O.desiredFPS &&
			sensMultipliers == O.sensMultipliers &&
			backgroundColor == O.backgroundColor &&
			enableVSync == O.enableVSync &&
			musicVolume == O.musicVolume &&
			soundVolume == O.soundVolume;
	}
	bool operator!=(const SettingsVerification &O) {
		return !operator==(O);
	}
};
class Constants
{
public:
	//Game
	static const char *GAME_NAME;
	static const char *GAME_VERSION;

	static const int GAME_PLAYAGAIN_COOLDOWN;
	static const double SCORE_MULTIPLIER;

	//What interval does the game run in? Since the game is not frame based, this var determines how many times a second EVERYTHING updates
	static const double TICK_RATE;
	static const int MAX_UPDATES_TO_RENDER;
	static const int ANGLE_STEPS;
	//SDL weirdness
	static const int KEYBOARD_SDL_CONTROL_NUMBER;
	static const XYIntSystem SCREEN_SIZE_LOGICAL;

	static const int FONT_SIZE;
	static const double REGULAR_HEIGHT;

	//Camera
	static const XYSystem CAMERA_CENTER_INIT;

	static const unsigned int TEMP_CACHE_SIZE;

	//Changeable defaults
	/*static const Uint8 SHIP_COLORS_DEFAULT[4][3];
	static const int STAR_COUNT_DEFAULT;
	static const int PLAYER_COUNT_DEFAULT; // for use in settings menu
	static const int CONTROLS_DEFAULT[4][4];
	static const double FONT_SIZE_DEFAULT;
	static const WindowMode WINDOW_MODE_DEFAULT;*/
	static const Settings SETTINGS_DEFAULT;
	static const SettingsVerification SETTINGS_VERIFIED;
	static const SettingsVerification SETTINGS_UNVERIFIED;

	static const SaveData SAVE_DATA_EMPTY;
	static const SaveDataVerification SAVE_DATA_UNVERIFIED;
	static const SaveDataVerification SAVE_DATA_VERIFIED;
	static const char SAVE_DATA_DELIM;

	static const int SAVE_DATA_JIBBERISH_RATE;
	static const int SAVE_DATA_ENCRYPTION_MULTIPLIER1;
	static const int SAVE_DATA_ENCRYPTION_MULTIPLIER2;
	static const int SAVE_DATA_ENCRYPTION_CONSTANT;
	static const int SAVE_DATA_KEY[3];

	static const int SCORE_LIMIT;


	//Colors
	static const Uint8 FULL_COLOR[4];
	static const Uint8 NO_COLOR[4];
	static const Uint8 DIM_COLOR_MENU[4];
	static const Uint8 DIM_COLOR_LOSE[4];

	static const Uint8 TIMER_INCOMPLETE[4];
	static const Uint8 TIMER_COMPLETE[4];
	

	//Stars
	/*static const double STAR_MIN_NONTWINKLE_TIME; //TIME is in TICK_RATEs
	static const double STAR_MAX_NONTWINKLE_TIME;
	static const double STAR_TWINKLE_TIME;
	static const double STAR_SIZE;*/

	static const double STAR_SIZE_TWINKLE_MOD_MIN;
	static const double STAR_SIZE_TWINKLE_MOD_MAX;
	static const int STAR_TWINKLE_TIME_MIN;
	static const int STAR_TWINKLE_TIME_MAX;
	static const int STAR_NONTWINKLE_TIME_MIN;
	static const int STAR_NONTWINKLE_TIME_MAX;

	//Powerups
	static const double POWERUP_RADIUS;
	static const double POWERUP_MASS;
	static const double POWERUP_SPEED_MIN;
	static const double POWERUP_SPEED_MAX;
	static const double POWERUP_SPEED_MULTIPLIER;
	static const double POWERUP_SPEED_BASE;
	static const std::string POWERUP_PATH_PREFIX;
	static const std::string POWERUP_PATHS[(int)PowerupType::LAST];
	static const std::string POWERUP_PATH_SUFFIX;
	static const double EXPLOSION_RADIUS_MULTIPLIER;
	static const std::string EXPLOSION_PATH_PREFIX;
	static const std::string EXPLOSION_PATH;
	static const std::string EXPLOSION_PATH_SUFFIX;
	static const double SLOWMO_RADIUS_MULTIPLIER;
	static const std::string SLOWMO_PATH_PREFIX;
	static const std::string SLOWMO_PATH;
	static const std::string SLOWMO_PATH_SUFFIX;
	static const std::string GUN_PATH_PREFIX;
	static const std::string GUN_PATH;
	static const std::string GUN_PATH_SUFFIX;
	static const XYSystem GUN_SIZE;
	static const int GUN_DIRECTIONS;
	static const Uint8 GUN_COLOR[4];
	static const int GUN_FIRE_RATE;
	static const XYSystem BULLET_SIZE;
	static const double BULLET_SPEED;
	static const std::string BULLET_PATH_PREFIX;
	static const std::string BULLET_PATH;
	static const std::string BULLET_PATH_SUFFIX;
	

	//Comets
	static const double COMET_RADIUS;
	static const double COMET_MASS;
	static const double COMET_RADIUS_GIANT;
	static const double COMET_MASS_GIANT;
	static const double COMET_GIANT_CHANCE; // 0-1000
	static const double COMET_GIANT_SPEED_MP;
	static const int COMET_SPAWN_ATTEMPTS_PER_TICK;
	static const std::string COMET_PATH_PREFIX;
	static const std::string COMET_PATH;
	static const std::string COMET_PATH_SUFFIX;

	//Players
	static const double PLAYER_RADIUS;
	static const double PLAYER_MASS;
	static const double PLAYER_SPEED_HORIZ;
	static const double PLAYER_SPEED_VERT;
	static const std::string PLAYER_PATH_PREFIX;
	static const std::string PLAYER_PATH;
	static const std::string PLAYER_PATH_SUFFIX;

	static const std::string SHIELD_PATH_PREFIX;
	static const std::string SHIELD_PATH;
	static const std::string SHIELD_PATH_SUFFIX;
	static const int SHIELD_RADIUS;

	//Difficulty Dependent
	static const int COMET_COUNT_INIT[7];
	static const int COMET_SPAWN_RATES[7];
	static const double COMET_SPEED_MIN[7];
	static const double COMET_SPEED_MAX[7];
	static const double COMET_SPEED_MULTIPLIER[7];
	static const double ZOOM_LEVELS[7];
	static const int POWERUP_RATES[7];

	//Music
	static const std::string MUSIC_NAMES[7];
	static const std::vector<double> MUSIC_START_TIMES[7];
	static const std::string MUSIC_PATH_PREFIX;
	static const std::string MUSIC_PATHS[7];
	static const std::string MUSIC_PATH_SUFFIX;
	static const double MUSIC_BPMS[7]; //in BPM
	static const double MUSIC_OFFSETS[7]; //in Beats
	static const int MUSIC_COUNT; //7

	static const double MUSIC_START_JUMP;

	//Sound
	static const std::string SOUND_EFFECT_NAMES[6];
	static const std::string SOUND_EFFECT_PATH_PREFIX;
	static const std::string SOUND_EFFECT_PATHS[6];
	static const std::string SOUND_EFFECT_PATH_SUFFIX;
	static const int SOUND_EFFECT_COUNT; //6

	//Font
	static const std::string FONT_PATH_PREFIX;
	static const std::string FONT_PATH;
	static const std::string FONT_PATH_SUFFIX;
	
	//Other Paths
	static const std::string SETTINGS_PATH_PREFIX;
	static const std::string SETTINGS_PATH;
	static const std::string SETTINGS_PATH_SUFFIX;
	static const std::string SAVE_DATA_PATH_PREFIX;
	static const std::string SAVE_DATA_PATH;
	static const std::string SAVE_DATA_PATH_SUFFIX;
	static const std::string PATH_BASE_LOCATION;
	static const std::string ARROW_PATH_PREFIX;
	static const std::string ARROW_PATH;
	static const std::string ARROW_PATH_SUFFIX;
	static const XYSystem ARROW_SIZE;
	
	//Settings
	static const char SETTINGS_NAME_VAL_DELIM;
	static const char SETTINGS_VAL_NAME_DELIM;
	static const double KEY_OPTION_RADIUS;

	//Entities
	static const int ENTITY_COUNT_MAX;
	static const double SLIDER_BAR_HORIZ_Y;
	static const XYSystem SLIDER_BAR_VERT;
	static const XYSystem DOT_SIZE;
	static const std::string DOT_PATH_PREFIX;
	static const std::string DOT_PATH;
	static const std::string DOT_PATH_SUFFIX;

	static const std::vector<XYIntSystem> RESOLUTIONS;
	
	static const double LEADERBOARD_ENTRY_HEIGHT;
	static const int LEADERBOARD_ENTRY_COUNT;

	//Steam
	static const int ACHIEVEMENTS_EMPTY[(int)Achievement::LAST];
	static const int ACHIEVEMENTS_COMPLETED[(int)Achievement::LAST];
	static const std::string STAT_PREFIX;

};

