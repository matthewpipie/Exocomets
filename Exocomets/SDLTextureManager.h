#pragma once
#include "stdafx.h"
#include "SDL_imageWrapper.h"
#include <map>
class SDLTextureManager
{
public:
	static SDL_Texture *getTexture(std::string filepath, bool renderFilepathNameInsteadOfFile, SDL_Renderer *rnd);
	static void init();
	static void unload();
	static void clearCache();
private:
	static std::unordered_map<std::string, SDL_Texture*> _textureCache;
	static std::unordered_map<std::string, SDL_Texture*> _textTextureCache;
	static std::map<std::string, SDL_Texture*> _tempTextTextureCache;
};

