#include "RectangleEntity.h"


RectangleEntity::RectangleEntity(XYSystem location, std::vector<XYSystem> relativeHitbox, double mass, XYSystem speed) :
	CollidableEntity(location, relativeHitbox, mass, speed)
{
}


RectangleEntity::~RectangleEntity()
{
}

void RectangleEntity::resolvePotentialCollision(CollidableEntity &other) {
	other.resolvePotentialCollision(*this);
}
