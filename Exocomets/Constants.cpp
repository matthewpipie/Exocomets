#include "Constants.h"


//Game
const char *Constants::GAME_NAME = "Exocomets";
const char *Constants::GAME_VERSION = "1.2.0";

const int Constants::GAME_PLAYAGAIN_COOLDOWN = 60;
const double Constants::SCORE_MULTIPLIER = 100.0;

//What interval does the game run in? Since the game is not frame based, this var determines how many times a second EVERYTHING updates
const double Constants::TICK_RATE = 60.0;
const int Constants::MAX_UPDATES_TO_RENDER = 10;
const int Constants::ANGLE_STEPS = 32;
//SDL weirdness
const int Constants::KEYBOARD_SDL_CONTROL_NUMBER = 1073741753;
const XYIntSystem Constants::SCREEN_SIZE_LOGICAL = XYIntSystem(1920, 1080);

const int Constants::FONT_SIZE = 64;
const double Constants::REGULAR_HEIGHT = 30;

//Camera
const XYSystem Constants::CAMERA_CENTER_INIT = XYSystem(Constants::SCREEN_SIZE_LOGICAL.x / 2.0, Constants::SCREEN_SIZE_LOGICAL.y / 2.0);

const unsigned int Constants::TEMP_CACHE_SIZE = 100;

//Changeable defaults
/*const Uint8 SHIP_COLORS_DEFAULT[4][3];
const int STAR_COUNT_DEFAULT;
const int PLAYER_COUNT_DEFAULT; // for use in settings menu
const int CONTROLS_DEFAULT[4][4];
const double FONT_SIZE_DEFAULT;
const WindowMode WINDOW_MODE_DEFAULT;*/
const Settings Constants::SETTINGS_DEFAULT = {
	{{255, 0, 255, 255},
	{255, 0, 0, 255},
	{0, 0, 255, 255},
	{0, 255, 0, 255}}, // Ship colors
	1, // Player count
	{{-1, -1, -1, -1},
	{SDLK_a, SDLK_s, SDLK_w, SDLK_d},
	{SDLK_LEFT, SDLK_DOWN, SDLK_UP, SDLK_RIGHT},
	{SDLK_j, SDLK_k, SDLK_i, SDLK_l}}, //default controls
	true, //silly sounds
	{255, 255, 255, 255}, // font color
	WindowMode::FULLSCREEN_BORDERLESS, // fullscreen mode
	{1280, 720}, // resolution
	60, //fps
	{1, 1, 1, 1}, //sens multipliers
	{0, 0, 0, 255}, //background color
	false, //vsync
	SDL_MIX_MAXVOLUME / 2, //music volume
	SDL_MIX_MAXVOLUME / 2 //sound volume
};
const SettingsVerification Constants::SETTINGS_VERIFIED = {  16, 1, 16, 1, 4, 1, 2, 1, 4, 4, 1, 1, 1 };
const SettingsVerification Constants::SETTINGS_UNVERIFIED = { 0, 0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

const SaveData Constants::SAVE_DATA_EMPTY = { {0, 0, 0, 0, 0, 0} };
const SaveDataVerification Constants::SAVE_DATA_UNVERIFIED = { 0 };
const SaveDataVerification Constants::SAVE_DATA_VERIFIED = { 6 };

const char Constants::SAVE_DATA_DELIM = '(';
const int Constants::SAVE_DATA_JIBBERISH_RATE = 3;
const int Constants::SAVE_DATA_ENCRYPTION_MULTIPLIER1 = 3;
const int Constants::SAVE_DATA_ENCRYPTION_MULTIPLIER2 = 4;
const int Constants::SAVE_DATA_ENCRYPTION_CONSTANT = 104;
const int Constants::SAVE_DATA_KEY[3] = { 236, 74, 85 };

const int Constants::SCORE_LIMIT = 10000;


//Colors
const Uint8 Constants::FULL_COLOR[4] = { 255, 255, 255, 255 };
const Uint8 Constants::NO_COLOR[4] = { 0, 0, 0, 0 };
const Uint8 Constants::DIM_COLOR_MENU[4] = { 255, 255, 255, 128 };
const Uint8 Constants::DIM_COLOR_LOSE[4] = { 255, 255, 255, 200 };

const Uint8 Constants::TIMER_INCOMPLETE[4] = { 255, 200, 200, 255 };
const Uint8 Constants::TIMER_COMPLETE[4] = { 0, 255, 0, 255 };


//Stars
/*const double Constants::STAR_MIN_NONTWINKLE_TIME = 40;
const double Constants::STAR_MAX_NONTWINKLE_TIME = 5000;
const double Constants::STAR_TWINKLE_TIME = 50;
const double Constants::STAR_SIZE = 4;
*/
const double Constants::STAR_SIZE_TWINKLE_MOD_MIN = 1.1;
const double Constants::STAR_SIZE_TWINKLE_MOD_MAX = 2;
const int Constants::STAR_TWINKLE_TIME_MIN = 5;
const int Constants::STAR_TWINKLE_TIME_MAX = 25;
const int Constants::STAR_NONTWINKLE_TIME_MIN = 30;
const int Constants::STAR_NONTWINKLE_TIME_MAX = 300;

//Powerups
const double Constants::POWERUP_RADIUS = 8;
const double Constants::POWERUP_MASS = 0;
const double Constants::POWERUP_SPEED_MIN = -3;
const double Constants::POWERUP_SPEED_MAX = 1;
const double Constants::POWERUP_SPEED_MULTIPLIER = 1;
const double Constants::POWERUP_SPEED_BASE = 2;
const std::string Constants::POWERUP_PATH_PREFIX = "textures/";
const std::string Constants::POWERUP_PATHS[(int)PowerupType::LAST] = { "star", "shield_powerup", "bomb_powerup", "timer_powerup", "" };
const std::string Constants::POWERUP_PATH_SUFFIX = ".bmp";
const double Constants::EXPLOSION_RADIUS_MULTIPLIER = 30;
const std::string Constants::EXPLOSION_PATH_PREFIX = "textures/";
const std::string Constants::EXPLOSION_PATH = "player";
const std::string Constants::EXPLOSION_PATH_SUFFIX = ".bmp";
const double Constants::SLOWMO_RADIUS_MULTIPLIER = 30;
const std::string Constants::SLOWMO_PATH_PREFIX = "textures/";
const std::string Constants::SLOWMO_PATH = "shield";
const std::string Constants::SLOWMO_PATH_SUFFIX = ".bmp";
const std::string Constants::GUN_PATH_PREFIX = "textures/";
const std::string Constants::GUN_PATH = "dot";
const std::string Constants::GUN_PATH_SUFFIX = ".bmp";
const XYSystem Constants::GUN_SIZE(5, 2);
const int Constants::GUN_DIRECTIONS = 8;
const Uint8 Constants::GUN_COLOR[4] = { 128, 128, 128, 255 };
const int Constants::GUN_FIRE_RATE = 60;
const XYSystem Constants::BULLET_SIZE(5, 2);
const double Constants::BULLET_SPEED = 3;
const std::string Constants::BULLET_PATH_PREFIX = "textures/";
const std::string Constants::BULLET_PATH = "dot";
const std::string Constants::BULLET_PATH_SUFFIX = ".bmp";

//Comets
const double Constants::COMET_RADIUS = 9;
const double Constants::COMET_MASS = 8;
const double Constants::COMET_RADIUS_GIANT = 128;
const double Constants::COMET_MASS_GIANT = 704;
const double Constants::COMET_GIANT_CHANCE = 2.2; // 0-1000
const double Constants::COMET_GIANT_SPEED_MP = 0.6;
const int Constants::COMET_SPAWN_ATTEMPTS_PER_TICK = 3;
const std::string Constants::COMET_PATH_PREFIX = "textures/";
const std::string Constants::COMET_PATH = "comet";
const std::string Constants::COMET_PATH_SUFFIX = ".bmp";

//Players
const double Constants::PLAYER_RADIUS = 4;
const double Constants::PLAYER_MASS = 4;
const double Constants::PLAYER_SPEED_HORIZ = 6;
const double Constants::PLAYER_SPEED_VERT = 6;
const std::string Constants::PLAYER_PATH_PREFIX = "textures/";
const std::string Constants::PLAYER_PATH = "player";
const std::string Constants::PLAYER_PATH_SUFFIX = ".bmp";

const std::string Constants::SHIELD_PATH_PREFIX = "textures/";
const std::string Constants::SHIELD_PATH = "shield";
const std::string Constants::SHIELD_PATH_SUFFIX = ".bmp";
const int Constants::SHIELD_RADIUS = 10;

//Difficulty Dependent
const int Constants::COMET_COUNT_INIT[7] = { 5, 10, 15, 20, 25, 25, 30 };
const int Constants::COMET_SPAWN_RATES[7] = { 7, 4, 4, 3, 3, 3, 2 };
const double Constants::COMET_SPEED_MIN[7] = { 1, 0.8, 0.9, 1.05, 1.2, 1.4, 1.6 };
const double Constants::COMET_SPEED_MAX[7] = { 2, 1.3, 1.4, 1.6, 1.8, 2.0, 2.2 };
const double Constants::COMET_SPEED_MULTIPLIER[7] = { 2, 2, 2, 2, 2, 2, 2 };
const double Constants::ZOOM_LEVELS[7] = { 1, 1.3, 1.3, 1.3, 1.6, 1.6, 1.6 };
const int Constants::POWERUP_RATES[7] = { 0, 450, 425, 400, 325, 300, 275 };

//Music
const std::string Constants::MUSIC_NAMES[7] = { "menu", "1", "2", "3", "4", "5", "6" };
const std::vector<double> Constants::MUSIC_START_TIMES[7] = {
	{0}, // main menu
	{0, 12, 24, 36, 43.5, 55.5, 67.5, 79.5}, // jitterbug, 82.5 secs long
	{0, 6.0/7.0, 102.0/7.0, 192.0/7.0, 246.0/7.0, 342.0/7.0}, // jazzsambacat, 
	{0, 12, 24, 36, 48, 60, 72, 84}, // banjo - len=96
	{0, 11.2, 21.8, 33, 44.8, 56.2, 60.8, 73.8, 84.4}, // marimba - len=96.2
	{0, 12, 18, 24, 36, 48, 55.5, 61.5}, // song2
	{0, 900.0 / 61.0, 1800.0 / 61.0, 2700.0 / 61.0, 3240.0 / 61.0, 4140.0 / 61.0, 5040.0 / 61.0, 5850.0 / 61.0} // Exocomets_Remix - len=6300.0 / 61.0
};
const std::string Constants::MUSIC_PATH_PREFIX = "music/";
const std::string Constants::MUSIC_PATHS[7] = { "Mainmenufinal", "Jitterbug", "Jazz Samba Cat", "BanjoSong", "MarimbaFest", "song2", "Exocomets_Remix" };
const std::string Constants::MUSIC_PATH_SUFFIX = ".ogg";
const double Constants::MUSIC_BPMS[7] = { 60, 60, 60, 60, 60, 60, 60 };
const double Constants::MUSIC_OFFSETS[7] = { 0, 0, 0, 0, 0, 0, 0 };
const int Constants::MUSIC_COUNT = 7;

const double Constants::MUSIC_START_JUMP = 0.04;

							  //Sound
const std::string Constants::SOUND_EFFECT_NAMES[] = { "explosion", "click", "heartbeat", "fireworks0", "fireworks1", "fireworks2" };
const std::string Constants::SOUND_EFFECT_PATH_PREFIX = "sounds/";
const std::string Constants::SOUND_EFFECT_PATHS[] = { "explosion", "click", "heartbeat", "fireworks0", "fireworks1", "fireworks2" };
const std::string Constants::SOUND_EFFECT_PATH_SUFFIX = ".wav";
const int Constants::SOUND_EFFECT_COUNT = 6; //6

									 //Font
const std::string Constants::FONT_PATH_PREFIX = "Overpass_Mono/";
const std::string Constants::FONT_PATH = "OverpassMono-Regular";
const std::string Constants::FONT_PATH_SUFFIX = ".ttf";

//Other Paths
#ifdef __APPLE__
#include "MacManager.h"
const std::string Constants::SETTINGS_PATH_PREFIX = MacManager::getPathOfExecutableWithSlash();
const std::string Constants::SAVE_DATA_PATH_PREFIX = MacManager::getPathOfExecutableWithSlash();
#else
const std::string Constants::SETTINGS_PATH_PREFIX = "";
const std::string Constants::SAVE_DATA_PATH_PREFIX = "";
#endif
const std::string Constants::SETTINGS_PATH = "settings";
const std::string Constants::SAVE_DATA_PATH = "savedata";
const std::string Constants::SETTINGS_PATH_SUFFIX = ".ini";
const std::string Constants::SAVE_DATA_PATH_SUFFIX = ".dat";
const std::string Constants::PATH_BASE_LOCATION = "resources/";
const std::string Constants::ARROW_PATH_PREFIX = "textures/";
const std::string Constants::ARROW_PATH = "arrow2";
const std::string Constants::ARROW_PATH_SUFFIX = ".bmp";
const XYSystem Constants::ARROW_SIZE(64, 45);

//Settings
const char Constants::SETTINGS_NAME_VAL_DELIM = '=';
const char Constants::SETTINGS_VAL_NAME_DELIM = '\n';
const double Constants::KEY_OPTION_RADIUS = 46;

//Entities
const int Constants::ENTITY_COUNT_MAX = 100;
const double Constants::SLIDER_BAR_HORIZ_Y = 3;
const XYSystem Constants::SLIDER_BAR_VERT(3, 20);
const XYSystem Constants::DOT_SIZE(6, 22);
const std::string Constants::DOT_PATH_PREFIX = "textures/";
const std::string Constants::DOT_PATH = "dot";
const std::string Constants::DOT_PATH_SUFFIX = ".bmp";

const std::vector<XYIntSystem> Constants::RESOLUTIONS = {
	{1024, 768},
	{1280, 720},
	{1280, 800},
	{1280, 1024},
	{1366, 768},
	{1440, 900},
	{1600, 900},
	{1680, 1050},
	{1920, 1080},
	{1920, 1200},
	{2560, 1440},
	{3840, 2160},
	{7680, 4320}
};

const double Constants::LEADERBOARD_ENTRY_HEIGHT = 54;
const int Constants::LEADERBOARD_ENTRY_COUNT = 15;


//Steam
const int Constants::ACHIEVEMENTS_EMPTY[(int)Achievement::LAST] = {
	0, //BEAT_LVL_1,
	0, //BEAT_LVL_2,
	0, //BEAT_LVL_3,
	0, //BEAT_LVL_4,
	0, //BEAT_LVL_5,
	0, //BEAT_LVL_6,
	0, //STARPOWER_1000_NO_POWERUP,
	0, //BEAT_4P_GAME,
	0, //REVIVE_PLAYER,
	0, //LOSE_10000_TIMES,
	0, //RESPECT
	0, //COLLECT_100_STARS,
	0, //COLLECT_100_SHIELDS,
	0, //COLLECT_100_BOMBS,
	0, //COLLECT_100_TIMERS,
	0, //COLLECT_100_GUNS,
};
const int Constants::ACHIEVEMENTS_COMPLETED[(int)Achievement::LAST] = {
	1, //BEAT_LVL_1,
	1, //BEAT_LVL_2,
	1, //BEAT_LVL_3,
	1, //BEAT_LVL_4,
	1, //BEAT_LVL_5,
	1, //BEAT_LVL_6,
	1, //STARPOWER_1000_NO_POWERUP,
	1, //BEAT_4P_GAME,
	1, //REVIVE_PLAYER,
	1000, //LOSE_1000_TIMES,
	1, //RESPECT
	100, //COLLECT_100_STARS,
	100, //COLLECT_100_SHIELDS,
	100, //COLLECT_100_BOMBS,
	100, //COLLECT_100_TIMERS,
	100, //COLLECT_100_GUNS,
};
const std::string Constants::STAT_PREFIX = "STAT_";

