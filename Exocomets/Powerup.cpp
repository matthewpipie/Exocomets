#include "Powerup.h"
#include "RandomNumberManager.h"
#include "MainGame.h"
#include "Camera.h"

Powerup::Powerup(std::shared_ptr<Camera> camera, PowerupType pType) :
	Powerup(camera, pType, Difficulty::MAIN_MENU)
{}
Powerup::Powerup(std::shared_ptr<Camera> camera, PowerupType pType, Difficulty diff) :
	CollidableEntity(XYSystem(), Constants::POWERUP_RADIUS, Constants::POWERUP_MASS, XYSystem()),
	powerupType(pType)
{
	location = generateLocation(camera);
	speed = generateSpeed(camera);
	potentialLocation = location + speed;
	Uint8 red = 255;
	Uint8 green = 255;
	Uint8 blue = 255;
	Uint8 alpha = 255;
	switch (pType)
	{
	case PowerupType::STAR:
		break;
	case PowerupType::SHIELD:
		red = 63;
		blue = 63;
		break;
	case PowerupType::BOMB:
		red = 96;
		green = 96;
		break;
	case PowerupType::TIMER:
		green = 96;
		blue = 128;
		break;
	case PowerupType::GUN:
		//red = Constants::GUN_COLOR[0];
		//green = Constants::GUN_COLOR[1];
		//blue = Constants::GUN_COLOR[2];
		//alpha = Constants::GUN_COLOR[3];
		green = 165;
		blue = 0;
		break;
	case PowerupType::LAST:
		break;
	default:
		break;
	}
	Uint8 colorMod[4] = { red, green, blue, alpha };
	if (pType != PowerupType::GUN) {
		spriteBatch->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::POWERUP_PATH_PREFIX + Constants::POWERUP_PATHS[(int)pType] + Constants::POWERUP_PATH_SUFFIX, XYSystem(), XYSystem(2 * Constants::POWERUP_RADIUS), Direction(), false, false, colorMod, false));
		if (diff > Difficulty::LEVEL_3) {
			double rotPerFrame = RandomNumberManager::getInstance()->generateRandomDouble(-0.02, 0.02, RandomType::powerupRotation);
			spriteBatch->sprites.back()->everyRender = [&o = spriteBatch->sprites.back(), rotPerFrame]() {o->rotation = o->rotation.rotateRight(rotPerFrame); };

			spriteBatch->sprites.back()->rotation = RandomNumberManager::getInstance()->generateRandomDouble(-M_PI, M_PI, RandomType::powerupRotation);
		}
	}
	if (pType == PowerupType::GUN) {
		for (int i = 0; i < Constants::GUN_DIRECTIONS; i++) {
			double rad = (double)i / (double)Constants::GUN_DIRECTIONS * 2.0 * M_PI;
			spriteBatch->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::GUN_PATH_PREFIX + Constants::GUN_PATH + Constants::GUN_PATH_SUFFIX, XYSystem::generateCoordinates(rad, Constants::PLAYER_RADIUS), Constants::GUN_SIZE, Direction(rad), false, false, colorMod, false));
		}
	}
	//if (pType == PowerupType::REPEL) {
		//spriteBatch->RGBAColorMod[1] = 128;
	//}
}

Powerup::Powerup(XYSystem loc, XYSystem speed, PowerupType pType) :
	CollidableEntity(loc, Constants::POWERUP_RADIUS, Constants::POWERUP_MASS, speed),
	powerupType(pType)
{
	if (pType != PowerupType::GUN) {
		spriteBatch->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::POWERUP_PATH_PREFIX + Constants::POWERUP_PATHS[(int)pType] + Constants::POWERUP_PATH_SUFFIX, XYSystem(2 * Constants::POWERUP_RADIUS)));
	}
	else {
		for (int i = 0; i < Constants::GUN_DIRECTIONS; i++) {
			double rad = (double)i / (double)Constants::GUN_DIRECTIONS * 2.0 * M_PI;
			spriteBatch->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::GUN_PATH_PREFIX + Constants::GUN_PATH + Constants::GUN_PATH_SUFFIX, XYSystem::generateCoordinates(rad, Constants::PLAYER_RADIUS), Constants::GUN_SIZE, Direction(rad), false, false, Constants::GUN_COLOR, false));
		}
	}
}

Powerup::Powerup(PowerupType pType) :
	CollidableEntity(XYSystem(), Constants::POWERUP_RADIUS, Constants::POWERUP_MASS, XYSystem()),
	powerupType(pType)
{
}


Powerup::~Powerup()
{
}

void Powerup::resolvePotentialCollision(CollidableEntity &other) {
	other.resolvePotentialCollision(*this);
}

void Powerup::resolvePotentialCollision(Player &other) {
	other.resolvePotentialCollision(*this);
}

XYSystem Powerup::generateLocation(std::shared_ptr<Camera> camera)
{
	int xOrYFirst = RandomNumberManager::getInstance()->generateRandomInt(0, 2, RandomType::generateXOrY);
	XYSystem ret;
	ret.x = RandomNumberManager::getInstance()->generateRandomDouble(camera->getBottomLeft().x, camera->getTopRight().x, RandomType::locationX);
	ret.y = RandomNumberManager::getInstance()->generateRandomDouble(camera->getBottomLeft().y, camera->getTopRight().y, RandomType::locationY);
	if (xOrYFirst == 0) {
		//generate x first
		if (ret.y < camera->getCameraCenter().y) {
			ret.y = camera->getBottomLeft().y - radius;
		}
		else {
			ret.y = camera->getTopRight().y + radius;
		}
	}
	else {
		if (ret.x < camera->getCameraCenter().x) {
			ret.x = camera->getBottomLeft().x - radius;
		}
		else {
			ret.x = camera->getTopRight().x + radius;
		}
	}
	return ret;
}
XYSystem Powerup::generateSpeed(std::shared_ptr<Camera> camera)
{
	XYSystem result;
	result.x = std::pow(Constants::POWERUP_SPEED_BASE, RandomNumberManager::getInstance()->generateRandomDouble(Constants::POWERUP_SPEED_MIN, Constants::POWERUP_SPEED_MAX, RandomType::speedX) * Constants::POWERUP_SPEED_MULTIPLIER);
	result.y = std::pow(Constants::POWERUP_SPEED_BASE, RandomNumberManager::getInstance()->generateRandomDouble(Constants::POWERUP_SPEED_MIN, Constants::POWERUP_SPEED_MAX, RandomType::speedY) * Constants::POWERUP_SPEED_MULTIPLIER);
	result.x *= RandomNumberManager::getInstance()->generateRandomInt(0, 2, RandomType::isSpeedXNegative) == 0 ? -1 : 1;
	result.y *= RandomNumberManager::getInstance()->generateRandomInt(0, 2, RandomType::isSpeedYNegative) == 0 ? -1 : 1;
	if ((location.x > camera->getTopRight().x && result.x > 0) || (location.x < camera->getBottomLeft().x && result.x < 0)) {
		result.x *= -1;
	}
	else if ((location.y > camera->getTopRight().y && result.y > 0) || (location.y < camera->getBottomLeft().y && result.y < 0)) {
		result.y *= -1;
	}
	return result;
}
