#pragma once
#include "CollidableEntity.h"
#include "Player.h"
#include "Camera.h"
class Comet :
	public CollidableEntity
{
public:
	Comet(std::shared_ptr<Camera> camera, Difficulty cometDifficulty, double radius, double mass, XYSystem speedMP);
	Comet(XYSystem location, XYSystem speed, Difficulty cometDifficulty, double radius, double mass);
	~Comet();
	double getPostCollisionFraction(double v1, double v2, double m1, double m2, double t1, double t2, double p);
	double getPostCollisionXSpeed(double v1, double v2, double m1, double m2, double t1, double t2, double p);
	double getPostCollisionYSpeed(double v1, double v2, double m1, double m2, double t1, double t2, double p);

	virtual void resolvePotentialCollision(CollidableEntity &other) override;
	void resolvePotentialCollision(Comet &other) override;
	void resolvePotentialCollision(Player &other) override;


private:
	XYSystem generateCometSpeed(std::shared_ptr<Camera> camera, Difficulty cometDifficulty, XYSystem speedMP);
	XYSystem generateOffscreenLocation(std::shared_ptr<Camera> camera);
	Difficulty _cometDifficulty;
	void addCometSprite();
};

