#pragma once
#include "Scene.h"
class MainMenuScene :
	public Scene
{
public:
	MainMenuScene(std::shared_ptr<SDLRenderer> rnd);
	~MainMenuScene();
	void init() override;
	void deinit() override;
protected:
	void customEveryTick() override;
};

