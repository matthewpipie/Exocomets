#include "SpriteBatch.h"



SpriteBatch::SpriteBatch() :
	location(0, 0),
	rotation(0),
	zindex(1)
{
	int size = sizeof(RGBAColorMod) / sizeof(RGBAColorMod[0]);
	for (int i = 0; i < size; i++) {
		RGBAColorMod[i] = 255;
	}
}


SpriteBatch::~SpriteBatch()
{
}

void SpriteBatch::updateSprites()
{
	for (auto it : sprites) {
		it->everyRender();
	}
}
