#pragma once
#include "stdafx.h"
#include "Constants.h"
class Direction
{
public:
	Direction();
	Direction(double rads);
	Direction(XYSystem start, XYSystem end);
	~Direction();
	double radians;
	double degrees();
	static Direction makeFromDegs(double degs);
	Direction opposite();
	Direction rotateLeftDegrees(double degs);
	Direction rotateRightDegrees(double degs);
	Direction rotateLeftRadians(double rads);
	Direction rotateRightRadians(double rads);
	Direction rotateLeft(Direction amount);
	Direction rotateRight(Direction amount);
	XYSystem polarToCartesian(double distance);
	//Direction operator=(const Direction &O);
private:
	double getAngleRadians(XYSystem start, XYSystem end);
	double rad(double degs);
	// The constraints of radians are defined here
	double fixRadians(double rads);
	// 1 for under, 0 for good, -1 for over
	int getRadState(double rads);
};
