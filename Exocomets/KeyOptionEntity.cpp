#include "KeyOptionEntity.h"
#include "Player.h"
#include "InputManager.h"
#include "SettingsManager.h"


KeyOptionEntity::KeyOptionEntity(XYSystem loc) :
	CollidableEntity(loc, Constants::KEY_OPTION_RADIUS, 0, XYSystem()),
	value(0),
	_wantsInput(false)
{
	spriteBatch->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::SHIELD_PATH_PREFIX + Constants::SHIELD_PATH + Constants::SHIELD_PATH_SUFFIX, XYSystem(radius * 2.0)));
	spriteBatch->sprites.push_back(std::make_shared<Sprite>("", XYSystem(), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
}


KeyOptionEntity::~KeyOptionEntity()
{
}

void KeyOptionEntity::resolvePotentialCollision(CollidableEntity &other) {
	other.resolvePotentialCollision(*this);
}

void KeyOptionEntity::resolvePotentialCollision(Player &other) {
	if (isActive) {
		if (!grace) {
			_wantsInput = true;
		}
	}
	else _wantsInput = false;
	didCollideLastFrame = true;
}

void KeyOptionEntity::calculateCustomPotentialLocation()
{
	if (_wantsInput) {
		int lastPressed = InputManager::getInstance()->getLatestPressedKey();
		if (InputManager::getInstance()->isKeyJustDown(SDLK_ESCAPE)) {
			_wantsInput = false;
			InputManager::getInstance()->eatKey(SDLK_ESCAPE);
		}
		else if (lastPressed != 0) {
			value = lastPressed;
			_wantsInput = false;
		}
		spriteBatch->sprites[1]->updateText("...", Constants::REGULAR_HEIGHT);
	}
	else {
		spriteBatch->sprites[1]->updateText(InputManager::getInstance()->getDisplayNameFromKey(value), Constants::REGULAR_HEIGHT);
	}
	grace = didCollideLastFrame;
	didCollideLastFrame = false;
}
