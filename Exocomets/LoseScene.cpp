#include "LoseScene.h"
#include "InputManager.h"
#include "SettingsManager.h"
#include "MainGame.h"
#include "SDLTextCreator.h"
#include "SaveDataManager.h"

LoseScene::LoseScene(std::shared_ptr<SDLRenderer> rnd, int finalPlayerScores[4], Difficulty diff) :
	Scene(rnd, diff),
	_canBecomeDestroyedTick(_createdTick + Constants::GAME_PLAYAGAIN_COOLDOWN)
{
	for (int i = 0; i < 4; i++) {
		playerScores[i] = finalPlayerScores[i];
	}
	entityBatches.push_back(std::make_shared<EntityBatch>());
	spriteBatches.push_back(std::make_shared<SpriteBatch>());

	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>("Level " + std::to_string((int)diff), XYSystem(960, 900), Constants::REGULAR_HEIGHT * 1.5, Direction(), false, false, Constants::FULL_COLOR, true));
	Settings set = SettingsManager::getSettings();
	Uint8 colors[4][4];
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			colors[i][j] = set.shipColors[i][j];
		}
	}
	switch (set.playerCount) {
	case 1:

		spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>("Score: " + std::to_string((int)((double)playerScores[0] / Constants::TICK_RATE * Constants::SCORE_MULTIPLIER)), XYSystem(960, 540), Constants::REGULAR_HEIGHT * 1.5, Direction(), false, false, colors[0], true));
		entityBatches[0]->entities.push_back(std::make_shared<Player>(0,                                                                                     camera->convertHudToReal(XYSystem(960, 600))));
		break;
	case 2:
		spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>("Score: " + std::to_string((int)((double)playerScores[0] / Constants::TICK_RATE * Constants::SCORE_MULTIPLIER)), XYSystem(760, 540), Constants::REGULAR_HEIGHT * 1.5, Direction(), false, false, colors[0], true));
		entityBatches[0]->entities.push_back(std::make_shared<Player>(0,                                                                                     camera->convertHudToReal(XYSystem(760, 600))));
		spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>("Score: " + std::to_string((int)((double)playerScores[1] / Constants::TICK_RATE * Constants::SCORE_MULTIPLIER)), XYSystem(1160, 540), Constants::REGULAR_HEIGHT * 1.5, Direction(), false, false, colors[1], true));
		entityBatches[0]->entities.push_back(std::make_shared<Player>(1,                                                                                     camera->convertHudToReal(XYSystem(1160, 600))));
		break;
	case 3:
		spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>("Score: " + std::to_string((int)((double)playerScores[0] / Constants::TICK_RATE * Constants::SCORE_MULTIPLIER)), XYSystem(560, 540), Constants::REGULAR_HEIGHT * 1.5, Direction(), false, false, colors[0], true));
		entityBatches[0]->entities.push_back(std::make_shared<Player>(0,                                                                                     camera->convertHudToReal(XYSystem(560, 600))));
		spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>("Score: " + std::to_string((int)((double)playerScores[1] / Constants::TICK_RATE * Constants::SCORE_MULTIPLIER)), XYSystem(960, 540), Constants::REGULAR_HEIGHT * 1.5, Direction(), false, false, colors[1], true));
		entityBatches[0]->entities.push_back(std::make_shared<Player>(1,                                                                                     camera->convertHudToReal(XYSystem(960, 600))));
		spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>("Score: " + std::to_string((int)((double)playerScores[2] / Constants::TICK_RATE * Constants::SCORE_MULTIPLIER)), XYSystem(1360, 540), Constants::REGULAR_HEIGHT * 1.5, Direction(), false, false, colors[2], true));
		entityBatches[0]->entities.push_back(std::make_shared<Player>(2,                                                                                     camera->convertHudToReal(XYSystem(1360, 600))));
		break;
	case 4:
		spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>("Score: " + std::to_string((int)((double)playerScores[0] / Constants::TICK_RATE * Constants::SCORE_MULTIPLIER)), XYSystem(360, 540), Constants::REGULAR_HEIGHT * 1.5, Direction(), false, false, colors[0], true));
		entityBatches[0]->entities.push_back(std::make_shared<Player>(0,                                                                                     camera->convertHudToReal(XYSystem(360, 600))));
		spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>("Score: " + std::to_string((int)((double)playerScores[1] / Constants::TICK_RATE * Constants::SCORE_MULTIPLIER)), XYSystem(760, 540), Constants::REGULAR_HEIGHT * 1.5, Direction(), false, false, colors[1], true));
		entityBatches[0]->entities.push_back(std::make_shared<Player>(1,                                                                                     camera->convertHudToReal(XYSystem(760, 600))));
		spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>("Score: " + std::to_string((int)((double)playerScores[2] / Constants::TICK_RATE * Constants::SCORE_MULTIPLIER)), XYSystem(1160, 540), Constants::REGULAR_HEIGHT * 1.5, Direction(), false, false, colors[2], true));
		entityBatches[0]->entities.push_back(std::make_shared<Player>(2,                                                                                     camera->convertHudToReal(XYSystem(1160, 600))));
		spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>("Score: " + std::to_string((int)((double)playerScores[3] / Constants::TICK_RATE * Constants::SCORE_MULTIPLIER)), XYSystem(1560, 540), Constants::REGULAR_HEIGHT * 1.5, Direction(), false, false, colors[3], true));
		entityBatches[0]->entities.push_back(std::make_shared<Player>(3,                                                                                     camera->convertHudToReal(XYSystem(1560, 600))));
		break;
	}
	int topScore = 0;
	for (int i = 0; i < set.playerCount; i++) {
		if (playerScores[i] > topScore) {
			topScore = playerScores[i];
		}
	}
	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>("Best: " + std::to_string(SaveDataManager::submitScore(difficulty, (int)((double)topScore / Constants::TICK_RATE * Constants::SCORE_MULTIPLIER))), XYSystem(960, 810), Constants::REGULAR_HEIGHT * 1.5, Direction(), false, false, Constants::FULL_COLOR, true));


	entityBatches[1]->entities.push_back(std::make_shared<RectangleEntity>(XYSystem(760, 270), SDLTextCreator::getTextSize("Retry (click/space)", Constants::REGULAR_HEIGHT).getSizeVector(), 0, 0));
	entityBatches[1]->entities[0]->spriteBatch->sprites.push_back(std::make_shared<Sprite>("Retry (click/space)", XYSystem(), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));

	entityBatches[1]->entities.push_back(std::make_shared<RectangleEntity>(XYSystem(1160, 270), SDLTextCreator::getTextSize("Back (escape)", Constants::REGULAR_HEIGHT).getSizeVector(), 0, 0));
	entityBatches[1]->entities[1]->spriteBatch->sprites.push_back(std::make_shared<Sprite>("Back (escape)", XYSystem(), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));

	//spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>("Press space to try again or escape to exit.", XYSystem(960, 270), Constants::REGULAR_HEIGHT * 1.5, Direction(), false, false, Constants::FULL_COLOR, true));
}


LoseScene::~LoseScene()
{
}

void LoseScene::init() {
	SDLMusicManager::getInstance()->pauseMusic();
}

void LoseScene::customEveryTick() {
	if (MainGame::getTickCount() >= _canBecomeDestroyedTick) {
		if (InputManager::getInstance()->isKeyDown(SDLK_SPACE)) {
			action = SceneAction::DESTROY_SCENE;
			eatKeys = false;
		}
		if (InputManager::getInstance()->isMouseDown()) {
			action = SceneAction::DESTROY_SCENE;
			eatKeys = false;
		}
	}
	entityBatches[1]->entities[0]->calculatePotentialLocation();
	if (entityBatches[1]->entities[0]->isPotentiallyColliding(*entityBatches[0]->entities[0])) {
		if (SettingsManager::getSettings().controls[0][0] != -1 || InputManager::getInstance()->isMouseDown()) {
			action = SceneAction::DESTROY_SCENE;
			eatKeys = false;
		}
	}
	entityBatches[1]->entities[1]->calculatePotentialLocation();
	if (entityBatches[1]->entities[1]->isPotentiallyColliding(*entityBatches[0]->entities[0])) {
		if (SettingsManager::getSettings().controls[0][0] != -1 || InputManager::getInstance()->isMouseDown()) {
			action = SceneAction::DESTROY_TWO_SCENES;
		}
	}
	if (InputManager::getInstance()->isKeyJustDown(SDLK_ESCAPE)) {
		action = SceneAction::DESTROY_TWO_SCENES;
		eatKeys = false;
	}
	
	//updateScoreSprite();
	/*if (InputManager::getInstance()->isKeyJustDown(SDLK_UP)) {
		camera->setZoom(XYSystem(1.1));
	}*/
}

void LoseScene::deinit() {
	//std::cout << "rip" << std::endl;
	//SDLMusicManager::getInstance()->resumeMusic();
}

bool LoseScene::doBackground()
{
	return false;
}

void LoseScene::updateScoreSprite() {
	std::shared_ptr<Sprite> spr = spriteBatches[0]->sprites[0];
	spr->filepathOrText = std::to_string(_score);
	spr->relativeLocation = camera->getTopRight();
	XYSystem size = SDLTextCreator::getTextSize(spr->filepathOrText.c_str(), Constants::REGULAR_HEIGHT) * 2;
	size *= camera->getSize().x / (double)Constants::SCREEN_SIZE_LOGICAL.x;
	spr->size = size;
	spr->relativeLocation.x -= size.x / 2.0;
	spr->relativeLocation.y -= size.y / 4.0;
}
