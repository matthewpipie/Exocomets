#include "SDLTextCreator.h"
#include "ErrorHandler.h"
#include "Constants.h"
#include "SettingsManager.h"

std::unordered_map<int, TTF_Font*> SDLTextCreator::_fontCache = {};

void SDLTextCreator::init()
{
	if (TTF_Init() == -1) {
		ErrorHandler::error(ErrorCode::TTF_INIT);
	};
}

void SDLTextCreator::unload() {
	for (auto it : _fontCache) {
		TTF_CloseFont(it.second);
	}
	TTF_Quit();
}

SDL_Texture* SDLTextCreator::makeText(SDL_Renderer *rnd, const char *text, int fontSize) {
	TTF_Font *font = getFontFromSize(fontSize);
	//Settings settings = SettingsManager::getSettings();
	SDL_Color color = { Constants::FULL_COLOR[0], Constants::FULL_COLOR[1], Constants::FULL_COLOR[2] };
	//SDL_Color black = { 0, 0, 0 };
	SDL_Surface *surface = TTF_RenderText_Blended(font, text, color);
	SDL_Texture *ret = SDL_CreateTextureFromSurface(rnd, surface);
	SDL_FreeSurface(surface);
	return ret;
}

XYSystem SDLTextCreator::getTextSize(const char *text, double height) {
	XYIntSystem ret;
	TTF_SizeText(getFontFromSize(Constants::FONT_SIZE), text, &ret.x, &ret.y);
	return XYSystem((double)ret.x / (double)ret.y * height, height);
}


TTF_Font* SDLTextCreator::getFontFromSize(int fontSize) {
	if (!_fontCache.count(fontSize)) {
		return _fontCache[fontSize] = createFont(fontSize);
	}
	else {
		return _fontCache[fontSize];
	}
}

TTF_Font* SDLTextCreator::createFont(int fontSize) {
	TTF_Font *temp = TTF_OpenFont((Constants::PATH_BASE_LOCATION + Constants::FONT_PATH_PREFIX + Constants::FONT_PATH + Constants::FONT_PATH_SUFFIX).c_str(), fontSize);
	if (temp == nullptr) {
		ErrorHandler::error(ErrorCode::TTF_FONT, std::to_string(fontSize));
	}
	return temp;
}