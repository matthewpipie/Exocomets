#pragma once
#include "stdafx.h"
#include "steam/steam_api.h"
#include "Constants.h"
#include "Leaderboard.h"

struct EnumClassHash
{
	template <typename T>
	std::size_t operator()(T t) const
	{
		return static_cast<std::size_t>(t);
	}
};

class SteamManager
{
public:

	static const std::unordered_map<Achievement, std::string, EnumClassHash> ACH_NAMES;
	static const std::unordered_map<Difficulty, std::string, EnumClassHash> LEADERBOARD_NAMES;

	const CGameID gameID;

	void everyTick();

	static std::shared_ptr<SteamManager> getInstance();
	static void init();
	static void deinit();

	STEAM_CALLBACK(SteamManager, onStatsReceived, UserStatsReceived_t, _callbackOnStatsReceived);
	STEAM_CALLBACK(SteamManager, onStatsStored, UserStatsStored_t, _callbackOnStatsStored);

	void progressAchievement(Achievement ach);

	void loadLeaderboard(Difficulty diff, LeaderboardType type, std::function<void(Leaderboard)> callback);
	void stopLoadingLB();
	void uploadScore(Difficulty diff, int score);
	Uint64 getSteamID();

	~SteamManager();
private:
	SteamManager();
	static std::shared_ptr<SteamManager> _staticInstance;

	static bool _enabled;
	bool _gotSteamStats;
	bool _updatedStatsWithoutSteam;
	void unlockAchievement(Achievement ach);
	int _achievements[(int)Achievement::LAST];

	//DEBUG
//public:

	ISteamUserStats *_steamUserStats;
//private:


	// Leaderboard stuffs
	std::function<void(Leaderboard)> _onLeaderboardsCallback;

	void onFindMainLeaderboard( LeaderboardFindResult_t *pFindLeaderboardResult, bool bIOFailure );
	void onDownloadMainLeaderboard(LeaderboardScoresDownloaded_t *pLeaderboardScoresDownloaded, bool bIOFailure);

	void onFindSecondaryLeaderboard( LeaderboardFindResult_t *pFindLeaderboardResult, bool bIOFailure );
	void onDownloadSecondaryLeaderboard(LeaderboardScoresDownloaded_t *pLeaderboardScoresDownloaded, bool bIOFailure);
	void finishDownload(std::vector<LeaderboardEntry_t> vec);

	CCallResult<SteamManager, LeaderboardFindResult_t> _steamCallResultCreateLeaderboard;
	CCallResult<SteamManager, LeaderboardScoresDownloaded_t> _steamCallResultDownloadEntries;

	std::vector<CSteamID> _entriesNeededID;
	std::vector<LeaderboardEntry> _entriesNeededLB;
	//std::unordered_map<CSteamID, LeaderboardEntry> _entriesNeeded;

	Difficulty _currentDiff;
	void getNextScores();

	Leaderboard _currentLB;

	bool _stopLoadingLB;
	bool _isLoadingLB;



	//Uploading leaderboard stuffs
	void onFindLeaderboardToUpload(LeaderboardFindResult_t *pFindLeaderboardResult, bool bIOFailure);
	void onUpload(LeaderboardScoreUploaded_t *p, bool failure);
	CCallResult<SteamManager, LeaderboardScoreUploaded_t> _steamCallResultUploadScore;
	//Difficulty _diffToUpload;
	int _scoreToUpload;

	std::vector<std::pair<Difficulty, int>> _uploadQueue;
	int _uploadedInPast10;
	int _next10MinuteIntervalTick;
	bool _isUploading;

	// Converting steamID to name:
	//void getNamesInLBEntries();

	//CCallResult<SteamManager, PersonaStateChange_t> _steamCallPersonaChange;
	//void onPersonaChange(PersonaStateChange_t *pPersonaChange, bool bIOFailure);
};

