#include "RandomNumberManager.h"

std::shared_ptr<RandomNumberManager> RandomNumberManager::_staticInstance = nullptr;

void RandomNumberManager::init(time_t seed) {
	_seed = seed;
	for (int i = 0; i < (int)RandomType::LAST; i++) {
		randomSeeds[i].seed((unsigned int)(_seed + i));
	}
}

double RandomNumberManager::generateRandomDouble(double min, double max, RandomType randomType) {
	if (min > max) {
		int a = 0;
	}
	std::uniform_real_distribution<double> dist(min, max);
	return dist(randomSeeds[(int)randomType]);
}

int RandomNumberManager::generateRandomInt(int minInclusive, int maxExclusive, RandomType randomType) {
	return (int)generateRandomDouble(minInclusive, maxExclusive, randomType);
}

std::shared_ptr<RandomNumberManager> RandomNumberManager::getInstance() {
	if (!_staticInstance)
		_staticInstance = std::shared_ptr<RandomNumberManager>(new RandomNumberManager());
	return _staticInstance;
}

time_t RandomNumberManager::getSeed() {
	return _seed;
}

RandomNumberManager::RandomNumberManager() {}
