#pragma once
#include "CollidableEntity.h"
#include "Comet.h"
class Player :
	public CollidableEntity
{
public:
	Player(int playerNumber, XYSystem location);
	Player(int playerNumber, XYSystem location, Difficulty diff);
	Player(int playerNumber, double initStarPower, XYSystem location, Difficulty diff);
	Player(int playerNumber, XYSystem location, bool revival, Difficulty diff);
	~Player();
	void calculateCustomPotentialLocation() override;

	virtual void resolvePotentialCollision(CollidableEntity &other) override;
	void resolvePotentialCollision(Comet &other) override;
	void resolvePotentialCollision(Player &other) override;
	void resolvePotentialCollision(Powerup &other) override;
	virtual bool isEssentialEntity() override;
	double starCount;
	virtual void clearAction() override;
	int getPlayerNumber();
private:
	bool _invincible;
	int _playerNumber;
	bool _hasGottenAPowerup;
	void addPlayerSprites();
	bool _hasPowerups[(int)PowerupType::LAST];
	void updateStarCount();
	bool removePowerup(PowerupType powerup);
};

