#include "Slider.h"


Slider::Slider(double minValue, double maxValue, XYSystem centralLoc, double size, bool isInt) :
	Slider(minValue, maxValue, centralLoc, size, isInt, false)
{}
Slider::Slider(double minValue, double maxValue, XYSystem centralLoc, double size, bool isInt, bool invis) :
	_location(centralLoc),
	_dotLocation(centralLoc),
	_minValue(minValue),
	_maxValue(maxValue),
	_size(size),
	_isInt(isInt),
	_invis(invis)
{
}


Slider::~Slider()
{
}


std::shared_ptr<RectangleEntity> Slider::getSliderDot() {
	std::shared_ptr<RectangleEntity> r(std::make_shared<RectangleEntity>(_dotLocation, getSliderDotHitbox(), 0, XYSystem()));
	r->spriteBatch->sprites.push_back(getDotSprite());
	return r;
}

void Slider::moveSlider(CollidableEntity &ent) {
	if (_invis) return;
	if (_isInt) {
		if (ent.potentialLocation.x - ent.location.x > 0) {
			setValue(getValue() + 1);
		}
		else {
			setValue(getValue() - 1);
		}
	}
	else {
		_dotLocation.x += ent.potentialLocation.x - ent.location.x;
	}
	_dotLocation = _dotLocation.fixInRange(XYSystem(_location.x - _size / 2.0, _dotLocation.y), XYSystem(_location.x + _size / 2.0, _dotLocation.y));
	ent.failPotentialLocation();
}

double Slider::getValue() {
	double ans = (_dotLocation.x - (_location.x - _size / 2.0)) / _size * (_maxValue - _minValue) + _minValue;
	if (_isInt) {
		return std::round(ans);
	}
	return ans;
}

void Slider::setValue(double newVal) {
	_dotLocation.x = ((newVal - _minValue) / (_maxValue - _minValue) - .5) * _size + _location.x;
}

std::vector<XYSystem> Slider::getSliderDotHitbox() {
	std::vector<XYSystem> ret;
	ret.push_back(XYSystem(Constants::DOT_SIZE.x / 2.0, Constants::DOT_SIZE.y / 2.0));
	ret.push_back(XYSystem(Constants::DOT_SIZE.x / 2.0, Constants::DOT_SIZE.y / -2.0));
	ret.push_back(XYSystem(Constants::DOT_SIZE.x / -2.0, Constants::DOT_SIZE.y / -2.0));
	ret.push_back(XYSystem(Constants::DOT_SIZE.x / -2.0, Constants::DOT_SIZE.y / 2.0));
	return ret;
}
std::shared_ptr<Sprite> Slider::getDotSprite() {
	return std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::DOT_PATH_PREFIX + Constants::DOT_PATH + Constants::DOT_PATH_SUFFIX, XYSystem(), Constants::DOT_SIZE, Direction(), false, false, _invis ? Constants::NO_COLOR : Constants::FULL_COLOR, false);
}

std::shared_ptr<SpriteBatch> Slider::getSliderSpriteBatch() {
	std::shared_ptr<SpriteBatch> sb = std::make_shared<SpriteBatch>();
	sb->location = _location;
	sb->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::DOT_PATH_PREFIX + Constants::DOT_PATH + Constants::DOT_PATH_SUFFIX, XYSystem(), XYSystem(_size + Constants::DOT_SIZE.x, Constants::SLIDER_BAR_HORIZ_Y), Direction(), false, false, _invis ? Constants::NO_COLOR : Constants::FULL_COLOR, false));
	sb->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::DOT_PATH_PREFIX + Constants::DOT_PATH + Constants::DOT_PATH_SUFFIX, XYSystem(_size / -2.0 - Constants::DOT_SIZE.x, 0), Constants::SLIDER_BAR_VERT, Direction(), false, false, _invis ? Constants::NO_COLOR : Constants::FULL_COLOR, false));
	sb->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::DOT_PATH_PREFIX + Constants::DOT_PATH + Constants::DOT_PATH_SUFFIX, XYSystem(_size / 2.0 + Constants::DOT_SIZE.x, 0), Constants::SLIDER_BAR_VERT, Direction(), false, false, _invis ? Constants::NO_COLOR : Constants::FULL_COLOR, false));
	return sb;
}
