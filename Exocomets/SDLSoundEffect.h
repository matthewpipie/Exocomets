#pragma once
#include "stdafx.h"
#include "SDL_mixerWrapper.h"
class SDLSoundEffect
{
public:
	SDLSoundEffect(std::string name, std::string path);
	~SDLSoundEffect();
	Mix_Chunk *sdlMixChunk;
	const std::string NAME;
	const std::string PATH;
private:
	Mix_Chunk *generateMixChunk(std::string path);
};

