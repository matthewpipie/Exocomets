// Exocomets.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "RandomNumberManager.h"
#include "SettingsManager.h"
#include "MainGame.h"
#include "SaveDataManager.h"
#include "SteamManager.h"

int main(int argc, char *argv[])
{
	std::cout << "initing steam..." << std::endl;
	SteamManager::init();

	//SteamManager::getInstance()->_steamUserStats->ResetAllStats(true);
	//return 0;

	std::cout << "initing rng..." << std::endl;
	RandomNumberManager::getInstance()->init(time(NULL));

	std::cout << "initing settings..." << std::endl;
	SettingsManager::init();

	std::cout << "initing savedata..." << std::endl;
	SaveDataManager::init();

	std::cout << "running game..." << std::endl;
	MainGame().run();

	std::cout << "quitting steam..." << std::endl;
	SteamManager::deinit();

	return 0;
}

