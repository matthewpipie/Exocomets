#include "CreditsScene.h"
#include "SettingsManager.h"
#include "MainGame.h"
#include "SteamManager.h"
#include "InputManager.h"
#include "SDLTextCreator.h"



CreditsScene::CreditsScene(std::shared_ptr<SDLRenderer> renderer) :
	Scene(renderer, Difficulty::MAIN_MENU)
{
	entityBatches.push_back(std::make_shared<EntityBatch>());
	spriteBatches.push_back(std::make_shared<SpriteBatch>());

	entityBatches[0]->entities.push_back(std::make_shared<Player>(0, camera->getCameraCenter()));

	entityBatches[1]->entities.push_back(std::make_shared<RectangleEntity>(XYSystem(960, 70), SDLTextCreator::getTextSize("Back", Constants::REGULAR_HEIGHT).getSizeVector(), 0, 0));
	entityBatches[1]->entities[0]->spriteBatch->sprites.push_back(std::make_shared<Sprite>("Back", XYSystem(), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, true));

	std::vector<std::string> credits;
	credits.push_back("Thank you so much for playing " + std::string(Constants::GAME_NAME) + "!");
	credits.push_back("");
	credits.push_back("Idea by:                    Matthew \"matthewpipie\" Giordano");
	credits.push_back("Programmer:                 Matthew \"matthewpipie\" Giordano");
	credits.push_back("Designer:                   Matthew \"matthewpipie\" Giordano");
	credits.push_back("");
	credits.push_back("Main Menu Music:                Mark \"marchimedes\" Trautner");
	credits.push_back("Level 1 Music:                Ethan \"ethanpiedude\" Pesikoff");
	credits.push_back("Level 2 Music:                Ethan \"ethanpiedude\" Pesikoff");
	credits.push_back("Level 3 Music:                Ethan \"ethanpiedude\" Pesikoff");
	credits.push_back("Level 4 Music:                Ethan \"ethanpiedude\" Pesikoff");
	credits.push_back("Level 5 Music:                  Andy \"KeyboardFire\" Tockman");
	credits.push_back("Level 6 Music:              Matthew \"matthewpipie\" Giordano");
	credits.push_back("");
	credits.push_back("");
	credits.push_back("");
	credits.push_back("Special thanks to my father for motivating me to pursue my dreams!");

	for (unsigned int i = 0; i < credits.size(); i++) {
		spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>(credits[i], XYSystem(960, 980 - 50 * i), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	}

}


CreditsScene::~CreditsScene()
{
}
//#include "SaveDataManager.h"
void CreditsScene::customEveryTick()
{
	if (InputManager::getInstance()->isKeyJustDown(SDLK_ESCAPE)) {
		action = SceneAction::DESTROY_SCENE;
		eatKeys = false;
	}
	if (SettingsManager::getSettings().controls[0][0] != -1 || InputManager::getInstance()->isMouseDown()) {
		if (entityBatches[0]->entities[0]->isPotentiallyColliding(*entityBatches[1]->entities[0])) {
			action = SceneAction::DESTROY_SCENE;
		}
	}
	if (_createdTick + Constants::TICK_RATE * 10.0 <= MainGame::getTickCount()) {
		SteamManager::getInstance()->progressAchievement(Achievement::RESPECT);
	}


	
	/*if (InputManager::getInstance()->isKeyJustDown(SDLK_z)) {
		SaveDataManager::submitScore(Difficulty::LEVEL_1, 10000);
		SaveDataManager::submitScore(Difficulty::LEVEL_2, 10000);
		SaveDataManager::submitScore(Difficulty::LEVEL_3, 10000);
	}*/
}
