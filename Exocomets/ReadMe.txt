EXOCOMETS by matthewpipie

TODO LIST


DONE:
* starcount above players
* players revive with explosion
* make the lose screen more self explanatory (ie "press space to try again")
* make font high quality by requiring height instead of font size
* think of something else for font size to be replaced by
* make volume quiet during last 10 secs
* scores per player
* make player starcount not intersect with shield
* make keyboarders move twice as fast to compensate for reduced tickrate
* turn menu into something pretty
* arrows for the options screen
* scenes for unlocking powerups
* make credits scene
* try better to make the text not super low quality
* make the order RGBA instead of ABGR in the options menu
* clean resources
* make left click retry, right click escape
* make level 6 get increasingly harder
* fix music not starting at right time
* make explosion thing go away faster
* make heartbeat sound for every second & camera zoom
* make sound get quieter betterer
* fix collision
* FIX SILLYSOUNDS
* FIX FRAMERATE
* FIX MUSIC
* MAKE GIANT COMET HAVE SLIGHTLY HIGHER MASS
* FIX FREAKING SOUND GLITCHYNESS ON MAIN MENU
* make it say "success" or something when u succeed

TODO:

fix woosh sound to happen on every button

* make option for star power font size

* REMOVE BLACK BORDER ON COMETS

* turn XYSystem and XYIntSystem into not stdafx.h


* fix bug where pressing default and back pseudo-saves (like music on 0 -> default -> back = music)
* make achievement for getting a less than 50 score?
* make textures 64x64 instead of 63x63
* leaderboards page
* "silly sounds" sounds and regular sounds for every powerup
    powerup | regular sound | silly sound
player deth : current boom    breeeow (going down in pitch)
	 shield : cling on loss   doink!
	   bomb : boom            GONG
	  timer : slow music?     neeyooooo?
        gun : mini boom       balloon pop
* integrate steam to leaderboards & cloud save data
* confirmation for saveing and defaulting and backing in options menu

* add comets to main menu background maybe?
* rankings for players at the end maybe?



make losescene below gamescene not above

1. make particles on collision, and a big one at the center that lasts longer
2. "supercomets" that have a HUGE mass and move extra slow, if they collide with another they make a star
3. a channel so that drawing can communicate easily
4. a channel for soething i cant remember
5. scenes, each scene contains a SpriteBatch which is just a collection of boxes and textures to render, then the SceneManager will hand it over a channel (or maybe the Scene itself will)
6. Comets forsee their collisions and thus will not phase through each other
7. preset levels?
lets go baby

TODO #2:
1. MAKE SURE THAT THE SETTINGS ARE INIT'D AT BEGINNING
2. MAKE SURE TEXTURECACHE IS CLEARED AT END
3. LOAD SDL, WINDOW, RENDERER, SDL_TTF, SDL_MIXER, ALL MUSIC, SOUND EFFECTS, FONT, SDL_IMAGE, TEXTURES

4. fix satTestAxisOverlaps
5. ifx intersectsCircle
otehrs?

problem: i want it so that if the player moves the mouse fast it accounts for the collision throughout (from starting loc to ending loc within that frame)
			but i cant because i would have to override isColliding which is bad
		Instead, to fix it, i could fix all collision problems by applying the movement formulas to all collidable entities
			so that when there is a collision, the entities will store the exact location/tick
		That way, there is no tickrate too low issue causing jumps because every entity will be stretched along its movement

problem with solution: if two comets moving perpendicular to each other jump past each other and by the calculations above "collide,"
			it is possible that they did not collide accounting time and blew past each other
		However, probably, if they never collide, the quadratic formula will have imaginary or negative answers
			

Total solution:
		Make every entity every frame first calcMove, then isColliding to see if they WILL hit by using their paths (for a comet, use a rectangle and two comets on either side) (for a polygon, make a new polygon out of the lines between the original vertecies and the new ones (needs thinking))
		If colliding, store point and tick
		Then, in player, i can just isColliding like anything else
