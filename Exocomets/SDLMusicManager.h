#pragma once
#include "stdafx.h"
#include "SDLMusic.h"
#include "SDLSoundEffect.h"
class SDLMusicManager
{
public:
	friend class SDLManager;
	SDLMusicManager(SDLMusicManager const&) = delete;
	void operator=(SDLMusicManager const&) = delete;
	static std::shared_ptr<SDLMusicManager> getInstance();
	~SDLMusicManager();
	void playMusic(std::string name);
	void playMusic(std::string name, bool fromBeginning);
	void playMusicIfNotPlaying(std::string name);
	void playMusicFromBeginningIfNotPlaying(std::string name);
	void pauseMusic(); // on death (also on ScoreScene add)
	void resumeMusic(); // continues music from after death
	void haltMusic();
	void playSound(std::string name);

	void setMusicVolume(int newVolume);
	void setSoundVolume(int newVolume);

	void tempVolumeDown(double mp);
	void endTempVolumeDown();

private:
	static std::shared_ptr<SDLMusicManager> _staticInstance;
	SDLMusicManager();
	void init();
	void unload();
	std::unordered_map<std::string, std::shared_ptr<SDLMusic>> _musicMap;
	std::unordered_map<std::string, std::shared_ptr<SDLSoundEffect>> _soundEffectMap;
	std::string _currentlyPlaying;

	void setMusicVolume(int newVolume, bool isTemp);
	int _currentMusicVolume;
	bool _temp;
};

