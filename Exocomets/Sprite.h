#pragma once
#include "stdafx.h"
#include "Constants.h"
#include "Direction.h"
class Sprite
{
public:
	Sprite(const std::string filepath, XYSystem size);
	Sprite(const std::string filepath, XYSystem size, std::function<void()> everyRender);
	Sprite(const std::string text, XYSystem relativeLocation, double height, Direction rotation, bool horizontalFlip, bool verticalFlip, const Uint8 rgbaColorMod[4], bool isHudElement);
	Sprite(const std::string filepath, XYSystem relativeLocation, XYSystem size, Direction rotation, bool horizontalFlip, bool verticalFlip, const Uint8 rgbaColorMod[4], bool isHudElement);
	Sprite(const std::string filepath, XYSystem relativeLocation, XYSystem size, Direction rotation, bool horizontalFlip, bool verticalFlip, const Uint8 rgbaColorMod[4], bool isHudElement, Direction colorModD);

	~Sprite();

	void updateText(std::string newText, double fontSize);

	XYSystem relativeLocation;
	// x=width, y=height
	XYSystem size;
	XYSystem sizeMod;
	// Rotation
	Direction rotation;
	// Horizontal flip around center
	bool horizontalFlip;
	// Vertical flip around center
	bool verticalFlip;
	// Filepath
	std::string filepathOrText;
	// If true, it will make a texture that literally is the text in _filepath
	// If false, it will look for the file in filepath and make that the texture
	bool renderFilepathNameInsteadOfFile;

	bool isHudElement;

	Uint8 RGBAColorMod[4];
	Direction colorModD;

	std::function<void()> everyRender;
private:
	Sprite(std::string filepath, bool renderFilePathNameInsteadOfFile, XYSystem relativeLocation, XYSystem size, Direction rotation, bool horizontalFlip, bool verticalFlip, const Uint8 rgbaColorMod[4], bool isHudElement, std::function<void()> everyRender);
	Sprite(std::string filepath, bool renderFilePathNameInsteadOfFile, XYSystem relativeLocation, XYSystem size, Direction rotation, bool horizontalFlip, bool verticalFlip, const Uint8 rgbaColorMod[4], bool isHudElement, std::function<void()> everyRender, Direction colorModD);
};

