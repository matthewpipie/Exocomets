#include "MainMenuScene.h"
#include "SDLMusicManager.h"
#include "SettingsManager.h"
#include "InputManager.h"
#include "SDLTextCreator.h"


MainMenuScene::MainMenuScene(std::shared_ptr<SDLRenderer> rnd) :
	Scene(rnd, Difficulty::MAIN_MENU)
{
	entityBatches.push_back(std::make_shared<EntityBatch>());
	entityBatches.push_back(std::make_shared<EntityBatch>());

	spriteBatches.push_back(std::make_shared<SpriteBatch>());

	entityBatches[1]->entities.push_back(std::make_shared<RectangleEntity>(XYSystem(960, 690), SDLTextCreator::getTextSize("Play", Constants::REGULAR_HEIGHT * 1.5).getSizeVector(), 0, 0));
	entityBatches[1]->entities[0]->spriteBatch->sprites.push_back(std::make_shared<Sprite>("Play", XYSystem(), Constants::REGULAR_HEIGHT * 1.5, Direction(), false, false, Constants::FULL_COLOR, true));

	entityBatches[1]->entities.push_back(std::make_shared<RectangleEntity>(XYSystem(760, 540), SDLTextCreator::getTextSize("Options", Constants::REGULAR_HEIGHT * 1.5).getSizeVector(), 0, 0));
	entityBatches[1]->entities[1]->spriteBatch->sprites.push_back(std::make_shared<Sprite>("Options", XYSystem(), Constants::REGULAR_HEIGHT * 1.5, Direction(), false, false, Constants::FULL_COLOR, true));

	entityBatches[1]->entities.push_back(std::make_shared<RectangleEntity>(XYSystem(1160, 540), SDLTextCreator::getTextSize("Credits", Constants::REGULAR_HEIGHT * 1.5).getSizeVector(), 0, 0));
	entityBatches[1]->entities[2]->spriteBatch->sprites.push_back(std::make_shared<Sprite>("Credits", XYSystem(), Constants::REGULAR_HEIGHT * 1.5, Direction(), false, false, Constants::FULL_COLOR, true));

	entityBatches[1]->entities.push_back(std::make_shared<RectangleEntity>(XYSystem(960, 390), SDLTextCreator::getTextSize("Quit", Constants::REGULAR_HEIGHT * 1.5).getSizeVector(), 0, 0));
	entityBatches[1]->entities[3]->spriteBatch->sprites.push_back(std::make_shared<Sprite>("Quit", XYSystem(), Constants::REGULAR_HEIGHT * 1.5, Direction(), false, false, Constants::FULL_COLOR, true));

	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>(Constants::GAME_NAME, XYSystem(960, 910), Constants::REGULAR_HEIGHT * 2, Direction(), false, false, Constants::FULL_COLOR, true));
	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>(("v" + std::string(Constants::GAME_VERSION) + " by matthewpipie").c_str(), XYSystem(960, 70), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, true));

}




void MainMenuScene::init() {
	difficulty = Difficulty::MAIN_MENU;
	SDLMusicManager::getInstance()->playMusicIfNotPlaying(Constants::MUSIC_NAMES[(int)difficulty]);
	entityBatches[0]->entities.push_back(std::make_shared<Player>(0, camera->getCameraCenter()));

	int a = 0;
}

void MainMenuScene::customEveryTick() {
	for (unsigned int i = 0; i < entityBatches[1]->entities.size(); i++) {
		entityBatches[1]->entities[i]->calculatePotentialLocation();
		if (entityBatches[1]->entities[i]->isPotentiallyColliding(*entityBatches[0]->entities[0])) {
			if (SettingsManager::getSettings().controls[0][0] != -1 || InputManager::getInstance()->isMouseDown()) {
				switch (entityBatches[1]->entities[i]->spriteBatch->sprites[0]->filepathOrText[0]) {
				case 'P':
					action = SceneAction::CREATE_LEVEL_SELECT_SCENE;
					break;
				case 'O':
					action = SceneAction::CREATE_OPTIONS_SCENE;
					break;
				case 'C':
					action = SceneAction::CREATE_CREDITS_SCENE;
					break;
				case 'Q':
					action = SceneAction::DESTROY_SCENE;
					break;
				}
			}
		}
	}
	if (InputManager::getInstance()->isKeyJustDown(SDLK_ESCAPE)) {
		action = SceneAction::DESTROY_SCENE;
		eatKeys = false;
	}

}

void MainMenuScene::deinit() {
//	SDLMusicManager::getInstance()->playSound("click");
	entityBatches[0]->entities.clear();
}

MainMenuScene::~MainMenuScene()
{
	entityBatches[1]->entities.clear();
}
