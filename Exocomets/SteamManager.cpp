#include "SteamManager.h"
#include "MainGame.h"
#include <sstream>
#include <iostream>
#include "ErrorHandler.h"

std::shared_ptr<SteamManager> SteamManager::_staticInstance = nullptr;
bool SteamManager::_enabled = false;

#define _ACH__NAME(a) {a, std::string(#a).substr(std::string(#a).find("::")).substr(2)}

const std::unordered_map<Achievement, std::string, EnumClassHash> SteamManager::ACH_NAMES = {
	_ACH__NAME(Achievement::BEAT_LVL_1),
	_ACH__NAME(Achievement::BEAT_LVL_1),
	_ACH__NAME(Achievement::BEAT_LVL_2),
	_ACH__NAME(Achievement::BEAT_LVL_3),
	_ACH__NAME(Achievement::BEAT_LVL_4),
	_ACH__NAME(Achievement::BEAT_LVL_5),
	_ACH__NAME(Achievement::BEAT_LVL_6),
	_ACH__NAME(Achievement::STARPOWER_1000_NO_POWERUP),
	_ACH__NAME(Achievement::BEAT_4P_GAME),
	_ACH__NAME(Achievement::REVIVE_PLAYER),
	_ACH__NAME(Achievement::LOSE_1000_TIMES),
	_ACH__NAME(Achievement::RESPECT),
	_ACH__NAME(Achievement::COLLECT_100_STARS),
	_ACH__NAME(Achievement::COLLECT_100_SHIELDS),
	_ACH__NAME(Achievement::COLLECT_100_BOMBS),
	_ACH__NAME(Achievement::COLLECT_100_TIMERS),
	_ACH__NAME(Achievement::COLLECT_100_GUNS)
};
const std::unordered_map<Difficulty, std::string, EnumClassHash> SteamManager::LEADERBOARD_NAMES = {
	_ACH__NAME(Difficulty::LEVEL_1),
	_ACH__NAME(Difficulty::LEVEL_2),
	_ACH__NAME(Difficulty::LEVEL_3),
	_ACH__NAME(Difficulty::LEVEL_4),
	_ACH__NAME(Difficulty::LEVEL_5),
	_ACH__NAME(Difficulty::LEVEL_6)
};

void SteamManager::progressAchievement(Achievement ach)
{
	if (_achievements[(int)ach] == Constants::ACHIEVEMENTS_COMPLETED[(int)ach] && Constants::ACHIEVEMENTS_COMPLETED[(int)ach] == 1) return;

	if (!_gotSteamStats) {
		_updatedStatsWithoutSteam = true;
	}

	if (++_achievements[(int)ach] == Constants::ACHIEVEMENTS_COMPLETED[(int)ach]) {
		unlockAchievement(ach);
		std::cout << "Unlocked achievement " << (int)ach << "!" << std::endl;
	}
	if (Constants::ACHIEVEMENTS_COMPLETED[(int)ach] != 1){
		std::string c = Constants::STAT_PREFIX + ACH_NAMES.at(ach);
		_steamUserStats->SetStat(c.c_str(), _achievements[(int)ach]);
		if (_achievements[(int)ach] == Constants::ACHIEVEMENTS_COMPLETED[(int)ach] / 2) {
			_steamUserStats->IndicateAchievementProgress(ACH_NAMES.at(ach).c_str(), _achievements[(int)ach], Constants::ACHIEVEMENTS_COMPLETED[(int)ach]);
		}
	}
	//maybe if -updatedstats?
	_steamUserStats->StoreStats();
}

void SteamManager::loadLeaderboard(Difficulty diff, LeaderboardType type, std::function<void(Leaderboard)> callback)
{
	_steamCallResultCreateLeaderboard.Cancel();
	_steamCallResultDownloadEntries.Cancel();
	_stopLoadingLB = false;
	_isLoadingLB = true;
	_currentLB = Leaderboard(diff, type);
	_entriesNeededID.clear();
	_entriesNeededLB.clear();

	//_entriesNeededID.push_back(SteamUser()->GetSteamID());
	//LeaderboardEntry e;
	//e.isPlayer = true;
	//e.steamName = SteamFriends()->GetPersonaName();
	//for (int i = 0; i < 6; i++) {
		//e.scores[i] = 0;
	//}
	//_entriesNeededLB.push_back(e);

	_onLeaderboardsCallback = callback;
	SteamAPICall_t call = 0;
	call = _steamUserStats->FindLeaderboard(LEADERBOARD_NAMES.at(diff).c_str());
	if (call == 0) {
		std::cerr << "u rektum " << (int)diff << std::endl;
		exit(2330);
	}
	_steamCallResultCreateLeaderboard.Set(call, this, &SteamManager::onFindMainLeaderboard);
}

void SteamManager::stopLoadingLB()
{
	_stopLoadingLB = true;
}

void SteamManager::uploadScore(Difficulty diff, int score)
{
	//_diffToUpload = diff;
	if (!_isUploading && _uploadedInPast10 < 10) {
		std::cout << "uploading score " << score << " on diff " << LEADERBOARD_NAMES.at(diff) << std::endl;
		_scoreToUpload = score;
		SteamAPICall_t call = 0;
		call = _steamUserStats->FindLeaderboard(LEADERBOARD_NAMES.at(diff).c_str());
		if (call == 0) {
			std::cerr << "u rektum sd " << (int)diff << std::endl;
			exit(2329);
		}
		_steamCallResultCreateLeaderboard.Set(call, this, &SteamManager::onFindLeaderboardToUpload);
		_isUploading = true;
	}
	else {
		_uploadQueue.push_back({ diff, score });
	}
}

Uint64 SteamManager::getSteamID()
{
	return SteamUser()->GetSteamID().ConvertToUint64();
}

SteamManager::SteamManager() :
	_callbackOnStatsReceived(this, &SteamManager::onStatsReceived),
	_callbackOnStatsStored(this, &SteamManager::onStatsStored),
	_gotSteamStats(false),
	_updatedStatsWithoutSteam(false),
	_steamUserStats(NULL),
	gameID(SteamUtils()->GetAppID()),
	_currentLB(Difficulty::LEVEL_1, LeaderboardType::TOP_X),
	_uploadedInPast10(0),
	_next10MinuteIntervalTick((int)(Constants::TICK_RATE * 60.0 * 10.0)),
	_isUploading(false),
	_isLoadingLB(false)
{
	for (int i = 0; i < (int)Achievement::LAST; i++) {
		_achievements[i] = Constants::ACHIEVEMENTS_EMPTY[i];
	}
	_steamUserStats = SteamUserStats();

	_steamUserStats->RequestCurrentStats();
}
SteamManager::~SteamManager() {}

void SteamManager::unlockAchievement(Achievement ach)
{
	_achievements[(int)ach] = Constants::ACHIEVEMENTS_COMPLETED[(int)ach];
	_steamUserStats->SetAchievement(ACH_NAMES.at(ach).c_str());
}

void SteamManager::onFindMainLeaderboard(LeaderboardFindResult_t * pFindLeaderboardResult, bool bIOFailure)
{
	if (_stopLoadingLB) return;
	if (!pFindLeaderboardResult->m_bLeaderboardFound || bIOFailure) {
		std::cout << "errror" << std::endl;
		return;
	}

	SteamAPICall_t call = 0;
	int start;
	int end;
	ELeaderboardDataRequest req;
	switch (_currentLB.type)
	{
	case LeaderboardType::TOP_X:
		start = 1;
		end = Constants::LEADERBOARD_ENTRY_COUNT;
		req = k_ELeaderboardDataRequestGlobal;
		break;
	case LeaderboardType::GLOBAL_SURROUND:
		start = -1 * Constants::LEADERBOARD_ENTRY_COUNT / 2;
		end = Constants::LEADERBOARD_ENTRY_COUNT / 2;
		req = k_ELeaderboardDataRequestGlobalAroundUser;
		break;
	case LeaderboardType::FRIENDS:
	default:
		start = 0;
		end = 0;
		req = k_ELeaderboardDataRequestFriends;
		break;
	}
	call = _steamUserStats->DownloadLeaderboardEntries(pFindLeaderboardResult->m_hSteamLeaderboard, req, start, end);
	if (call == 0) {
		std::cerr << "u rektumer " << start << " " << end << " " << (int)req << std::endl;
		exit(2332);
	}
	_steamCallResultDownloadEntries.Set(call, this, &SteamManager::onDownloadMainLeaderboard);
}

void SteamManager::onDownloadMainLeaderboard(LeaderboardScoresDownloaded_t * pLeaderboardScoresDownloaded, bool bIOFailure)
{
	if (_stopLoadingLB) return;

	// leaderboard entries handle will be invalid once we return from this function. Copy all data now.
	std::vector<LeaderboardEntry_t> vec;
	for (int i = 0; i < pLeaderboardScoresDownloaded->m_cEntryCount; i++) {
		LeaderboardEntry_t entry;
		_steamUserStats->GetDownloadedLeaderboardEntry(pLeaderboardScoresDownloaded->m_hSteamLeaderboardEntries,
			i, &entry, NULL, 0);
		vec.push_back(entry);
	}
	for (unsigned int i = 0; i < vec.size(); i++) {
		LeaderboardEntry e;
		e.scores[(int)_currentLB.getDiff() - 1] = vec[i].m_nScore;

		/*for (int j = 0; j < _entriesNeededID.size(); j++) { // This SHOULD be empty unless you manually add something
			auto it = std::find_if(vec.begin(), vec.end(), [&id = _entriesNeededID[j]](const LeaderboardEntry_t &e) -> bool {return id == e.m_steamIDUser; });
			if (it != vec.end()) {
				//overwrite, thru code below
			}
			else {
				e.scores[(int)_currentLB.getDiff() - 1] = 0;
			}
		}*/
		e.isPlayer = SteamUser()->GetSteamID().ConvertToUint64() == vec[i].m_steamIDUser.ConvertToUint64();
		e.placing = vec[i].m_nGlobalRank;
		e.steamName = SteamFriends()->GetFriendPersonaName(vec[i].m_steamIDUser); // convert to string

		std::vector<CSteamID>::iterator it;
		//if ((it = std::find(_entriesNeededID.begin(), _entriesNeededID.end(), vec[i].m_steamIDUser)) != _entriesNeededID.end()) {
			////This person is here twice!?
			//// update his stuffs
			//int index = it - _entriesNeededID.begin();
			//_entriesNeededLB[index] = e;
		//}
		//else {
			_entriesNeededID.push_back(vec[i].m_steamIDUser);
			_entriesNeededLB.push_back(e);
		//}
	}
	_currentDiff = Difficulty::LEVEL_1;
	getNextScores();
}

void SteamManager::onFindSecondaryLeaderboard(LeaderboardFindResult_t * pFindLeaderboardResult, bool bIOFailure)
{
	if (_stopLoadingLB) return;

	if (!pFindLeaderboardResult->m_bLeaderboardFound || bIOFailure) {
		std::cout << "errror34" << std::endl;
		return;
	}
	SteamAPICall_t call = 0;

	std::vector<CSteamID> _fullScoresStillNeeded;
	for (auto it : _entriesNeededID) {
		_fullScoresStillNeeded.push_back(it);
	}

	if (_fullScoresStillNeeded.size() == 0) {
		finishDownload({});
	}
	else {
		call = _steamUserStats->DownloadLeaderboardEntriesForUsers(pFindLeaderboardResult->m_hSteamLeaderboard, &_fullScoresStillNeeded[0], _fullScoresStillNeeded.size());
		if (call == 0) {
			std::cerr << "u rektumer OOo " << std::endl;
			exit(23333);
		}
		_steamCallResultDownloadEntries.Set(call, this, &SteamManager::onDownloadSecondaryLeaderboard);
	}
}

void SteamManager::onDownloadSecondaryLeaderboard(LeaderboardScoresDownloaded_t * pLeaderboardScoresDownloaded, bool bIOFailure)
{
	if (_stopLoadingLB) return;

	// leaderboard entries handle will be invalid once we return from this function. Copy all data now.
	std::vector<LeaderboardEntry_t> vec;
	for (int i = 0; i < pLeaderboardScoresDownloaded->m_cEntryCount; i++) {
		LeaderboardEntry_t entry;
		_steamUserStats->GetDownloadedLeaderboardEntry(pLeaderboardScoresDownloaded->m_hSteamLeaderboardEntries,
			i, &entry, NULL, 0);
		vec.push_back(entry);
	}
	/*for (int i = 0; i < vec.size(); i++) {

		auto it = std::find(_entriesNeededID.begin(), _entriesNeededID.end(), vec[i].m_steamIDUser);
		if (it == _entriesNeededID.end()) {
			std::cout << "wow thats quite the mistake" << std::endl;
			exit(456457);
		}
		int index = it - _entriesNeededID.begin();
		LeaderboardEntry *e = &_entriesNeededLB[index];

		e->scores[(int)_currentDiff - 1] = vec[i].m_nScore;

	}*/
	
	finishDownload(vec);
}

void SteamManager::finishDownload(std::vector<LeaderboardEntry_t> vec)
{
	if (_stopLoadingLB) return;

	for (unsigned int i = 0; i < _entriesNeededID.size(); i++) {
		auto it = std::find_if(vec.begin(), vec.end(), [&id = _entriesNeededID[i]](const LeaderboardEntry_t &e) -> bool {return id == e.m_steamIDUser; });
		if (it == vec.end()) {
			// You haven't submitted a score for this level yet
			_entriesNeededLB[i].scores[(int)_currentDiff - 1] = 0;
		}
		else {
			_entriesNeededLB[i].scores[(int)_currentDiff - 1] = it->m_nScore;
		}
	}
	if (_currentDiff != Difficulty::LEVEL_6) {
		_currentDiff = (Difficulty)((int)_currentDiff + 1);
		getNextScores();
	}
	else {
		for (auto it : _entriesNeededLB) {
			_currentLB.addEntry(it);
		}
		_onLeaderboardsCallback(_currentLB);
		_isLoadingLB = false;
		//getNamesInLBEntries();
	}
}

void SteamManager::getNextScores()
{
	if (_stopLoadingLB) return;

	if (_currentDiff != _currentLB.getDiff()) {
		// Load _currentDiff scores
		SteamAPICall_t call = 0;
		call = _steamUserStats->FindLeaderboard(LEADERBOARD_NAMES.at(_currentDiff).c_str());
		if (call == 0) {
			std::cerr << "u rektumeee " << (int)_currentDiff << std::endl;
			exit(2336);
		}
		_steamCallResultCreateLeaderboard.Set(call, this, &SteamManager::onFindSecondaryLeaderboard);
	}
	else {
		if (_currentDiff != Difficulty::LEVEL_6) {
			_currentDiff = (Difficulty)((int)_currentDiff + 1);
			getNextScores();
		}
		else {
			for (auto it : _entriesNeededLB) {
				_currentLB.addEntry(it);
			}
			_onLeaderboardsCallback(_currentLB);
			_isLoadingLB = false;
		}
	}
	
}

void SteamManager::onFindLeaderboardToUpload(LeaderboardFindResult_t * pFindLeaderboardResult, bool bIOFailure)
{
	if (!pFindLeaderboardResult->m_bLeaderboardFound || bIOFailure) {
		std::cout << "rerraror" << std::endl;
		return;
	}
	SteamAPICall_t call = 0;
	call = _steamUserStats->UploadLeaderboardScore(pFindLeaderboardResult->m_hSteamLeaderboard, k_ELeaderboardUploadScoreMethodKeepBest, _scoreToUpload, NULL, 0);
	if (call != 0) {
		_steamCallResultUploadScore.Set(call, this, &SteamManager::onUpload);
	}
}

void SteamManager::onUpload(LeaderboardScoreUploaded_t * p, bool failure)
{
	if (!p->m_bSuccess || failure) {
		std::cout << "eferrror" << std::endl;
		return;
	}
	std::cout << "success" << std::endl;
	_isUploading = false;
	++_uploadedInPast10;
}

/*void SteamManager::getNamesInLBEntries()
{
	




		_onLeaderboardsCallback(_currentLB);
}

void SteamManager::onPersonaChange(PersonaStateChange_t * pPersonaChange, bool bIOFailure)
{
	pPersonaChange->m_ulSteamID;
}*/

void SteamManager::init()
{
	if (!SteamAPI_Init()) {
		ErrorHandler::error(ErrorCode::STEAM);
	}
	_enabled = true;
	//getInstance();
}

void SteamManager::deinit()
{
	if (!_enabled) return;
	//SteamUserStats()->SetStat("test", 3);
	//SteamUserStats()->StoreStats();
	//SteamUserStats()->RequestCurrentStats();
	SteamAPI_Shutdown();
}

void SteamManager::everyTick()
{
	SteamAPI_RunCallbacks();
	if (!_isUploading && _uploadQueue.size() > 0) {
		if (_uploadedInPast10 < 10) {
			std::pair<Difficulty, int> diffScore(_uploadQueue[0]);
			_uploadQueue.erase(_uploadQueue.begin());
			uploadScore(diffScore.first, diffScore.second);
		}
	}
	if (MainGame::getTickCount() >= _next10MinuteIntervalTick) {
		_uploadedInPast10 = 0;
		_next10MinuteIntervalTick = (int)(MainGame::getTickCount() + Constants::TICK_RATE * 10.0 * 60.0);
	}
}

std::shared_ptr<SteamManager> SteamManager::getInstance() {
	if (!_staticInstance)
		_staticInstance = std::shared_ptr<SteamManager>(new SteamManager());
	return _staticInstance;
}

void SteamManager::onStatsReceived(UserStatsReceived_t *pCallback) {
	if (gameID.ToUint64() == pCallback->m_nGameID) {
		if (k_EResultOK == pCallback->m_eResult) {
			_gotSteamStats = true;
			std::cout << "received steam stats!" << std::endl;
			for (int i = 0; i < (int)Achievement::LAST; i++) {
				_steamUserStats->GetAchievement(ACH_NAMES.at((Achievement)i).c_str(), (bool *)&_achievements[i]);
				if ((bool)_achievements[i]) {
					_achievements[i] = Constants::ACHIEVEMENTS_COMPLETED[i];
				}
				if (Constants::ACHIEVEMENTS_COMPLETED[i] != 1) {
					std::string c = Constants::STAT_PREFIX + ACH_NAMES.at((Achievement)i);
					_steamUserStats->GetStat(c.c_str(), (int32 *)&_achievements[i]);
				}
			}
		}
		else {
			std::cout << "error in receiving steam stats..." << std::endl;
		}
	}
	else {
		std::cout << "got steam stats for the wrong game..." << std::endl;
	}

	//_steamUserStats->ResetAllStats(true);

}



void SteamManager::onStatsStored(UserStatsStored_t *pCallback)
{
	// we may get callbacks for other games' stats arriving, ignore them
	if (gameID.ToUint64() == pCallback->m_nGameID)
	{
		if (k_EResultOK == pCallback->m_eResult)
		{
			//std::cout << ("StoreStats - success\n") << std::endl;
		}
		else if (k_EResultInvalidParam == pCallback->m_eResult)
		{
			// One or more stats we set broke a constraint. They've been reverted,
			// and we should re-iterate the values now to keep in sync.
			//std::cout << ("StoreStats - some failed to validate\n") << std::endl;
			// Fake up a callback here so that we re-load the values.
			//UserStatsReceived_t callback;
			//callback.m_eResult = k_EResultOK;
			//callback.m_nGameID = m_GameID.ToUint64();
			//OnUserStatsReceived(&callback);
		}
		else
		{
			//char buffer[128];
			//std::cout << buffer << "StoreStats - failed, %d\n" << pCallback->m_eResult << std::endl;
			//buffer[sizeof(buffer) - 1] = 0;
			//std::cout << (buffer) << std::endl;
		}
	}
}