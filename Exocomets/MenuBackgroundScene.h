#pragma once
#include "BackgroundScene.h"
class MenuBackgroundScene :
	public BackgroundScene
{
public:
	MenuBackgroundScene(std::shared_ptr<SDLRenderer> rnd, std::shared_ptr<Camera> sceneCamera);
	~MenuBackgroundScene();
	void init() override;
	void onSceneChangeCustom(std::shared_ptr<Scene> oldScene, std::shared_ptr<Scene> newScene) override;
	void deinit() override;
protected:
	void customEveryTick() override;
private:
	void addComet();
};

