#pragma once
#include "CollidableEntity.h"
class DummyCircle :
	public CollidableEntity
{
public:
	DummyCircle(XYSystem location, double radius, double mass, XYSystem speed);
	~DummyCircle();
	virtual void resolvePotentialCollision(CollidableEntity &other);
};

