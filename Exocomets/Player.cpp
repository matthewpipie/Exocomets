#include "Player.h"
#include "Comet.h"
#include "SteamManager.h"
#include "SettingsManager.h"
#include "InputManager.h"
#include "SDLMusicManager.h"
#include "MainGame.h"
#include "Powerup.h"

Player::Player(int playerNumber, XYSystem location) :
	Player(playerNumber, location, Difficulty::MAIN_MENU) 
{
}
Player::Player(int playerNumber, XYSystem location, Difficulty diff) :
	Player(playerNumber, location, false, diff)
{
}

Player::Player(int playerNumber, double initStarPower, XYSystem location, Difficulty diff) :
	Player(playerNumber, location, false, diff)
{
	starCount = initStarPower;
}

Player::Player(int playerNumber, XYSystem location, bool revival, Difficulty diff) :
	CollidableEntity(location, Constants::PLAYER_RADIUS, Constants::PLAYER_MASS, XYSystem(0, 0)), _playerNumber(playerNumber),
	starCount(0),
	_hasGottenAPowerup(false),
	_invincible(diff == Difficulty::MAIN_MENU)
{
	for (int i = 0; i < sizeof(spriteBatch->RGBAColorMod) / sizeof(spriteBatch->RGBAColorMod[0]); i++) {
		spriteBatch->RGBAColorMod[i] = SettingsManager::getSettings().shipColors[playerNumber][i];
	}
	addPlayerSprites();
	for (int i = 0; i < (int)PowerupType::LAST; i++) {
		_hasPowerups[i] = false;
	}
	if (revival) {
		starCount = 250;
		Powerup temp(PowerupType::BOMB);
		resolvePotentialCollision(temp);
	}
	//starCount = 30;
}


Player::~Player()
{
}

void Player::calculateCustomPotentialLocation() {
	potentialLocation = location;
	Settings settings = SettingsManager::getSettings();
	if (settings.controls[_playerNumber][0] == -1) {
		// mouse controls
		XYSystem mouseMovement = InputManager::getInstance()->getMouseMovement().makeDouble();
		potentialLocation += mouseMovement * settings.sensMultipliers[_playerNumber];
		//if (InputManager::getInstance()->isKeyDown(SDLK_b)) {
			//std::cout << mouseMovement.x << " " << mouseMovement.y << std::endl;
		//}
	}
	else {
		bool lD = InputManager::getInstance()->isKeyDown(settings.controls[_playerNumber][0]); //left
		bool dD = InputManager::getInstance()->isKeyDown(settings.controls[_playerNumber][1]); //down
		bool uD = InputManager::getInstance()->isKeyDown(settings.controls[_playerNumber][2]); //up
		bool rD = InputManager::getInstance()->isKeyDown(settings.controls[_playerNumber][3]); //right
		potentialLocation.x += (-1.0 * lD + rD) * Constants::PLAYER_SPEED_HORIZ * settings.sensMultipliers[_playerNumber];
		potentialLocation.y += (-1.0 * dD + uD) * Constants::PLAYER_SPEED_VERT * settings.sensMultipliers[_playerNumber];
	}
	for (int i = 0; i < sizeof(spriteBatch->RGBAColorMod) / sizeof(spriteBatch->RGBAColorMod[0]); i++) {
		spriteBatch->RGBAColorMod[i] = SettingsManager::getSettings().shipColors[_playerNumber][i];
	}
	//if (InputManager::getInstance()->isKeyDown(SDLK_RCTRL)) { starCount = 499; }

	updateStarCount();
	if (starCount == 0) {
		spriteBatch->sprites.back()->updateText("", 14);
	}
	else {
		spriteBatch->sprites.back()->updateText(std::to_string((int)std::ceil(starCount)), 14);
	}
}

void Player::resolvePotentialCollision(Comet &other) {
	if (_invincible) {
		//XYSystem realSpeed = speed;
		//XYSystem realLoc = location;
		XYSystem cometLoc = location;//(location - potentialLocation ) * 3 + location;
		XYSystem cometSpeed = potentialLocation - location;
		Comet com(cometLoc, cometSpeed, Difficulty::MAIN_MENU, radius, mass);
		other.resolvePotentialCollision(com);
		//potentialLocation = com.potentialLocation;
		//other.speed = other.speed.multiplyComponents(XYSystem(1.4));
		//speed = realSpeed;
		//location = realLoc;
		return;
	}
	if (_hasPowerups[(int)PowerupType::BOMB]) {
		other.flagForDeath = true;
		return;
	}
	if (removePowerup(PowerupType::SHIELD)) {
		other.flagForDeath = true;
	}
	else {
		//return;
		flagForDeath = true;
		SDLMusicManager::getInstance()->playSound("explosion");
		actionFlags = actionFlags | EntityActionFlags::ATTACH_DEATH_SCORE;
		//SteamManager::getInstance()->progressStat(Stat::PLAYER_DEATH_COUNT);
		if (_playerNumber == 0) {
			SteamManager::getInstance()->progressAchievement(Achievement::LOSE_1000_TIMES);
		}
	}
}

void Player::resolvePotentialCollision(CollidableEntity &other) {
	other.resolvePotentialCollision(*this);
}

bool Player::isEssentialEntity() {
	return true;
}



void Player::addPlayerSprites() {
	spriteBatch->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::PLAYER_PATH_PREFIX + Constants::PLAYER_PATH + Constants::PLAYER_PATH_SUFFIX,  XYSystem(radius * 2.0)));

	spriteBatch->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::SHIELD_PATH_PREFIX + Constants::SHIELD_PATH + Constants::SHIELD_PATH_SUFFIX, XYSystem(Constants::SHIELD_RADIUS * 2.0)));
	spriteBatch->sprites[spriteBatch->sprites.size() - 1]->RGBAColorMod[3] = 0;

	spriteBatch->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::EXPLOSION_PATH_PREFIX + Constants::EXPLOSION_PATH + Constants::EXPLOSION_PATH_SUFFIX, XYSystem(1/*Constants::EXPLOSION_RADIUS_MULTIPLIER * 2.0*/)));
	Uint8 &AColorMod = spriteBatch->sprites[spriteBatch->sprites.size() - 1]->RGBAColorMod[3];
	//AColorMod = 0;
	spriteBatch->sprites[spriteBatch->sprites.size() - 1]->everyRender = [&AColorMod]() {((int)AColorMod - 7 > 0 ? AColorMod -= 7 : AColorMod = 0); };

	spriteBatch->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::SLOWMO_PATH_PREFIX + Constants::SLOWMO_PATH + Constants::SLOWMO_PATH_SUFFIX, XYSystem(1/*Constants::SLOWMO_RADIUS_MULTIPLIER * 2.0*/)));
	spriteBatch->sprites[spriteBatch->sprites.size() - 1]->RGBAColorMod[3] = 0;

	//spriteBatch->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::SHIELD_PATH_PREFIX + Constants::SHIELD_PATH + Constants::SHIELD_PATH_SUFFIX, XYSystem(Constants::SHIELD_RADIUS * 2.0)));
	//spriteBatch->sprites[spriteBatch->sprites.size() - 1]->RGBAColorMod[3] = 0;

	for (int i = 0; i < Constants::GUN_DIRECTIONS; i++) {
		double rad = (double)i / (double)Constants::GUN_DIRECTIONS * 2.0 * M_PI;
		spriteBatch->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::GUN_PATH_PREFIX + Constants::GUN_PATH + Constants::GUN_PATH_SUFFIX, XYSystem::generateCoordinates(rad, Constants::PLAYER_RADIUS), Constants::GUN_SIZE, Direction(rad), false, false, Constants::GUN_COLOR, false));
		spriteBatch->sprites[spriteBatch->sprites.size() - 1]->RGBAColorMod[3] = 0;
	}

	spriteBatch->sprites.push_back(std::make_shared<Sprite>("", XYSystem(0, Constants::SHIELD_RADIUS + 9), 18, 0, false, false, Constants::FULL_COLOR, false));
}

void Player::resolvePotentialCollision(Player &other) {

}
void Player::resolvePotentialCollision(Powerup &other) {
	other.flagForDeath = true;
	starCount += 30;
	if (_hasPowerups[(int)other.powerupType]) {
		return;
	}
	if (other.powerupType == PowerupType::STAR) {
		starCount += 70;
		SteamManager::getInstance()->progressAchievement(Achievement::COLLECT_100_STARS);
		return;
	}
	else {
		_hasGottenAPowerup = true;
	}

	_hasPowerups[(int)other.powerupType] = true;

	switch (other.powerupType) {
	case PowerupType::SHIELD:
		radius = Constants::SHIELD_RADIUS;
		spriteBatch->sprites[(int)PowerupType::SHIELD]->RGBAColorMod[3] = 255;
		break;
	case PowerupType::BOMB:
		//radius = Constants::EXPLOSION_RADIUS_MULTIPLIER * starCount;
		spriteBatch->sprites[(int)PowerupType::BOMB]->RGBAColorMod[3] = 255;
		//if (starCount < 350) {
			spriteBatch->sprites[(int)PowerupType::BOMB]->sizeMod = XYSystem(0.0005 * starCount * starCount * Constants::EXPLOSION_RADIUS_MULTIPLIER + Constants::PLAYER_RADIUS * 2.0);
		//}
		//else {
			//spriteBatch->sprites[(int)PowerupType::BOMB]->sizeMod = XYSystem(0.0005 * 350 * 350 * Constants::EXPLOSION_RADIUS_MULTIPLIER + Constants::PLAYER_RADIUS * 2.0);
		//}
		actionFlags = actionFlags | EntityActionFlags::CREATE_EXPLOSION;
		break;
	case PowerupType::TIMER:
		spriteBatch->sprites[(int)PowerupType::TIMER]->RGBAColorMod[3] = 255;
		spriteBatch->sprites[(int)PowerupType::TIMER]->sizeMod = XYSystem(0.0005 * starCount * starCount * Constants::SLOWMO_RADIUS_MULTIPLIER + Constants::PLAYER_RADIUS * 2.0);
		actionFlags = actionFlags | EntityActionFlags::CREATE_SLOWMO;
		break;
	case PowerupType::GUN:
		for (int i = 0; i < Constants::GUN_DIRECTIONS; i++) {
			spriteBatch->sprites[i + (int)PowerupType::GUN]->RGBAColorMod[3] = 255;
		}
		break;
	//case PowerupType::REPEL:
		//actionFlags = actionFlags | EntityActionFlags::CREATE_REPEL;
		//break;
	}

	switch (other.powerupType)
	{
	case PowerupType::STAR:
		break;
	case PowerupType::SHIELD:
		SteamManager::getInstance()->progressAchievement(Achievement::COLLECT_100_SHIELDS);
		break;
	case PowerupType::BOMB:
		SteamManager::getInstance()->progressAchievement(Achievement::COLLECT_100_BOMBS);
		break;
	case PowerupType::TIMER:
		SteamManager::getInstance()->progressAchievement(Achievement::COLLECT_100_TIMERS);
		break;
	case PowerupType::GUN:
		SteamManager::getInstance()->progressAchievement(Achievement::COLLECT_100_GUNS);
		break;
	}
}
void Player::updateStarCount() {
	if (_hasPowerups[(int)PowerupType::SHIELD]) {
		//starCount -= std::log10(.0001 * starCount + .2) + 1.0;
		starCount -= .15;
	}
	if (_hasPowerups[(int)PowerupType::BOMB]) {
		//starCount -= 300;
	}
	if (_hasPowerups[(int)PowerupType::TIMER]) {
		//starCount -= std::log10(.001 * starCount + .2) + 1.0;
		starCount -= .25;
		//if (starCount < 350) {
			spriteBatch->sprites[(int)PowerupType::TIMER]->sizeMod = XYSystem(0.0005 * starCount * starCount * Constants::SLOWMO_RADIUS_MULTIPLIER + Constants::PLAYER_RADIUS * 2.0);
		//}
		//else {
			//spriteBatch->sprites[(int)PowerupType::TIMER]->sizeMod = XYSystem(0.0005 * 350 * 350 * Constants::SLOWMO_RADIUS_MULTIPLIER + Constants::PLAYER_RADIUS * 2.0);
		//}
		actionFlags = actionFlags | EntityActionFlags::CREATE_SLOWMO;
	}
	if (_hasPowerups[(int)PowerupType::GUN]) {
		starCount -= .35;
		if (MainGame::getTickCount() % (Constants::GUN_FIRE_RATE * (_hasPowerups[(int)PowerupType::TIMER] ? 2 : 1)) == 1) {
			actionFlags = actionFlags | EntityActionFlags::CREATE_BULLETS;
		}
	}
	//if (_hasPowerups[(int)PowerupType::REPEL]) {
		//actionFlags = actionFlags | EntityActionFlags::CREATE_REPEL;
	//}
	bool hasPowerup = false;
	for (int i = 0; i < (int)PowerupType::LAST; i++) {
		if (_hasPowerups[i]) {
			hasPowerup = true;
			break;
		}
	}
	if (!hasPowerup) {
		starCount -= 1.0 / Constants::TICK_RATE;
		if (starCount >= 1000 && !_hasGottenAPowerup) {
			SteamManager::getInstance()->progressAchievement(Achievement::STARPOWER_1000_NO_POWERUP);
		}
	}
	if (starCount <= 0) {
		for (int i = 0; i < (int)PowerupType::LAST; i++) {
			removePowerup((PowerupType)i);
		}
		starCount = 0;
	}
	if (starCount >= 500) {
		actionFlags = actionFlags | EntityActionFlags::REVIVE_PLAYER;
	}
}

bool Player::removePowerup(PowerupType powerup) {
	if (!_hasPowerups[(int)powerup]) return false;
	switch (powerup) {
	case PowerupType::SHIELD:
		radius = Constants::PLAYER_RADIUS;
		spriteBatch->sprites[(int)PowerupType::SHIELD]->RGBAColorMod[3] = 0;
		break;
	case PowerupType::TIMER:
		spriteBatch->sprites[(int)PowerupType::TIMER]->RGBAColorMod[3] = 0;
		break;
	case PowerupType::GUN:
		for (int i = 0; i < Constants::GUN_DIRECTIONS; i++) {
			spriteBatch->sprites[i + (int)PowerupType::GUN]->RGBAColorMod[3] = 0;
		}
	}
	_hasPowerups[(int)powerup] = false;
	return true;
}

void Player::clearAction()
{
	if ((actionFlags & EntityActionFlags::PLAYER_REVIVED_SUCCESSFULLY) == EntityActionFlags::PLAYER_REVIVED_SUCCESSFULLY) {
		starCount -= 500;
		if (starCount < 0) {
			starCount = 0;
		}
		SteamManager::getInstance()->progressAchievement(Achievement::REVIVE_PLAYER);
	}
	if ((actionFlags & EntityActionFlags::CREATE_BULLETS) == EntityActionFlags::CREATE_BULLETS) {
		int a = 0;
	}
	if ((actionFlags & EntityActionFlags::CREATE_EXPLOSION) == EntityActionFlags::CREATE_EXPLOSION) {
		_hasPowerups[(int)PowerupType::BOMB] = false;
		for (int i = 0; i < (int)PowerupType::LAST; i++) {
			removePowerup((PowerupType)i);
		}
		starCount = 0;
	}

	actionFlags = EntityActionFlags::NOTHING;
}

int Player::getPlayerNumber()
{
	return _playerNumber;
}
