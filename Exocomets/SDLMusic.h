#pragma once
#include "stdafx.h"
#include "SDL_mixerWrapper.h"
class SDLMusic
{
public:
	SDLMusic(std::string name, std::string path, double bpm, double firstBeatOffset, std::vector<double> startTimes);
	~SDLMusic();
	Mix_Music *sdlMixMusic;
	const std::string NAME;
	const std::string PATH;
	const double BPM;
	const double FIRST_BEAT_OFFSET;
	const std::vector<double> START_TIMES;
private:
	Mix_Music *generateMixMusic(std::string path);
};

