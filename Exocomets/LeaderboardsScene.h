#pragma once
#include "stdafx.h"
#include "SteamManager.h"
#include "Scene.h"


class LeaderboardsScene : 
	public Scene
{
public:
	LeaderboardsScene(std::shared_ptr<SDLRenderer> rnd);
	~LeaderboardsScene();
	void init() override;
	void deinit() override;
	bool doBackground() override;

protected:
	void customEveryTick() override;
private:
	void loadScoreboard();
	void resetScoreboard();
	double buildScoreboardFrame(Leaderboard &lb);
	void renderScoreboard(Leaderboard leaderboard);
	LeaderboardType _selectedType;
	Difficulty _selectedDiff;
	bool _loading;
	//std::function<void(std::unordered_map<Difficulty, Leaderboard>)> renderScoreboard;
};

