#if defined(_WIN32) || defined(WIN32)
#define WINDOWS true
#include <windows.h>
#else
#define WINDOWS false
#define MessageBox(a, b, c, d) 
#define MB_OK 
#define MB_ICONERROR 
#endif

#include "ErrorHandler.h"
#include <iostream>
#include "SDLWrapper.h"



void ErrorHandler::error(ErrorCode e)
{
	error(e, "");
}

void ErrorHandler::error(ErrorCode e, std::string extraInfo)
{
	std::string title;
	std::string body;
	switch (e) {
	case ErrorCode::SDL_INIT:
	case ErrorCode::SDL_WINDOW:
	case ErrorCode::SDL_RENDERER:
	case ErrorCode::SDL_MUSIC:
	case ErrorCode::SDL_MUSIC_MANAGER_MUSIC:
	case ErrorCode::SDL_MUSIC_MANAGER_SOUND:
	case ErrorCode::SDL_SOUND_EFFECT:
	case ErrorCode::TTF_INIT:
	case ErrorCode::TTF_FONT:
	case ErrorCode::SDL_TEXTURE:
	case ErrorCode::MUS_INIT:
	case ErrorCode::BMP_INIT:
		title = "SDL error!";
		body = std::string("SDL error #") + std::to_string((int)e) + std::string(".  Please report this by emailing ExocometsDev@gmail.com with as much relevant information as possible.");
		body += "\n\nSDL info: " + std::string(SDL_GetError());
		break;
	case ErrorCode::BAD_FILE_PATH:
	case ErrorCode::DIRECTION_PTS_EQUAL:
	case ErrorCode::DUMMY_CIRCLE_COLLISION:
	case ErrorCode::ENTITY_HITBOX_TOO_SMALL:
	case ErrorCode::NO_POWERUP_UNLOCKED:
	case ErrorCode::SETTINGS_CANT_CREATE:
	case ErrorCode::SAVE_DATA_CANT_CREATE:
		title = "Game error!";
		body = std::string("Game error #") + std::to_string((int)e) + std::string(".  Please report this by emailing ExocometsDev@gmail.com with as much relevant information as possible.");
		break;
	case ErrorCode::STEAM:
		title = "Steam error!";
		body = std::string("Steam error #") + std::to_string((int)e) + std::string(".  Please report this by emailing ExocometsDev@gmail.com with as much relevant information as possible.");
		body += "\nPossible causes:";
		body += "\n\tThe Steam client isn't running\n\tThe Steam client couldn't determine the AppID of game\n\tThe application is not running under the same user context as the Steam client";
		break;
	}

	if (extraInfo != "") {
		body += "\nEXTRA INFO (please report):" + extraInfo;
	}

	std::cerr << title << std::endl;
	std::cerr << body << std::endl;

	if (WINDOWS) {
		MessageBox(
			NULL,
			std::wstring(body.begin(), body.end()).c_str(),
			std::wstring(title.begin(), title.end()).c_str(),
			MB_OK | MB_ICONERROR
		);
	}
	else {
		std::cout << "Press enter to quit..." << std::endl;
		std::cin.ignore();
	}
	exit((int)e);
}
