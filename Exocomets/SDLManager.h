#pragma once
#include "stdafx.h"
#include "SDLWrapper.h"
#include "Constants.h"
#include "SDLMusicManager.h"
#include "SDLRenderer.h"

class SDLManager
{
public:
	SDLManager();
	~SDLManager();
	// Starts all SDL functions
	void initAll();
	void deinitAll();
	void setRenderDrawColor(int color[4]);
	std::shared_ptr<SDLRenderer> customRenderer;
	double getSDLTicks();

	void beginRender();
	void finishRender();
	
private:
	SDL_Window *_window;
	SDL_Renderer *_renderer;
	WindowMode _fullscreenMode;

};

