#pragma once
#include "Scene.h"
class LoseScene :
	public Scene
{
public:
	LoseScene(std::shared_ptr<SDLRenderer> rnd, int finalPlayerScore[4], Difficulty diff);
	~LoseScene();
	void init() override;
	void deinit() override;
	bool doBackground() override;

protected:
	void customEveryTick() override;
private:
	const int _canBecomeDestroyedTick;
	void updateScoreSprite();
};

