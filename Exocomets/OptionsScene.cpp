#include "OptionsScene.h"
#include "SDLMusicManager.h"
#include "SettingsManager.h"
#include "InputManager.h"
#include "KeyOptionEntity.h"
#include "SDLTextCreator.h"


OptionsScene::OptionsScene(std::shared_ptr<SDLRenderer> rnd) :
	Scene(rnd, Difficulty::MAIN_MENU)
{
	entityBatches.push_back(std::make_shared<EntityBatch>());
	entityBatches.push_back(std::make_shared<EntityBatch>());
	spriteBatches.push_back(std::make_shared<SpriteBatch>());
	spriteBatches.push_back(std::make_shared<SpriteBatch>());
	spriteBatches.push_back(std::make_shared<SpriteBatch>());
	spriteBatches.push_back(std::make_shared<SpriteBatch>());
	spriteBatches[0]->location = XYSystem(1280, 600);

	entityBatches[1]->entities.push_back(std::make_shared<RectangleEntity>(XYSystem(960, 70), SDLTextCreator::getTextSize("Save", Constants::REGULAR_HEIGHT).getSizeVector(), 0, 0));
	entityBatches[1]->entities[0]->spriteBatch->sprites.push_back(std::make_shared<Sprite>("Save", XYSystem(), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, true));
	for (int i = 0; i < 4; i++) {
		entityBatches[1]->entities[0]->spriteBatch->RGBAColorMod[i] = Constants::DIM_COLOR_MENU[i];
	}

	entityBatches[1]->entities.push_back(std::make_shared<RectangleEntity>(XYSystem(1280, 70), SDLTextCreator::getTextSize("View Powerup Info", Constants::REGULAR_HEIGHT).getSizeVector(), 0, 0));
	entityBatches[1]->entities[1]->spriteBatch->sprites.push_back(std::make_shared<Sprite>("View Powerup Info", XYSystem(), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, true));

	entityBatches[1]->entities.push_back(std::make_shared<RectangleEntity>(XYSystem(1600, 70), SDLTextCreator::getTextSize("Default", Constants::REGULAR_HEIGHT).getSizeVector(), 0, 0));
	entityBatches[1]->entities[2]->spriteBatch->sprites.push_back(std::make_shared<Sprite>("Default", XYSystem(), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, true));

	entityBatches[1]->entities.push_back(std::make_shared<RectangleEntity>(XYSystem(640, 70), SDLTextCreator::getTextSize("Back", Constants::REGULAR_HEIGHT).getSizeVector(), 0, 0));
	entityBatches[1]->entities[3]->spriteBatch->sprites.push_back(std::make_shared<Sprite>("Back", XYSystem(), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, true));


	sliders.push_back(std::make_shared<Slider>(0, Constants::RESOLUTIONS.size() - 1, XYSystem(320, 960), 320, true));
	spriteBatches[1]->sprites.push_back(std::make_shared<Sprite>("?", XYSystem(320, 960 - Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[2]->sprites.push_back(std::make_shared<Sprite>("Resolution", XYSystem(320, 960 + Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));

	sliders.push_back(std::make_shared<Slider>(0, 2, XYSystem(320, 840), 320, true));
	spriteBatches[1]->sprites.push_back(std::make_shared<Sprite>("?", XYSystem(320, 840 - Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[2]->sprites.push_back(std::make_shared<Sprite>("Window Mode", XYSystem(320, 840 + Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));

	sliders.push_back(std::make_shared<Slider>(0, 1, XYSystem(320, 720), 320, true));
	spriteBatches[1]->sprites.push_back(std::make_shared<Sprite>("?", XYSystem(320, 720 - Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[2]->sprites.push_back(std::make_shared<Sprite>("VSync", XYSystem(320, 720 + Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	
	sliders.push_back(std::make_shared<Slider>(10, 1000, XYSystem(320, 600), 320, false, true));
	spriteBatches[1]->sprites.push_back(std::make_shared<Sprite>("?", XYSystem(320, 600 - Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::NO_COLOR, false));
	spriteBatches[2]->sprites.push_back(std::make_shared<Sprite>("Desired FPS", XYSystem(320, 600 + Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::NO_COLOR, false));

	sliders.push_back(std::make_shared<Slider>(0, 1, XYSystem(320, 480), 320, true, true));
	spriteBatches[1]->sprites.push_back(std::make_shared<Sprite>("?", XYSystem(320, 480 - Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::NO_COLOR, false));
	spriteBatches[2]->sprites.push_back(std::make_shared<Sprite>("Silly Sounds", XYSystem(320, 480 + Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::NO_COLOR, false));

	sliders.push_back(std::make_shared<Slider>(0, SDL_MIX_MAXVOLUME, XYSystem(320, 360), 320, true));
	spriteBatches[1]->sprites.push_back(std::make_shared<Sprite>("?", XYSystem(320, 360 - Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[2]->sprites.push_back(std::make_shared<Sprite>("Music Volume", XYSystem(320, 360 + Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	
	sliders.push_back(std::make_shared<Slider>(0, SDL_MIX_MAXVOLUME, XYSystem(320, 240), 320, true));
	spriteBatches[1]->sprites.push_back(std::make_shared<Sprite>("?", XYSystem(320, 240 - Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[2]->sprites.push_back(std::make_shared<Sprite>("Sound Volume", XYSystem(320, 240 + Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));

	sliders.push_back(std::make_shared<Slider>(1, 4, XYSystem(320, 120), 320, true));
	spriteBatches[1]->sprites.push_back(std::make_shared<Sprite>("?", XYSystem(320, 120 - Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[2]->sprites.push_back(std::make_shared<Sprite>("Player Count", XYSystem(320, 120 + Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));

	const double SENS_MIN = 0.1;
	const double SENS_MAX = 5;
	//1
	sliders.push_back(std::make_shared<Slider>(SENS_MIN, SENS_MAX, XYSystem(800, 200), 160, false));
	spriteBatches[1]->sprites.push_back(std::make_shared<Sprite>("?", XYSystem(800, 200 - Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[2]->sprites.push_back(std::make_shared<Sprite>("Sensitivity", XYSystem(800, 200 + Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	sliders.push_back(std::make_shared<Slider>(0, 255, XYSystem(800, 520), 160, true));
	spriteBatches[1]->sprites.push_back(std::make_shared<Sprite>("?", XYSystem(800, 520 - Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[2]->sprites.push_back(std::make_shared<Sprite>("R", XYSystem(800, 520 + Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	sliders.push_back(std::make_shared<Slider>(0, 255, XYSystem(800, 440), 160, true));
	spriteBatches[1]->sprites.push_back(std::make_shared<Sprite>("?", XYSystem(800, 440 - Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[2]->sprites.push_back(std::make_shared<Sprite>("G", XYSystem(800, 440 + Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	sliders.push_back(std::make_shared<Slider>(0, 255, XYSystem(800, 360), 160, true));
	spriteBatches[1]->sprites.push_back(std::make_shared<Sprite>("?", XYSystem(800, 360 - Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[2]->sprites.push_back(std::make_shared<Sprite>("B", XYSystem(800, 360 + Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	sliders.push_back(std::make_shared<Slider>(0, 255, XYSystem(800, 280), 160, true));
	spriteBatches[1]->sprites.push_back(std::make_shared<Sprite>("?", XYSystem(800, 280 - Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[2]->sprites.push_back(std::make_shared<Sprite>("A", XYSystem(800, 280 + Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));

	//2
	sliders.push_back(std::make_shared<Slider>(SENS_MIN, SENS_MAX, XYSystem(800, 680), 160, false));
	spriteBatches[1]->sprites.push_back(std::make_shared<Sprite>("?", XYSystem(800, 680 - Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[2]->sprites.push_back(std::make_shared<Sprite>("Sensitivity", XYSystem(800, 680 + Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	sliders.push_back(std::make_shared<Slider>(0, 255, XYSystem(800, 1000), 160, true));
	spriteBatches[1]->sprites.push_back(std::make_shared<Sprite>("?", XYSystem(800, 1000 - Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[2]->sprites.push_back(std::make_shared<Sprite>("R", XYSystem(800, 1000 + Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	sliders.push_back(std::make_shared<Slider>(0, 255, XYSystem(800, 920), 160, true));
	spriteBatches[1]->sprites.push_back(std::make_shared<Sprite>("?", XYSystem(800, 920 - Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[2]->sprites.push_back(std::make_shared<Sprite>("G", XYSystem(800, 920 + Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	sliders.push_back(std::make_shared<Slider>(0, 255, XYSystem(800, 840), 160, true));
	spriteBatches[1]->sprites.push_back(std::make_shared<Sprite>("?", XYSystem(800, 840 - Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[2]->sprites.push_back(std::make_shared<Sprite>("B", XYSystem(800, 840 + Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	sliders.push_back(std::make_shared<Slider>(0, 255, XYSystem(800, 760), 160, true));
	spriteBatches[1]->sprites.push_back(std::make_shared<Sprite>("?", XYSystem(800, 760 - Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[2]->sprites.push_back(std::make_shared<Sprite>("A", XYSystem(800, 760 + Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));

	//3
	sliders.push_back(std::make_shared<Slider>(SENS_MIN, SENS_MAX, XYSystem(1440, 680), 160, false));
	spriteBatches[1]->sprites.push_back(std::make_shared<Sprite>("?", XYSystem(1440, 680 - Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[2]->sprites.push_back(std::make_shared<Sprite>("Sensitivity", XYSystem(1440, 680 + Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	sliders.push_back(std::make_shared<Slider>(0, 255, XYSystem(1440, 1000), 160, true));
	spriteBatches[1]->sprites.push_back(std::make_shared<Sprite>("?", XYSystem(1440, 1000 - Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[2]->sprites.push_back(std::make_shared<Sprite>("R", XYSystem(1440, 1000 + Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	sliders.push_back(std::make_shared<Slider>(0, 255, XYSystem(1440, 920), 160, true));
	spriteBatches[1]->sprites.push_back(std::make_shared<Sprite>("?", XYSystem(1440, 920 - Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[2]->sprites.push_back(std::make_shared<Sprite>("G", XYSystem(1440, 920 + Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	sliders.push_back(std::make_shared<Slider>(0, 255, XYSystem(1440, 840), 160, true));
	spriteBatches[1]->sprites.push_back(std::make_shared<Sprite>("?", XYSystem(1440, 840 - Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[2]->sprites.push_back(std::make_shared<Sprite>("B", XYSystem(1440, 840 + Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	sliders.push_back(std::make_shared<Slider>(0, 255, XYSystem(1440, 760), 160, true));
	spriteBatches[1]->sprites.push_back(std::make_shared<Sprite>("?", XYSystem(1440, 760 - Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[2]->sprites.push_back(std::make_shared<Sprite>("A", XYSystem(1440, 760 + Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));

	//4
	sliders.push_back(std::make_shared<Slider>(SENS_MIN, SENS_MAX, XYSystem(1440, 200), 160, false));
	spriteBatches[1]->sprites.push_back(std::make_shared<Sprite>("?", XYSystem(1440, 200 - Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[2]->sprites.push_back(std::make_shared<Sprite>("Sensitivity", XYSystem(1440, 200 + Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	sliders.push_back(std::make_shared<Slider>(0, 255, XYSystem(1440, 520), 160, true));
	spriteBatches[1]->sprites.push_back(std::make_shared<Sprite>("?", XYSystem(1440, 520 - Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[2]->sprites.push_back(std::make_shared<Sprite>("R", XYSystem(1440, 520 + Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	sliders.push_back(std::make_shared<Slider>(0, 255, XYSystem(1440, 440), 160, true));
	spriteBatches[1]->sprites.push_back(std::make_shared<Sprite>("?", XYSystem(1440, 440 - Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[2]->sprites.push_back(std::make_shared<Sprite>("G", XYSystem(1440, 440 + Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	sliders.push_back(std::make_shared<Slider>(0, 255, XYSystem(1440, 360), 160, true));
	spriteBatches[1]->sprites.push_back(std::make_shared<Sprite>("?", XYSystem(1440, 360 - Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[2]->sprites.push_back(std::make_shared<Sprite>("B", XYSystem(1440, 360 + Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	sliders.push_back(std::make_shared<Slider>(0, 255, XYSystem(1440, 280), 160, true));
	spriteBatches[1]->sprites.push_back(std::make_shared<Sprite>("?", XYSystem(1440, 280 - Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[2]->sprites.push_back(std::make_shared<Sprite>("A", XYSystem(1440, 280 + Constants::DOT_SIZE.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));

	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::DOT_PATH_PREFIX + Constants::DOT_PATH + Constants::DOT_PATH_SUFFIX, XYSystem(0, -240), XYSystem(2, 480), Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::DOT_PATH_PREFIX + Constants::DOT_PATH + Constants::DOT_PATH_SUFFIX, XYSystem(0, 240), XYSystem(2, 480), Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::DOT_PATH_PREFIX + Constants::DOT_PATH + Constants::DOT_PATH_SUFFIX, XYSystem(-320, 0), XYSystem(640, 2), Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::DOT_PATH_PREFIX + Constants::DOT_PATH + Constants::DOT_PATH_SUFFIX, XYSystem(320, 0), XYSystem(640, 2), Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::DOT_PATH_PREFIX + Constants::DOT_PATH + Constants::DOT_PATH_SUFFIX, XYSystem(320, -480), XYSystem(640, 2), Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::DOT_PATH_PREFIX + Constants::DOT_PATH + Constants::DOT_PATH_SUFFIX, XYSystem(-640, 240), XYSystem(2, 480), Direction(), false, false, Constants::FULL_COLOR, false));

	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>("1", SDLTextCreator::getTextSize("1", Constants::REGULAR_HEIGHT) * -1.0, Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	XYSystem size = SDLTextCreator::getTextSize("2", Constants::REGULAR_HEIGHT);
	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>("2", XYSystem(-size.x, size.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>("3", SDLTextCreator::getTextSize("3", Constants::REGULAR_HEIGHT), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	size = SDLTextCreator::getTextSize("4", Constants::REGULAR_HEIGHT);
	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>("4", XYSystem(size.x, -size.y), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));



	keyOptionEntities.push_back(std::make_shared<KeyOptionEntity>(XYSystem(1120, 216)));
	spriteBatches[3]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::ARROW_PATH_PREFIX + Constants::ARROW_PATH + Constants::ARROW_PATH_SUFFIX, XYSystem(1120 + 2.0 * Constants::KEY_OPTION_RADIUS, 216), Constants::ARROW_SIZE, Direction(M_PI), false, false, Constants::FULL_COLOR, false));
	keyOptionEntities.push_back(std::make_shared<KeyOptionEntity>(XYSystem(1120, 312)));
	spriteBatches[3]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::ARROW_PATH_PREFIX + Constants::ARROW_PATH + Constants::ARROW_PATH_SUFFIX, XYSystem(1120 + 2.0 * Constants::KEY_OPTION_RADIUS, 312), Constants::ARROW_SIZE, Direction(), false, false, Constants::FULL_COLOR, false));
	keyOptionEntities.push_back(std::make_shared<KeyOptionEntity>(XYSystem(1120, 408)));
	spriteBatches[3]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::ARROW_PATH_PREFIX + Constants::ARROW_PATH + Constants::ARROW_PATH_SUFFIX, XYSystem(1120 + 2.0 * Constants::KEY_OPTION_RADIUS, 408), Constants::ARROW_SIZE, Direction(M_PI / 2.0), false, false, Constants::FULL_COLOR, false));
	keyOptionEntities.push_back(std::make_shared<KeyOptionEntity>(XYSystem(1120, 504)));
	spriteBatches[3]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::ARROW_PATH_PREFIX + Constants::ARROW_PATH + Constants::ARROW_PATH_SUFFIX, XYSystem(1120 + 2.0 * Constants::KEY_OPTION_RADIUS, 504), Constants::ARROW_SIZE, Direction(-M_PI / 2.0), false, false, Constants::FULL_COLOR, false));

	keyOptionEntities.push_back(std::make_shared<KeyOptionEntity>(XYSystem(1120, 696)));
	spriteBatches[3]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::ARROW_PATH_PREFIX + Constants::ARROW_PATH + Constants::ARROW_PATH_SUFFIX, XYSystem(1120 + 2.0 * Constants::KEY_OPTION_RADIUS, 696), Constants::ARROW_SIZE, Direction(M_PI), false, false, Constants::FULL_COLOR, false));
	keyOptionEntities.push_back(std::make_shared<KeyOptionEntity>(XYSystem(1120, 792)));
	spriteBatches[3]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::ARROW_PATH_PREFIX + Constants::ARROW_PATH + Constants::ARROW_PATH_SUFFIX, XYSystem(1120 + 2.0 * Constants::KEY_OPTION_RADIUS, 792), Constants::ARROW_SIZE, Direction(), false, false, Constants::FULL_COLOR, false));
	keyOptionEntities.push_back(std::make_shared<KeyOptionEntity>(XYSystem(1120, 888)));
	spriteBatches[3]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::ARROW_PATH_PREFIX + Constants::ARROW_PATH + Constants::ARROW_PATH_SUFFIX, XYSystem(1120 + 2.0 * Constants::KEY_OPTION_RADIUS, 888), Constants::ARROW_SIZE, Direction(M_PI / 2.0), false, false, Constants::FULL_COLOR, false));
	keyOptionEntities.push_back(std::make_shared<KeyOptionEntity>(XYSystem(1120, 984)));
	spriteBatches[3]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::ARROW_PATH_PREFIX + Constants::ARROW_PATH + Constants::ARROW_PATH_SUFFIX, XYSystem(1120 + 2.0 * Constants::KEY_OPTION_RADIUS, 984), Constants::ARROW_SIZE, Direction(-M_PI / 2.0), false, false, Constants::FULL_COLOR, false));

	keyOptionEntities.push_back(std::make_shared<KeyOptionEntity>(XYSystem(1760, 696)));
	spriteBatches[3]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::ARROW_PATH_PREFIX + Constants::ARROW_PATH + Constants::ARROW_PATH_SUFFIX, XYSystem(1760 + 2.0 * Constants::KEY_OPTION_RADIUS, 696), Constants::ARROW_SIZE, Direction(M_PI), false, false, Constants::FULL_COLOR, false));
	keyOptionEntities.push_back(std::make_shared<KeyOptionEntity>(XYSystem(1760, 792)));
	spriteBatches[3]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::ARROW_PATH_PREFIX + Constants::ARROW_PATH + Constants::ARROW_PATH_SUFFIX, XYSystem(1760 + 2.0 * Constants::KEY_OPTION_RADIUS, 792), Constants::ARROW_SIZE, Direction(), false, false, Constants::FULL_COLOR, false));
	keyOptionEntities.push_back(std::make_shared<KeyOptionEntity>(XYSystem(1760, 888)));
	spriteBatches[3]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::ARROW_PATH_PREFIX + Constants::ARROW_PATH + Constants::ARROW_PATH_SUFFIX, XYSystem(1760 + 2.0 * Constants::KEY_OPTION_RADIUS, 888), Constants::ARROW_SIZE, Direction(M_PI / 2.0), false, false, Constants::FULL_COLOR, false));
	keyOptionEntities.push_back(std::make_shared<KeyOptionEntity>(XYSystem(1760, 984)));
	spriteBatches[3]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::ARROW_PATH_PREFIX + Constants::ARROW_PATH + Constants::ARROW_PATH_SUFFIX, XYSystem(1760 + 2.0 * Constants::KEY_OPTION_RADIUS, 984), Constants::ARROW_SIZE, Direction(-M_PI / 2.0), false, false, Constants::FULL_COLOR, false));

	keyOptionEntities.push_back(std::make_shared<KeyOptionEntity>(XYSystem(1760, 216)));
	spriteBatches[3]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::ARROW_PATH_PREFIX + Constants::ARROW_PATH + Constants::ARROW_PATH_SUFFIX, XYSystem(1760 + 2.0 * Constants::KEY_OPTION_RADIUS, 216), Constants::ARROW_SIZE, Direction(M_PI), false, false, Constants::FULL_COLOR, false));
	keyOptionEntities.push_back(std::make_shared<KeyOptionEntity>(XYSystem(1760, 312)));
	spriteBatches[3]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::ARROW_PATH_PREFIX + Constants::ARROW_PATH + Constants::ARROW_PATH_SUFFIX, XYSystem(1760 + 2.0 * Constants::KEY_OPTION_RADIUS, 312), Constants::ARROW_SIZE, Direction(), false, false, Constants::FULL_COLOR, false));
	keyOptionEntities.push_back(std::make_shared<KeyOptionEntity>(XYSystem(1760, 408)));
	spriteBatches[3]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::ARROW_PATH_PREFIX + Constants::ARROW_PATH + Constants::ARROW_PATH_SUFFIX, XYSystem(1760 + 2.0 * Constants::KEY_OPTION_RADIUS, 408), Constants::ARROW_SIZE, Direction(M_PI / 2.0), false, false, Constants::FULL_COLOR, false));
	keyOptionEntities.push_back(std::make_shared<KeyOptionEntity>(XYSystem(1760, 504)));
	spriteBatches[3]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::ARROW_PATH_PREFIX + Constants::ARROW_PATH + Constants::ARROW_PATH_SUFFIX, XYSystem(1760 + 2.0 * Constants::KEY_OPTION_RADIUS, 504), Constants::ARROW_SIZE, Direction(-M_PI / 2.0), false, false, Constants::FULL_COLOR, false));

	
}




void OptionsScene::init() {
	difficulty = Difficulty::MAIN_MENU;
	SDLMusicManager::getInstance()->playMusicIfNotPlaying(Constants::MUSIC_NAMES[(int)difficulty]);
	entityBatches[0]->entities.push_back(std::make_shared<Player>(0, XYSystem(960, 360)));
	entityBatches[0]->entities.push_back(std::make_shared<Player>(1, XYSystem(960, 840)));
	entityBatches[0]->entities.push_back(std::make_shared<Player>(2, XYSystem(1600, 840)));
	entityBatches[0]->entities.push_back(std::make_shared<Player>(3, XYSystem(1600, 360)));
	for (auto e : keyOptionEntities) {
		entityBatches[0]->entities.push_back(e);
	}
	SettingsManager::beginEditing();
	loadSettingsToSliderValues();
}

void OptionsScene::customEveryTick() {
	for (unsigned int i = 0; i < entityBatches[1]->entities.size(); i++) {
		entityBatches[1]->entities[i]->calculatePotentialLocation();
		if (entityBatches[1]->entities[i]->isPotentiallyColliding(*entityBatches[0]->entities[0])) {
			if (SettingsManager::getSettings().controls[0][0] != -1 || InputManager::getInstance()->isMouseDown()) {
				switch (entityBatches[1]->entities[i]->spriteBatch->sprites[0]->filepathOrText[0]) {
				case 'S':
					if (!(SettingsManager::_oldSettings == SettingsManager::_currentSettings)) {
						SettingsManager::saveSettings();
						action = SceneAction::NOTHING;
					}
					break;
				case 'D':
					SettingsManager::resetDefault();
					loadSettingsToSliderValues();
					action = SceneAction::NOTHING;
					break;
				case 'B':
					action = SceneAction::DESTROY_SCENE;
					break;
				case 'V':
					action = SceneAction::CREATE_POWERUP_UNLOCK_SCENE;
					break;
				}
			}
		}
	}
	if (SettingsManager::_oldSettings == SettingsManager::_currentSettings) {
		for (int i = 0; i < 4; i++) {
			entityBatches[1]->entities[0]->spriteBatch->RGBAColorMod[i] = Constants::DIM_COLOR_MENU[i];
		}
	}
	else {
		for (int i = 0; i < 4; i++) {
			entityBatches[1]->entities[0]->spriteBatch->RGBAColorMod[i] = Constants::FULL_COLOR[i];
		}
	}
	if (InputManager::getInstance()->isKeyJustDown(SDLK_ESCAPE)) {
		action = SceneAction::DESTROY_SCENE;
		eatKeys = false;
	}
	loadSliderValuesToSettings();
	entityBatches[0]->entities[1]->location = entityBatches[0]->entities[1]->location.fixInRange(XYSystem(642, 602) + entityBatches[0]->entities[1]->radius, XYSystem(1278, 1080) - entityBatches[0]->entities[1]->radius);
	entityBatches[0]->entities[2]->location = entityBatches[0]->entities[2]->location.fixInRange(XYSystem(1282, 602) + entityBatches[0]->entities[2]->radius, XYSystem(1920, 1080) - entityBatches[0]->entities[2]->radius);
	entityBatches[0]->entities[3]->location = entityBatches[0]->entities[3]->location.fixInRange(XYSystem(1282, 122) + entityBatches[0]->entities[3]->radius, XYSystem(1920, 598) - entityBatches[0]->entities[3]->radius);


	// Disabled options:
	SettingsManager::_currentSettings.desiredFPS = Constants::SETTINGS_DEFAULT.desiredFPS;
	SettingsManager::_currentSettings.sillySounds = Constants::SETTINGS_DEFAULT.sillySounds;
}

void OptionsScene::loadSettingsToSliderValues() {

	XYIntSystem currentRes(SettingsManager::getSettings().windowSize[0], SettingsManager::getSettings().windowSize[1]);
	bool found = false;
	unsigned int i;
	for (i = 0; i < Constants::RESOLUTIONS.size(); i++) {
		if (Constants::RESOLUTIONS[i] == currentRes) {
			found = true;
			break;
		}
	}
	if (!found) {
		i = 0;
	}
	sliders[0]->setValue(i);

	int fs = (int)SettingsManager::getSettings().windowMode;
	if (fs == (int)WindowMode::FULLSCREEN_BORDERLESS) {
		fs = 2;
	}
	sliders[1]->setValue(fs);

	sliders[2]->setValue(SettingsManager::getSettings().enableVSync);
	sliders[3]->setValue(SettingsManager::getSettings().desiredFPS);
	sliders[4]->setValue(SettingsManager::getSettings().sillySounds);
	sliders[5]->setValue(SettingsManager::getSettings().musicVolume);
	sliders[6]->setValue(SettingsManager::getSettings().soundVolume);
	sliders[7]->setValue(SettingsManager::getSettings().playerCount);

	sliders[8]->setValue(SettingsManager::getSettings().sensMultipliers[0]);
	sliders[9]->setValue(SettingsManager::getSettings().shipColors[0][0]);
	sliders[10]->setValue(SettingsManager::getSettings().shipColors[0][1]);
	sliders[11]->setValue(SettingsManager::getSettings().shipColors[0][2]);
	sliders[12]->setValue(SettingsManager::getSettings().shipColors[0][3]);

	sliders[13]->setValue(SettingsManager::getSettings().sensMultipliers[1]);
	sliders[14]->setValue(SettingsManager::getSettings().shipColors[1][0]);
	sliders[15]->setValue(SettingsManager::getSettings().shipColors[1][1]);
	sliders[16]->setValue(SettingsManager::getSettings().shipColors[1][2]);
	sliders[17]->setValue(SettingsManager::getSettings().shipColors[1][3]);

	sliders[18]->setValue(SettingsManager::getSettings().sensMultipliers[2]);
	sliders[19]->setValue(SettingsManager::getSettings().shipColors[2][0]);
	sliders[20]->setValue(SettingsManager::getSettings().shipColors[2][1]);
	sliders[21]->setValue(SettingsManager::getSettings().shipColors[2][2]);
	sliders[22]->setValue(SettingsManager::getSettings().shipColors[2][3]);

	sliders[23]->setValue(SettingsManager::getSettings().sensMultipliers[3]);
	sliders[24]->setValue(SettingsManager::getSettings().shipColors[3][0]);
	sliders[25]->setValue(SettingsManager::getSettings().shipColors[3][1]);
	sliders[26]->setValue(SettingsManager::getSettings().shipColors[3][2]);
	sliders[27]->setValue(SettingsManager::getSettings().shipColors[3][3]);


	keyOptionEntities[0]->value = SettingsManager::getSettings().controls[0][3];
	keyOptionEntities[1]->value = SettingsManager::getSettings().controls[0][0];
	keyOptionEntities[2]->value = SettingsManager::getSettings().controls[0][1];
	keyOptionEntities[3]->value = SettingsManager::getSettings().controls[0][2];

	keyOptionEntities[4]->value = SettingsManager::getSettings().controls[1][3];
	keyOptionEntities[5]->value = SettingsManager::getSettings().controls[1][0];
	keyOptionEntities[6]->value = SettingsManager::getSettings().controls[1][1];
	keyOptionEntities[7]->value = SettingsManager::getSettings().controls[1][2];

	keyOptionEntities[8]->value = SettingsManager::getSettings().controls[2][3];
	keyOptionEntities[9]->value = SettingsManager::getSettings().controls[2][0];
	keyOptionEntities[10]->value = SettingsManager::getSettings().controls[2][1];
	keyOptionEntities[11]->value = SettingsManager::getSettings().controls[2][2];

	keyOptionEntities[12]->value = SettingsManager::getSettings().controls[3][3];
	keyOptionEntities[13]->value = SettingsManager::getSettings().controls[3][0];
	keyOptionEntities[14]->value = SettingsManager::getSettings().controls[3][1];
	keyOptionEntities[15]->value = SettingsManager::getSettings().controls[3][2];
}

void OptionsScene::loadSliderValuesToSettings() {
	int val = (int)sliders[0]->getValue();
	SettingsManager::_currentSettings.windowSize[0] = Constants::RESOLUTIONS[val].x;
	SettingsManager::_currentSettings.windowSize[1] = Constants::RESOLUTIONS[val].y;
	spriteBatches[1]->sprites[0]->updateText(std::to_string(Constants::RESOLUTIONS[val].x) + "x" + std::to_string(Constants::RESOLUTIONS[val].y), Constants::REGULAR_HEIGHT);

	int wMode = (int)sliders[1]->getValue();
	if (wMode == 2) {
		wMode = (int)WindowMode::FULLSCREEN_BORDERLESS;
	}
	SettingsManager::_currentSettings.windowMode = (WindowMode)wMode;
	std::string displayName = "";
	switch ((WindowMode)wMode) {
	case WindowMode::WINDOWED:
		displayName = "Windowed";
		break;
	case WindowMode::FULLSCREEN_BORDERLESS:
		displayName = "Fullscreen Borderless";
		break;
	case WindowMode::FULLSCREEN:
		displayName = "Fullscreen";
		break;
	}
	spriteBatches[1]->sprites[1]->updateText(displayName, Constants::REGULAR_HEIGHT);

	SettingsManager::_currentSettings.enableVSync = sliders[2]->getValue();
	spriteBatches[1]->sprites[2]->updateText(sliders[2]->getValue() == 1 ? "On" : "Off", Constants::REGULAR_HEIGHT);

	SettingsManager::_currentSettings.desiredFPS = (int)sliders[3]->getValue();
	spriteBatches[1]->sprites[3]->updateText(std::to_string((int)sliders[3]->getValue()), Constants::REGULAR_HEIGHT);

	SettingsManager::_currentSettings.sillySounds = sliders[4]->getValue();
	spriteBatches[1]->sprites[4]->updateText(sliders[4]->getValue() == 1 ? "On" : "Off", Constants::REGULAR_HEIGHT);

	SettingsManager::_currentSettings.musicVolume = (int)sliders[5]->getValue();
	SDLMusicManager::getInstance()->setMusicVolume((int)sliders[5]->getValue());
	spriteBatches[1]->sprites[5]->updateText(std::to_string((int)std::round(sliders[5]->getValue() / ((double)SDL_MIX_MAXVOLUME / 100.0))), Constants::REGULAR_HEIGHT);

	SettingsManager::_currentSettings.soundVolume = (int)sliders[6]->getValue();
	SDLMusicManager::getInstance()->setSoundVolume((int)sliders[6]->getValue());
	spriteBatches[1]->sprites[6]->updateText(std::to_string((int)std::round(sliders[6]->getValue() / ((double)SDL_MIX_MAXVOLUME / 100.0))), Constants::REGULAR_HEIGHT);

	SettingsManager::_currentSettings.playerCount = (int)sliders[7]->getValue();
	spriteBatches[1]->sprites[7]->updateText(std::to_string((int)sliders[7]->getValue()), Constants::REGULAR_HEIGHT);

	SettingsManager::_currentSettings.sensMultipliers[0] = sliders[8]->getValue();
	spriteBatches[1]->sprites[8]->updateText(std::to_string(sliders[8]->getValue()).substr(0, 4), Constants::REGULAR_HEIGHT);
	SettingsManager::_currentSettings.shipColors[0][0] = (int)sliders[9]->getValue();
	spriteBatches[1]->sprites[9]->updateText(std::to_string((int)sliders[9]->getValue()), Constants::REGULAR_HEIGHT);
	SettingsManager::_currentSettings.shipColors[0][1] = (int)sliders[10]->getValue();
	spriteBatches[1]->sprites[10]->updateText(std::to_string((int)sliders[10]->getValue()), Constants::REGULAR_HEIGHT);
	SettingsManager::_currentSettings.shipColors[0][2] = (int)sliders[11]->getValue();
	spriteBatches[1]->sprites[11]->updateText(std::to_string((int)sliders[11]->getValue()), Constants::REGULAR_HEIGHT);
	SettingsManager::_currentSettings.shipColors[0][3] = (int)sliders[12]->getValue();
	spriteBatches[1]->sprites[12]->updateText(std::to_string((int)sliders[12]->getValue()), Constants::REGULAR_HEIGHT);

	SettingsManager::_currentSettings.sensMultipliers[1] = sliders[13]->getValue();
	spriteBatches[1]->sprites[13]->updateText(std::to_string(sliders[13]->getValue()).substr(0, 4), Constants::REGULAR_HEIGHT);
	SettingsManager::_currentSettings.shipColors[1][0] = (int)sliders[14]->getValue();
	spriteBatches[1]->sprites[14]->updateText(std::to_string((int)sliders[14]->getValue()), Constants::REGULAR_HEIGHT);
	SettingsManager::_currentSettings.shipColors[1][1] = (int)sliders[15]->getValue();
	spriteBatches[1]->sprites[15]->updateText(std::to_string((int)sliders[15]->getValue()), Constants::REGULAR_HEIGHT);
	SettingsManager::_currentSettings.shipColors[1][2] = (int)sliders[16]->getValue();
	spriteBatches[1]->sprites[16]->updateText(std::to_string((int)sliders[16]->getValue()), Constants::REGULAR_HEIGHT);
	SettingsManager::_currentSettings.shipColors[1][3] = (int)sliders[17]->getValue();
	spriteBatches[1]->sprites[17]->updateText(std::to_string((int)sliders[17]->getValue()), Constants::REGULAR_HEIGHT);

	SettingsManager::_currentSettings.sensMultipliers[2] = sliders[18]->getValue();
	spriteBatches[1]->sprites[18]->updateText(std::to_string(sliders[18]->getValue()).substr(0, 4), Constants::REGULAR_HEIGHT);
	SettingsManager::_currentSettings.shipColors[2][0] = (int)sliders[19]->getValue();
	spriteBatches[1]->sprites[19]->updateText(std::to_string((int)sliders[19]->getValue()), Constants::REGULAR_HEIGHT);
	SettingsManager::_currentSettings.shipColors[2][1] = (int)sliders[20]->getValue();
	spriteBatches[1]->sprites[20]->updateText(std::to_string((int)sliders[20]->getValue()), Constants::REGULAR_HEIGHT);
	SettingsManager::_currentSettings.shipColors[2][2] = (int)sliders[21]->getValue();
	spriteBatches[1]->sprites[21]->updateText(std::to_string((int)sliders[21]->getValue()), Constants::REGULAR_HEIGHT);
	SettingsManager::_currentSettings.shipColors[2][3] = (int)sliders[22]->getValue();
	spriteBatches[1]->sprites[22]->updateText(std::to_string((int)sliders[22]->getValue()), Constants::REGULAR_HEIGHT);

	SettingsManager::_currentSettings.sensMultipliers[3] = sliders[23]->getValue();
	spriteBatches[1]->sprites[23]->updateText(std::to_string(sliders[23]->getValue()).substr(0, 4), Constants::REGULAR_HEIGHT);
	SettingsManager::_currentSettings.shipColors[3][0] = (int)sliders[24]->getValue();
	spriteBatches[1]->sprites[24]->updateText(std::to_string((int)sliders[24]->getValue()), Constants::REGULAR_HEIGHT);
	SettingsManager::_currentSettings.shipColors[3][1] = (int)sliders[25]->getValue();
	spriteBatches[1]->sprites[25]->updateText(std::to_string((int)sliders[25]->getValue()), Constants::REGULAR_HEIGHT);
	SettingsManager::_currentSettings.shipColors[3][2] = (int)sliders[26]->getValue();
	spriteBatches[1]->sprites[26]->updateText(std::to_string((int)sliders[26]->getValue()), Constants::REGULAR_HEIGHT);
	SettingsManager::_currentSettings.shipColors[3][3] = (int)sliders[27]->getValue();
	spriteBatches[1]->sprites[27]->updateText(std::to_string((int)sliders[27]->getValue()), Constants::REGULAR_HEIGHT);


	SettingsManager::_currentSettings.controls[0][3] = keyOptionEntities[0]->value;
	SettingsManager::_currentSettings.controls[0][0] = keyOptionEntities[1]->value;
	SettingsManager::_currentSettings.controls[0][1] = keyOptionEntities[2]->value;
	SettingsManager::_currentSettings.controls[0][2] = keyOptionEntities[3]->value;

	SettingsManager::_currentSettings.controls[1][3] = keyOptionEntities[4]->value;
	SettingsManager::_currentSettings.controls[1][0] = keyOptionEntities[5]->value;
	SettingsManager::_currentSettings.controls[1][1] = keyOptionEntities[6]->value;
	SettingsManager::_currentSettings.controls[1][2] = keyOptionEntities[7]->value;

	SettingsManager::_currentSettings.controls[2][3] = keyOptionEntities[8]->value;
	SettingsManager::_currentSettings.controls[2][0] = keyOptionEntities[9]->value;
	SettingsManager::_currentSettings.controls[2][1] = keyOptionEntities[10]->value;
	SettingsManager::_currentSettings.controls[2][2] = keyOptionEntities[11]->value;

	SettingsManager::_currentSettings.controls[3][3] = keyOptionEntities[12]->value;
	SettingsManager::_currentSettings.controls[3][0] = keyOptionEntities[13]->value;
	SettingsManager::_currentSettings.controls[3][1] = keyOptionEntities[14]->value;
	SettingsManager::_currentSettings.controls[3][2] = keyOptionEntities[15]->value;

	for (int i = 0; i < 16; i++) {
		if (keyOptionEntities[i]->value == -1 && keyOptionEntities[i/4 * 4]->isActive && keyOptionEntities[i/4 * 4 + 1]->isActive && keyOptionEntities[i/4 * 4 + 2]->isActive && keyOptionEntities[i/4 * 4 + 3]->isActive) {
			keyOptionEntities[i / 4 * 4]->value = -1;
			keyOptionEntities[i / 4 * 4 + 1]->value = -1;
			keyOptionEntities[i / 4 * 4 + 2]->value = -1;
			keyOptionEntities[i / 4 * 4 + 3]->value = -1;
		}
	}
	for (int i = 0; i < 4; i++) {
		if (keyOptionEntities[i * 4 + 1]->value == -1) {
			keyOptionEntities[i * 4]->isActive = false;
			keyOptionEntities[i * 4 + 2]->isActive = false;
			keyOptionEntities[i * 4 + 3]->isActive = false;
			keyOptionEntities[i * 4]->spriteBatch->RGBAColorMod[3] = 0;
			keyOptionEntities[i * 4 + 2]->spriteBatch->RGBAColorMod[3] = 0;
			keyOptionEntities[i * 4 + 3]->spriteBatch->RGBAColorMod[3] = 0;
		}
		else {
			if (!keyOptionEntities[i * 4]->isActive) {
				keyOptionEntities[i * 4]->value = SDLK_3;
				keyOptionEntities[i * 4 + 2]->value = SDLK_2;
				keyOptionEntities[i * 4 + 3]->value = SDLK_1;
			}
			keyOptionEntities[i * 4]->isActive = true;
			keyOptionEntities[i * 4 + 2]->isActive = true;
			keyOptionEntities[i * 4 + 3]->isActive = true;
			keyOptionEntities[i * 4]->spriteBatch->RGBAColorMod[3] = 255;
			keyOptionEntities[i * 4 + 2]->spriteBatch->RGBAColorMod[3] = 255;
			keyOptionEntities[i * 4 + 3]->spriteBatch->RGBAColorMod[3] = 255;
		}
	}

}

void OptionsScene::deinit() {
	//SDLMusicManager::getInstance()->playSound("click");
	SettingsManager::discardChanges();
	loadSettingsToSliderValues();
	loadSliderValuesToSettings();
	entityBatches[0]->entities.clear();
}

bool OptionsScene::doBackground()
{
	return false;
}

OptionsScene::~OptionsScene()
{
	entityBatches[1]->entities.clear();
}
