#pragma once
#include "Powerup.h"
class StarPowerup :
	public Powerup
{
public:
	StarPowerup(std::shared_ptr<Camera> camera);
	StarPowerup(std::shared_ptr<Camera> camera, Difficulty diff);
	~StarPowerup();
protected:
	void calculateCustomPotentialLocation() override;
	void checkTwinkle();
	void generateNextTwinkleTime();
	int _nextTwinkleTick;
	int isTwinkling;
};

