#include "MenuBackgroundScene.h"
#include "MainGame.h"


MenuBackgroundScene::MenuBackgroundScene(std::shared_ptr<SDLRenderer> rnd, std::shared_ptr<Camera> sceneCamera) :
	BackgroundScene(rnd, Difficulty::MAIN_MENU, sceneCamera)
{
}

MenuBackgroundScene::~MenuBackgroundScene()
{
}

void MenuBackgroundScene::init()
{
	entityBatches[0]->entities.push_back(std::make_shared<Player>(0, camera->getCameraCenter()));

	for (int i = 0; i < Constants::COMET_COUNT_INIT[(int)difficulty]; i++) {
		addComet();
	}

}

void MenuBackgroundScene::onSceneChangeCustom(std::shared_ptr<Scene> oldScene, std::shared_ptr<Scene> newScene)
{
//	update = newScene->difficulty == Difficulty::MAIN_MENU;
	entityBatches[0]->entities[0]->location = camera->getCameraCenter();
}

void MenuBackgroundScene::deinit()
{
}

void MenuBackgroundScene::customEveryTick()
{
	if (MainGame::getTickCount() % (int)(std::ceil(Constants::COMET_SPAWN_RATES[(int)difficulty] / entityBatches[0]->nonEssentialSpeedMod.x)) == 0) {

		//		if (!entityBatches[0]->isTooFull())
		addComet();
		//std::cout << entityBatches[0]->entities.size() << "wow" <<std::endl;
	}
}

void MenuBackgroundScene::addComet()
{
	std::shared_ptr<Comet> comet = nullptr;
	double radius;
	double mass;
	XYSystem mod;
	for (int j = 0; j < Constants::COMET_SPAWN_ATTEMPTS_PER_TICK; j++) {
		radius = Constants::COMET_RADIUS;
		mass = Constants::COMET_MASS;
		mod = XYSystem(1);
		comet = std::make_shared<Comet>(camera, difficulty, radius, mass, mod);
		for (unsigned int i = 0; i < entityBatches[0]->entities.size() - 1; i++) {
			if (entityBatches[0]->entities[i]->isCurrentlyColliding(*comet)) {
				comet = nullptr;
				break;
			}
		}
		if (comet != nullptr) {
			entityBatches[0]->entities.push_back(comet);
			return;
		}
	}
}

