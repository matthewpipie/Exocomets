#pragma once

#include "targetver.h"

#include <stdio.h>
#ifdef _WIN32
#include <tchar.h>
#endif



// TODO: reference additional headers your program requires here

//#include <SDL.h>
//SDL_image only in sprite or something else i forgot
//SDL_mixer in MusicManager.h
//#include <SDL_ttf.h>
#include <stdio.h>
#include <cmath>
#include <ctime>
#include <functional>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <memory>
#include <unordered_map>
#include <random>
#include <stack>

const double PI = 3.141592653589793;



struct XYSystem {
	double x;
	double y;
	XYSystem() : XYSystem(0, 0) {}
	XYSystem(double xy) : XYSystem(xy, xy) {}
	XYSystem(double nx, double ny) : x(nx), y(ny) {}
	static XYSystem generateCoordinates(double rads, double magnitude) {
		return XYSystem(std::cos(rads) * magnitude, std::sin(rads) * magnitude);
	}
	double getMagnitudeSquared() const {
		return x*x + y*y;
	}
	double getMangitude() const {
		return std::sqrt(getMagnitudeSquared());
	}
	double distanceToSquared(const XYSystem &other) const {
		return std::pow(x - other.x, 2.0) + std::pow(y - other.y, 2.0);
	}
	double distanceTo(const XYSystem &other) const {
		return std::sqrt(distanceToSquared(other));
	}
	std::vector<XYSystem> getSizeVector() const {
		return std::vector<XYSystem>({ XYSystem(-x / 2.0, -y / 2.0), XYSystem(-x / 2.0, y / 2.0), XYSystem(x / 2.0, y / 2.0), XYSystem(x / 2.0, -y / 2.0) });
	}
	// Returns + for left, 0 for on, - for right
	int isLeft(XYSystem line1, XYSystem line2) const {
		return isLeft(line1, line2, *this);
	}
	int isLeft(XYSystem line1, XYSystem line2, XYSystem point) const {
		return (int)(((line2.x - line1.x) * (point.y - line1.y)
			- (point.x - line1.x) * (line2.y - line1.y)));
	}
	// VECTOR TIME (thanks http://doswa.com/2009/07/13/circle-segment-intersectioncollision.html)
	bool lineTangentToLineSegmentThroughPointIntersectsLineSegment(XYSystem line1, XYSystem line2) const {
		XYSystem seg_v = line2 - line1;
		XYSystem pt_v = *this - line1;
		double proj_v_mag = pt_v.dotProduct(seg_v / seg_v.getMangitude());
		if (proj_v_mag < 0 || proj_v_mag > seg_v.getMangitude()) {
			return false;
		}
		return true;
	}
	XYSystem getMidpoint(const XYSystem &O) const {
		return XYSystem((x + O.x) / 2.0, (y + O.y) / 2.0);
	}

	XYSystem fixInRange(const XYSystem bottomLeft, const XYSystem topRight) const {
		XYSystem ret;
		if (x < bottomLeft.x) {
			ret.x = bottomLeft.x;
		}
		else if (x > topRight.x) {
			ret.x = topRight.x;
		}
		else {
			ret.x = x;
		}
		if (y < bottomLeft.y) {
			ret.y = bottomLeft.y;
		}
		else if (y > topRight.y) {
			ret.y = topRight.y;
		}
		else {
			ret.y = y;
		}
		return ret;
	}

	XYSystem getMinimum(XYSystem &p2) {
		return fixInRange(*this, p2);
	}

	XYSystem getTranslationVectorToProjectPointOnToLine(XYSystem line1, XYSystem line2) const {
		XYSystem seg_v = line2 - line1;
		XYSystem pt_v = *this - line1;
		double proj_v_mag = pt_v.dotProduct(seg_v / seg_v.getMangitude());
		XYSystem proj_v = seg_v / seg_v.getMangitude() * proj_v_mag;
		XYSystem closestPoint = line1 + proj_v;
		XYSystem dist_v = closestPoint - *this;
		return dist_v;
	}
	XYSystem getTranslationVectorToProjectPointOnToLine(XYSystem line1) const {
		return getTranslationVectorToProjectPointOnToLine(XYSystem(), line1);
	}
	XYSystem getTranslatedPointToLine(XYSystem line1, XYSystem line2) const {
		return *this + getTranslationVectorToProjectPointOnToLine(line1, line2);
	}
	XYSystem getTranslatedPointToLine(XYSystem line1) const {
		return *this + getTranslationVectorToProjectPointOnToLine(line1);
	}
	bool operator==(const XYSystem &O) const {
		return x == O.x && y == O.y;
	}
	bool operator!=(const XYSystem &O) const {
		return !operator==(O);
	}
	XYSystem operator+(const XYSystem &O) const {
		return XYSystem(x + O.x, y + O.y);
	}
	void operator+=(const XYSystem &O) {
		*this = operator+(O);
	}
	XYSystem operator-(const XYSystem &O) const {
		return XYSystem(x - O.x, y - O.y);
	}
	void operator-=(const XYSystem &O) {
		*this = operator-(O);
	}
	XYSystem operator+(const double &O) const {
		return XYSystem(x + O, y + O);
	}
	void operator+=(const double &O) {
		*this = operator+(O);
	}
	XYSystem operator-(const double &O) const {
		return XYSystem(x - O, y - O);
	}
	void operator-=(const double &O) {
		*this = operator-(O);
	}
	// Dot product
	double dotProduct(const XYSystem &O) const {
		return x * O.x + y * O.y;
	}
	XYSystem operator*(const double &O) const {
		return XYSystem(x * O, y * O);
	}
	void operator*=(const double &O) {
		*this = operator*(O);
	}
	XYSystem operator/(const double &O) const {
		return operator*(1.0 / O);
	}
	void operator/=(const double &O) {
		*this = operator/(O);
	}
	XYSystem multiplyComponents(XYSystem &O) const {
		return XYSystem(x * O.x, y * O.y);
	}
	XYSystem divideComponents(XYSystem &O) const {
		return XYSystem(x / O.x, y / O.y);
	}
};
struct XYIntSystem {
	XYIntSystem() : x(0), y(0) {}
	XYIntSystem(int nx, int ny) : x(nx), y(ny) {}
	int x;
	int y;
	const XYSystem makeDouble() const {
		return XYSystem(x, y);
	}
	bool operator==(const XYIntSystem &O) const {
		return x == O.x && y == O.y;
	}
	bool operator!=(const XYIntSystem &O) const {
		return !operator==(O);
	}
};