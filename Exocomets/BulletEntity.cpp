#include "BulletEntity.h"
#include "Comet.h"
#include "Player.h"



BulletEntity::BulletEntity(XYSystem location, double rad) :
	CollidableEntity(location, generateHitbox(rad), 0, XYSystem::generateCoordinates(rad, Constants::BULLET_SPEED))
{
	spriteBatch->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::BULLET_PATH_PREFIX + Constants::BULLET_PATH + Constants::BULLET_PATH_SUFFIX, XYSystem(), Constants::BULLET_SIZE, Direction(rad), false, false, Constants::FULL_COLOR, false));
}


BulletEntity::~BulletEntity()
{
}

const std::vector<XYSystem> BulletEntity::generateHitbox(double rad)
{
	std::vector<XYSystem> a;
	a.push_back(XYSystem::generateCoordinates(rad + M_PI, Constants::BULLET_SIZE.x / 2.0) + XYSystem::generateCoordinates(rad + M_PI * 3.0/2.0, Constants::BULLET_SIZE.y / 2.0));
	a.push_back(XYSystem::generateCoordinates(rad + M_PI, Constants::BULLET_SIZE.x / 2.0) + XYSystem::generateCoordinates(rad + M_PI / 2.0, Constants::BULLET_SIZE.y / 2.0));
	a.push_back(XYSystem::generateCoordinates(rad, Constants::BULLET_SIZE.x / 2.0) + XYSystem::generateCoordinates(rad + M_PI / 2.0, Constants::BULLET_SIZE.y / 2.0));
	a.push_back(XYSystem::generateCoordinates(rad, Constants::BULLET_SIZE.x / 2.0) + XYSystem::generateCoordinates(rad + M_PI * 3.0/2.0, Constants::BULLET_SIZE.y / 2.0));
	return a;
}

void BulletEntity::resolvePotentialCollision(CollidableEntity &other) {
	other.resolvePotentialCollision(*this);
}

void BulletEntity::resolvePotentialCollision(Comet & other)
{
	other.flagForDeath = true;
	flagForDeath = true;
}
void BulletEntity::resolvePotentialCollision(Player & other)
{
	int a = 0;
}
