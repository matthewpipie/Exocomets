#pragma once
#include "stdafx.h"
//#include <cassert>
//#include <iostream>
//#include <cstdlib>
//#include <map>
#include <unordered_map>
#include <regex>
#include "Constants.h" //has Settings struct in it and default vals

class SettingsManager
{
public:
	friend class OptionsScene; // TODO: this
	//getter for _currentSettings
	static Settings getSettings(); 
	//call on game start to load settings
	static void init();
	
private:
	//static std::unordered_map<std::string, std::string> _stringSettings;
	//static std::unordered_map<std::string, double> _doubleSettings;
	//static std::unordered_map<std::string, int> _intSettings;
	//static const std::string DEFAULT_SETTINGS;
	//static std::string getDefaultSettings();
	
	// These are things you should use with the friend class
	//edit this from SettingsScene and call saveSettings if you want to save it or discardTemp to clear it
	static Settings _currentSettings;
	//returns true if needs restart to apply
	static bool saveSettings(); 
	//call on settings screen close to reset temp
	static void discardChanges(); 
	static void beginEditing();
	static void resetDefault();
	
	
	static Settings _oldSettings; 
	static void readSettings();
	static void writeSettings(Settings settings);
	static bool needsRestart(Settings s1, Settings s2);
	const static std::unordered_map<std::string, std::regex> _regexMap;
};

/*
In order to add a setting:
1. add setting to Settings struct in Constants.h
2. add setting to SettingsVerification struct in Constants.h
3. add operator== thing for your setting in Constants.h
4. add a default value in Constants.cpp to SETTINGS_DEFAULT
5. add a default value in Constants.cpp to SETTINGS_UNVERIFIED
6. add a verified value in Constants.cpp to SETTINGS_VERIFIED
7. add a regex val in _regexMap in SettingsManager.cpp
8. add a reading ability in readSettings in SettingsManager.cpp
9. add a writing ability in writeSettings in SettingsManager.cpp (be sure to add a \n to the previous)

Usage of class:
1. call init() AS THE FIRST LINE IN main() (or close to that)
2. change "friend class SettingsScene" to whatever class you want to be able to edit settings
3. call getSettings() whenever anywhere you want the settings
4. within the friend class, edit _currentSettings
5. on destruction of the friend class, if it was destroyed and changes are accepted, call saveSettings().  If denied, call discardTemp()
*/
