#include "DummyCircle.h"
#include "ErrorHandler.h"



DummyCircle::DummyCircle(XYSystem location, double radius, double mass, XYSystem speed) :
	CollidableEntity(location, radius, mass, speed)
{
}


DummyCircle::~DummyCircle()
{
}

void DummyCircle::resolvePotentialCollision(CollidableEntity & other)
{
	ErrorHandler::error(ErrorCode::DUMMY_CIRCLE_COLLISION);
}
