#include "LevelSelectScene.h"
#include "SaveDataManager.h"
#include "SDLMusicManager.h"
#include "SettingsManager.h"
#include "InputManager.h"
#include "SDLTextCreator.h"


LevelSelectScene::LevelSelectScene(std::shared_ptr<SDLRenderer> rnd) :
	Scene(rnd, Difficulty::MAIN_MENU)
{
	entityBatches.push_back(std::make_shared<EntityBatch>());
	entityBatches.push_back(std::make_shared<EntityBatch>());
	spriteBatches.push_back(std::make_shared<SpriteBatch>());

}

void LevelSelectScene::init() {
	difficulty = Difficulty::MAIN_MENU;
	SDLMusicManager::getInstance()->playMusicIfNotPlaying(Constants::MUSIC_NAMES[(int)difficulty]);
	entityBatches[0]->entities.push_back(std::make_shared<Player>(0, camera->getCameraCenter()));
	spawnTextEntities();

		int a = 0;
}

void LevelSelectScene::customEveryTick() {
	for (unsigned int i = 0; i < entityBatches[1]->entities.size(); i++) {
		entityBatches[1]->entities[i]->calculatePotentialLocation();
		if (entityBatches[1]->entities[i]->isPotentiallyColliding(*entityBatches[0]->entities[0])) {
			if (SettingsManager::getSettings().controls[0][0] != -1 || InputManager::getInstance()->isMouseDown()) {
				if (entityBatches[1]->entities[i]->spriteBatch->sprites[0]->filepathOrText.size() == std::string("Level 1").size()) {
					switch (entityBatches[1]->entities[i]->spriteBatch->sprites[0]->filepathOrText[6]) {
					case '1':
						difficulty = Difficulty::LEVEL_1;
						action = SceneAction::CREATE_GAME_SCENE;
						break;
					case '2':
						if (checkHS(0)) {
							difficulty = Difficulty::LEVEL_2;
							action = SceneAction::CREATE_GAME_SCENE;
						}
						break;
					case '3':
						if (checkHS(1)) {
							difficulty = Difficulty::LEVEL_3;
							action = SceneAction::CREATE_GAME_SCENE;
						}
						break;
					case '4':
						if (checkHS(2)) {
							difficulty = Difficulty::LEVEL_4;
							action = SceneAction::CREATE_GAME_SCENE;
						}
						break;
					case '5':
						if (checkHS(3)) {
							difficulty = Difficulty::LEVEL_5;
							action = SceneAction::CREATE_GAME_SCENE;
						}
						break;
					case '6':
						if (checkHS(4)) {
							difficulty = Difficulty::LEVEL_6;
							action = SceneAction::CREATE_GAME_SCENE;
						}
						break;
					default:
						action = SceneAction::DESTROY_SCENE;
						break;
					}
				}
				else if (entityBatches[1]->entities[i]->spriteBatch->sprites[0]->filepathOrText.size() == std::string("Leaderboards").size()) {
					action = SceneAction::CREATE_LEADERBOARDS_SCENE;
				}
				else {
					action = SceneAction::DESTROY_SCENE;
				}
			}
		}
	}
	if (InputManager::getInstance()->isKeyJustDown(SDLK_ESCAPE)) {
		action = SceneAction::DESTROY_SCENE;
		eatKeys = false;
	}
}

bool LevelSelectScene::checkHS(int index)
{
	return SaveDataManager::getCurrentSaveData().highScores[index] >= Constants::SCORE_LIMIT;
}

void LevelSelectScene::spawnTextEntities()
{
	Uint8 mod[4];
	std::copy(std::begin(Constants::FULL_COLOR), std::end(Constants::FULL_COLOR), std::begin(mod));

	entityBatches[1]->entities.push_back(std::make_shared<RectangleEntity>(XYSystem(760, 690), SDLTextCreator::getTextSize("Level 1", Constants::REGULAR_HEIGHT * 1.5).getSizeVector(), 0, 0));
	entityBatches[1]->entities[0]->spriteBatch->sprites.push_back(std::make_shared<Sprite>("Level 1", XYSystem(), Constants::REGULAR_HEIGHT * 1.5, Direction(), false, false, mod, true));

	setModColor(0, mod);
	entityBatches[1]->entities.push_back(std::make_shared<RectangleEntity>(XYSystem(760, 540), SDLTextCreator::getTextSize("Level 2", Constants::REGULAR_HEIGHT * 1.5).getSizeVector(), 0, 0));
	entityBatches[1]->entities[1]->spriteBatch->sprites.push_back(std::make_shared<Sprite>("Level 2", XYSystem(), Constants::REGULAR_HEIGHT * 1.5, Direction(), false, false, mod, true));

	setModColor(1, mod);
	entityBatches[1]->entities.push_back(std::make_shared<RectangleEntity>(XYSystem(760, 390), SDLTextCreator::getTextSize("Level 3", Constants::REGULAR_HEIGHT * 1.5).getSizeVector(), 0, 0));
	entityBatches[1]->entities[2]->spriteBatch->sprites.push_back(std::make_shared<Sprite>("Level 3", XYSystem(), Constants::REGULAR_HEIGHT * 1.5, Direction(), false, false, mod, true));

	setModColor(2, mod);
	entityBatches[1]->entities.push_back(std::make_shared<RectangleEntity>(XYSystem(1160, 690), SDLTextCreator::getTextSize("Level 4", Constants::REGULAR_HEIGHT * 1.5).getSizeVector(), 0, 0));
	entityBatches[1]->entities[3]->spriteBatch->sprites.push_back(std::make_shared<Sprite>("Level 4", XYSystem(), Constants::REGULAR_HEIGHT * 1.5, Direction(), false, false, mod, true));

	setModColor(3, mod);
	entityBatches[1]->entities.push_back(std::make_shared<RectangleEntity>(XYSystem(1160, 540), SDLTextCreator::getTextSize("Level 5", Constants::REGULAR_HEIGHT * 1.5).getSizeVector(), 0, 0));
	entityBatches[1]->entities[4]->spriteBatch->sprites.push_back(std::make_shared<Sprite>("Level 5", XYSystem(), Constants::REGULAR_HEIGHT * 1.5, Direction(), false, false, mod, true));

	setModColor(4, mod);
	entityBatches[1]->entities.push_back(std::make_shared<RectangleEntity>(XYSystem(1160, 390), SDLTextCreator::getTextSize("Level 6", Constants::REGULAR_HEIGHT * 1.5).getSizeVector(), 0, 0));
	entityBatches[1]->entities[5]->spriteBatch->sprites.push_back(std::make_shared<Sprite>("Level 6", XYSystem(), Constants::REGULAR_HEIGHT * 1.5, Direction(), false, false, mod, true));

	entityBatches[1]->entities.push_back(std::make_shared<RectangleEntity>(XYSystem(960, 290), SDLTextCreator::getTextSize("Back", Constants::REGULAR_HEIGHT * 1.5).getSizeVector(), 0, 0));
	entityBatches[1]->entities[6]->spriteBatch->sprites.push_back(std::make_shared<Sprite>("Back", XYSystem(), Constants::REGULAR_HEIGHT * 1.5, Direction(), false, false, Constants::FULL_COLOR, true));

	entityBatches[1]->entities.push_back(std::make_shared<RectangleEntity>(XYSystem(960, 790), SDLTextCreator::getTextSize("Leaderboards", Constants::REGULAR_HEIGHT * 1.5).getSizeVector(), 0, 0));
	entityBatches[1]->entities[7]->spriteBatch->sprites.push_back(std::make_shared<Sprite>("Leaderboards", XYSystem(), Constants::REGULAR_HEIGHT * 1.5, Direction(), false, false, Constants::FULL_COLOR, true));


	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>("Play", XYSystem(960, 960), Constants::REGULAR_HEIGHT * 2, Direction(), false, false, Constants::FULL_COLOR, true));
}

void LevelSelectScene::setModColor(int index, Uint8 mod[4])
{
	if (checkHS(index)) {
		for (int i = 0; i < 4; i++) {
			mod[i] = Constants::FULL_COLOR[i];
		}
	}
	else {
		for (int i = 0; i < 4; i++) {
			mod[i] = Constants::DIM_COLOR_MENU[i];
		}
	}
}

void LevelSelectScene::deinit() {
	//SDLMusicManager::getInstance()->playSound("click");
	entityBatches[0]->entities.clear();
	entityBatches[1]->entities.clear();
}

LevelSelectScene::~LevelSelectScene()
{
}
