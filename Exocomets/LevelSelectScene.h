#pragma once
#include "Scene.h"
class LevelSelectScene :
	public Scene
{
public:
	LevelSelectScene(std::shared_ptr<SDLRenderer> rnd);
	~LevelSelectScene();
	void init() override;
	void deinit() override;
protected:
	void customEveryTick() override;
private:
	bool checkHS(int index);
	void spawnTextEntities();
	void setModColor(int index, Uint8 mod[4]);
};

