#include "MainGame.h"
#include "MenuBackgroundScene.h"
#include "SteamManager.h"
#include "CreditsScene.h"
#include "LeaderboardsScene.h"
#include "PowerupUnlockScene.h"
#include "CollidableEntity.h"
#include "Comet.h"
#include "SettingsManager.h"
#include "InputManager.h"
#include "RandomNumberManager.h"
#include "LoseScene.h"
#include "LevelSelectScene.h"
#include "MainMenuScene.h"
#include "OptionsScene.h"
#include "GameScene.h"

int MainGame::_tick = 0;

MainGame::MainGame() :
	_backgroundScene(nullptr)
{
}


MainGame::~MainGame()
{
}


void MainGame::run()
{
	//std::cout << "About to initAll!" << std::endl;
	_sdlManager.initAll();
	//std::cout << "Starting scene..." << std::endl;
	_sceneStack.push_back(std::make_shared<MainMenuScene>(_sdlManager.customRenderer));
	_sceneStack.back()->init();
	_backgroundScene = std::make_shared<MenuBackgroundScene>(_sdlManager.customRenderer, _sceneStack[0]->camera);
	_backgroundScene->init();
	//std::cout << "Beginning game loop" << std::endl;
	gameLoop();
	std::cout << "quitting game..." << std::endl;
	_sdlManager.deinitAll();
}

int MainGame::getTickCount() {
	return _tick;
}

void MainGame::gameLoop() {
	//std::cout << "seed=" << RandomNumberManager::getInstance()->getSeed() << std::endl;
	double currentMS = _sdlManager.getSDLTicks();
	double nextTickUpdateTime = _sdlManager.getSDLTicks() - 1000.0 / Constants::TICK_RATE; //ms
	double nextRenderTime = 0; //ms
	int tickUpdates;
	InputManager::getInstance()->pollInput();
	while (true) {
		//std::cout << "start" << std::endl;
		currentMS = _sdlManager.getSDLTicks(); //start ms
		//std::cout << currentMS << " " << nextTickUpdateTime << " " << nextRenderTime << std::endl;

		//if (!InputManager::getInstance()->isKeyDown(SDLK_LSHIFT)) {
			for (tickUpdates = 0; currentMS >= nextTickUpdateTime; tickUpdates++) { //update ticks
				everyTick();
				currentMS = _sdlManager.getSDLTicks();
				nextTickUpdateTime += 1000.0 / Constants::TICK_RATE;
				//std::cout << "updato" << std::endl;
				if (_shouldQuit) {
					break;
				}
				if (currentMS - nextTickUpdateTime <= 2001.0 / Constants::TICK_RATE) {
					render();
				}
				currentMS = _sdlManager.getSDLTicks();
			}
		//}
		//else {
			//everyTick();
			//render();
			////
		//}
		if (_shouldQuit) {
			break;
		}
		if (currentMS >= nextRenderTime) {
			//if (tickUpdates < Constants::MAX_UPDATES_TO_RENDER) {
				//render();
			//}
			while (currentMS >= nextRenderTime) {
				nextRenderTime += 1000.0 / (double)SettingsManager::getSettings().desiredFPS;
				//std::cout << "updato2" << std::endl;
			}
		}
		currentMS = _sdlManager.getSDLTicks();

		double *sleepUntilTime;
		//if (nextTickUpdateTime < nextRenderTime) {
			sleepUntilTime = &nextTickUpdateTime;
			//std::cout << "1" << std::endl;
		//}
		//else {
			//sleepUntilTime = &nextRenderTime;
			//std::cout << "2" << std::endl;
		//}
		//std::cout << currentMS << " " << nextTickUpdateTime << " " << nextRenderTime << std::endl;

		if (*sleepUntilTime > currentMS) {
			//std::cout << "sleeping for " << (Uint32)(*sleepUntilTime - currentMS) << " msecs" << std::endl;
			SDL_Delay((Uint32)(*sleepUntilTime - currentMS));
		}

	}
}

void MainGame::everyTick() {
	std::shared_ptr<Scene> activeScene = _sceneStack.back();
	//std::cout << "Polling input..." << std::endl;
	InputManager::getInstance()->pollInput();
	//if (InputManager::getInstance()->isKeyDown(SDLK_v)) {
		//std::cout << SDL_GetError() << std::endl;
	//}
	SteamManager::getInstance()->everyTick();
	//std::cout << "everyTicking..." << std::endl;
	/*
	static bool f;
	if (f || InputManager::getInstance()->isKeyDown(SDLK_TAB)) {
		int a = 0;
		std::shared_ptr<InputManager>b(InputManager::getInstance());
		b->_keysDown[SDLK_ESCAPE] = KeyState::KEY_JUST_DOWN;
		if (f) {
			b->_keysDown[SDLK_ESCAPE] = KeyState::KEY_DOWN;
		}
		f = true;
	}
	else {
		f = false;
	}
	if (InputManager::getInstance()->isKeyJustDown(SDLK_1)) {
		SDLMusicManager::getInstance()->playMusic("1");
	}
	if (InputManager::getInstance()->isKeyJustDown(SDLK_2)) {
		SDLMusicManager::getInstance()->playMusic("2");
	}
	if (InputManager::getInstance()->isKeyJustDown(SDLK_3)) {
		SDLMusicManager::getInstance()->playMusic("3");
	}
	if (InputManager::getInstance()->isKeyJustDown(SDLK_4)) {
		SDLMusicManager::getInstance()->playMusic("4");
	}*/
	std::shared_ptr<Scene> oldScene = activeScene;
	activeScene->everyTick();
	if (activeScene->doBackground()) {
		_backgroundScene->everyTick();
	}


	//std::cout << "Success." << std::endl;
	switch (activeScene->action) {
	case SceneAction::NOTHING:
		break;
	case SceneAction::DESTROY_TWO_SCENES:
		SDLMusicManager::getInstance()->playSound("click");
		if (activeScene->eatKeys) {
			InputManager::getInstance()->eatKey(-1);
		}
		activeScene->action = SceneAction::NOTHING;
		activeScene->deinit();
		_sceneStack.pop_back();
		if (_sceneStack.size() != 0) {
			//_sceneStack.back()->init();
			activeScene = _sceneStack.back();
		}
		else {
			_shouldQuit = true;
			break;
		}
		activeScene->action = SceneAction::NOTHING;
		_sceneStack.pop_back();
		if (_sceneStack.size() != 0) {
			_sceneStack.back()->init();
		}
		else {
			_shouldQuit = true;
		}
		break;
	case SceneAction::DESTROY_SCENE:
		SDLMusicManager::getInstance()->playSound("click");
		if (activeScene->eatKeys) {
			InputManager::getInstance()->eatKey(-1);
		}
		activeScene->action = SceneAction::NOTHING;
		activeScene->deinit();
		_sceneStack.pop_back();
		if (_sceneStack.size() != 0) {
			_sceneStack.back()->init();
		}
		else {
			_shouldQuit = true;
		}
		break;
	case SceneAction::CREATE_LOSE_SCENE:
		if (activeScene->eatKeys) {
			InputManager::getInstance()->eatKey(-1);
		}
		activeScene->action = SceneAction::NOTHING;
		_sceneStack.push_back(std::make_shared<LoseScene>(_sdlManager.customRenderer, activeScene->playerScores, activeScene->difficulty));
		activeScene->deinit();
		_sceneStack.back()->init();
		for (int i = 0; i < 4; i++) {
			//_sceneStack.back()->playerScores[i] = activeScene->playerScores[i];
		}
		//InputManager::getInstance()->eatKey(-1);
		break;
	case SceneAction::CREATE_GAME_SCENE:
		SDLMusicManager::getInstance()->playSound("click");
		if (activeScene->eatKeys) {
			InputManager::getInstance()->eatKey(-1);
		}
		activeScene->action = SceneAction::NOTHING;
		_sceneStack.push_back(std::make_shared<GameScene>(_sdlManager.customRenderer, activeScene->difficulty));
		activeScene->deinit();
		_sceneStack.back()->init();
		break;
	case SceneAction::CREATE_CREDITS_SCENE:
		SDLMusicManager::getInstance()->playSound("click");
		if (activeScene->eatKeys) {
			InputManager::getInstance()->eatKey(-1);
		}
		activeScene->action = SceneAction::NOTHING;
		_sceneStack.push_back(std::make_shared<CreditsScene>(_sdlManager.customRenderer));
		activeScene->deinit();
		_sceneStack.back()->init();
		break;
	case SceneAction::CREATE_LEVEL_SELECT_SCENE:
		SDLMusicManager::getInstance()->playSound("click");
		if (activeScene->eatKeys) {
			InputManager::getInstance()->eatKey(-1);
		}
		activeScene->action = SceneAction::NOTHING;
		_sceneStack.push_back(std::make_shared<LevelSelectScene>(_sdlManager.customRenderer));
		activeScene->deinit();
		_sceneStack.back()->init();
		break;	
	case SceneAction::CREATE_OPTIONS_SCENE:
		SDLMusicManager::getInstance()->playSound("click");
		if (activeScene->eatKeys) {
			InputManager::getInstance()->eatKey(-1);
		}
		activeScene->action = SceneAction::NOTHING;
		_sceneStack.push_back(std::make_shared<OptionsScene>(_sdlManager.customRenderer));
		activeScene->deinit();
		_sceneStack.back()->init();
		break;
	case SceneAction::CREATE_POWERUP_UNLOCK_SCENE:
		if (activeScene->eatKeys) {
			InputManager::getInstance()->eatKey(-1);
		}
		activeScene->action = SceneAction::NOTHING;
		_sceneStack.push_back(std::make_shared<PowerupUnlockScene>(_sdlManager.customRenderer));
		activeScene->deinit();
		_sceneStack.back()->init();
		break;
	case SceneAction::CREATE_POWERUP_UNLOCK_AND_LOSE_SCENE:
		if (activeScene->eatKeys) {
			InputManager::getInstance()->eatKey(-1);
		}
		activeScene->action = SceneAction::NOTHING;
		_sceneStack.push_back(std::make_shared<LoseScene>(_sdlManager.customRenderer, activeScene->playerScores, activeScene->difficulty));
		_sceneStack.push_back(std::make_shared<PowerupUnlockScene>(_sdlManager.customRenderer));
		activeScene->deinit();
		_sceneStack.back()->init();
		break;
	case SceneAction::CREATE_CREDITS_AND_LOSE_SCENE:
		if (activeScene->eatKeys) {
			InputManager::getInstance()->eatKey(-1);
		}
		activeScene->action = SceneAction::NOTHING;
		_sceneStack.push_back(std::make_shared<LoseScene>(_sdlManager.customRenderer, activeScene->playerScores, activeScene->difficulty));
		activeScene->deinit();
		_sceneStack.push_back(std::make_shared<CreditsScene>(_sdlManager.customRenderer));
		_sceneStack.back()->init();
		break;
	case SceneAction::CREATE_LEADERBOARDS_SCENE:
		SDLMusicManager::getInstance()->playSound("click");
		if (activeScene->eatKeys) {
			InputManager::getInstance()->eatKey(-1);
		}
		activeScene->action = SceneAction::NOTHING;
		_sceneStack.push_back(std::make_shared<LeaderboardsScene>(_sdlManager.customRenderer));
		activeScene->deinit();
		_sceneStack.back()->init();
		break;
	}

	
	if (InputManager::getInstance()->shouldQuit() || _sceneStack.size() == 0) {
		//std::cout << "goodbye " << std::endl;
		//std::cout << "seed=" << RandomNumberManager::getInstance()->getSeed() << std::endl;
		_shouldQuit = true;
	}
	else if (oldScene != _sceneStack.back()) {
		_backgroundScene->onSceneChange(oldScene, _sceneStack.back());
	}

	++_tick;
}

void MainGame::render() {
	_sdlManager.beginRender();
	if (_sceneStack.back()->doBackground()) {
		_backgroundScene->render();
	}
	_sceneStack.back()->render();
	_sdlManager.finishRender();
}

