#pragma once
#include "stdafx.h"
enum class RandomType {
	locationX=0,
	locationY,
	speedX,
	isSpeedXNegative,
	speedY,
	isSpeedYNegative,
	generateXOrY,
	musicTime,
	starTwinkleTime,
	starNonTwinkleTime,
	starTwinkleSize,
	playerToRevive,
	isGiantComet,
	fireworkType,
	fireworkTime,
	fireworkCount,
	powerupRotation,
	LAST
};
class RandomNumberManager
{
public:
	RandomNumberManager(RandomNumberManager const&) = delete;
	void operator=(RandomNumberManager const&) = delete;
	static std::shared_ptr<RandomNumberManager> getInstance();
	void init(time_t time);
	double generateRandomDouble(double min, double max, RandomType randomType);
	int generateRandomInt(int minInclusive, int maxExclusive, RandomType randomType);
	time_t getSeed();
private:
	time_t _seed;
	RandomNumberManager();
	std::mt19937 randomSeeds[(int)RandomType::LAST];
	static std::shared_ptr<RandomNumberManager> _staticInstance;
};

