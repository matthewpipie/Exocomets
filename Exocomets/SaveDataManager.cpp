#include "SaveDataManager.h"
#include "SteamManager.h"
#include "ErrorHandler.h"
#include <fstream>
#include <sstream>

SaveData SaveDataManager::_currentSaveData = Constants::SAVE_DATA_EMPTY;

void SaveDataManager::init() {
	std::ifstream saveDataFile;
	SaveDataVerification verified = Constants::SAVE_DATA_UNVERIFIED;
	saveDataFile.open(Constants::SAVE_DATA_PATH_PREFIX + Constants::SAVE_DATA_PATH + "_" + std::to_string(SteamManager::getInstance()->getSteamID()) + Constants::SAVE_DATA_PATH_SUFFIX);
	bool t = false;
	if (!saveDataFile.is_open()) {
		//std::cerr << "ERROR: unable to open/create savedata.dat (" + std::string(Constants::SETTINGS_PATH) + ").  Shutting down..." << std::endl;
		t = true;
	}
	else {
		std::string realSaveData = decryptFile(saveDataFile);
		saveDataFile.close();


		std::string buff{ "" };
		std::vector<std::string> v;

		for (auto n : realSaveData)
		{
			if (n != Constants::SAVE_DATA_DELIM) buff += n; else
				if (n == Constants::SAVE_DATA_DELIM && buff != "") { v.push_back(buff); buff = ""; }
		}
		if (buff != "") v.push_back(buff);


		try {
			for (unsigned int i = 0; i < v.size(); i++) {
				_currentSaveData.highScores[i] = stoi(v[i]);
				verified.highScores++;
			}
		}
		catch (...) {
			t = true;
		}

	}
	if (verified != Constants::SAVE_DATA_VERIFIED || t) {
		_currentSaveData = Constants::SAVE_DATA_EMPTY;
		writeSaveData();
	}
	SteamManager::getInstance()->uploadScore(Difficulty::LEVEL_1, _currentSaveData.highScores[0]);
	SteamManager::getInstance()->uploadScore(Difficulty::LEVEL_2, _currentSaveData.highScores[1]);
	SteamManager::getInstance()->uploadScore(Difficulty::LEVEL_3, _currentSaveData.highScores[2]);
	SteamManager::getInstance()->uploadScore(Difficulty::LEVEL_4, _currentSaveData.highScores[3]);
	SteamManager::getInstance()->uploadScore(Difficulty::LEVEL_5, _currentSaveData.highScores[4]);
	SteamManager::getInstance()->uploadScore(Difficulty::LEVEL_6, _currentSaveData.highScores[5]);

}
int SaveDataManager::submitScore(Difficulty difficulty, int score) {
	if (score > _currentSaveData.highScores[(int)difficulty - 1]) {
		_currentSaveData.highScores[(int)difficulty - 1] = score;
		writeSaveData();
		SteamManager::getInstance()->uploadScore(difficulty, score);
	}
	return _currentSaveData.highScores[(int)difficulty - 1];
}

void SaveDataManager::writeSaveData() {
	std::string output = "";
	for (int i = 0; i < sizeof(_currentSaveData.highScores) / sizeof(_currentSaveData.highScores[0]); i++) {
		output += std::to_string(_currentSaveData.highScores[i]) + Constants::SAVE_DATA_DELIM;
	}
	output.pop_back();
	std::ofstream saveDataFile(Constants::SAVE_DATA_PATH_PREFIX + Constants::SAVE_DATA_PATH + "_" + std::to_string(SteamManager::getInstance()->getSteamID()) + Constants::SAVE_DATA_PATH_SUFFIX, std::ios::out | std::ios::trunc);
	if (!saveDataFile.is_open()) {
		ErrorHandler::error(ErrorCode::SAVE_DATA_CANT_CREATE);
	}
	std::string enc = encryptString(output);
	saveDataFile << enc;
	saveDataFile.close();
}

std::string SaveDataManager::encryptString(std::string input) {
	std::string output;
	for (unsigned int i = 0; i < input.size(); i++) {
		/*if (i % Constants::SAVE_DATA_JIBBERISH_RATE == 0) {
			output += i * (Constants::SAVE_DATA_ENCRYPTION_MULTIPLIER1 + Constants::SAVE_DATA_ENCRYPTION_CONSTANT - Constants::SAVE_DATA_ENCRYPTION_MULTIPLIER2);
			i--;
		}
		else {
*/
		//output += (input[i] * Constants::SAVE_DATA_ENCRYPTION_MULTIPLIER1 + i * Constants::SAVE_DATA_ENCRYPTION_MULTIPLIER2 + Constants::SAVE_DATA_ENCRYPTION_CONSTANT);
		output += input[i] ^ (Constants::SAVE_DATA_KEY[i % 3] + (SteamManager::getInstance()->getSteamID() + (SteamManager::getInstance()->getSteamID() + Constants::SAVE_DATA_ENCRYPTION_CONSTANT + i * Constants::SAVE_DATA_ENCRYPTION_MULTIPLIER2) * Constants::SAVE_DATA_ENCRYPTION_MULTIPLIER1) % 255);
		int asdf = 0;
//		}
	}
	return output;
}

std::string SaveDataManager::decryptFile(std::ifstream &file) {
	std::string output = "";
	file.seekg(0, std::ios::end);
	int chars = file.tellg();
	file.seekg(0, std::ios::beg);

	std::stringstream ss;
	ss << file.rdbuf();
	std::string input = ss.str();
	file.seekg(0, std::ios::beg);

	std::vector<char> vec;

	for (int i = 0; i < chars; i++) {
		char o;
		//file.seekg(i);
		file.get(o);
		auto a = file.tellg();
			//if (i % Constants::SAVE_DATA_JIBBERISH_RATE) {
				//	continue;
			//}
		//char b = input[i];
		//output += (o - Constants::SAVE_DATA_ENCRYPTION_CONSTANT - i * Constants::SAVE_DATA_ENCRYPTION_MULTIPLIER2) / Constants::SAVE_DATA_ENCRYPTION_MULTIPLIER1;
		output += o ^ (Constants::SAVE_DATA_KEY[i % 3] + (SteamManager::getInstance()->getSteamID() + (SteamManager::getInstance()->getSteamID() + Constants::SAVE_DATA_ENCRYPTION_CONSTANT + i * Constants::SAVE_DATA_ENCRYPTION_MULTIPLIER2) * Constants::SAVE_DATA_ENCRYPTION_MULTIPLIER1) % 255);
		//vec.push_back((o - Constants::SAVE_DATA_ENCRYPTION_CONSTANT - i * Constants::SAVE_DATA_ENCRYPTION_MULTIPLIER2) / Constants::SAVE_DATA_ENCRYPTION_MULTIPLIER1);
		int asdf = 0;
	}
	
	return output;
}
SaveData SaveDataManager::getCurrentSaveData() {
	return _currentSaveData;
}
