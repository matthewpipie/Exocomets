#include "Camera.h"


Camera::Camera() :
	Camera(XYSystem(Constants::CAMERA_CENTER_INIT.x, Constants::CAMERA_CENTER_INIT.y))
{
}

Camera::Camera(XYSystem centralLocation) :
	Camera(centralLocation, XYSystem(Constants::SCREEN_SIZE_LOGICAL.x, Constants::SCREEN_SIZE_LOGICAL.y))
{
}

Camera::Camera(XYSystem centralLocation, XYSystem size) :
	_followEntity(nullptr),
	_centralLocation(centralLocation),
	_size(size)
{
}


Camera::~Camera()
{
}

XYSystem Camera::getCameraCenter() {
	return _centralLocation;
}
void Camera::setCameraCenter(XYSystem newCenter) {
	_centralLocation = newCenter;
}
void Camera::moveCamera(XYSystem movement) {
	_centralLocation += movement;
}
XYSystem Camera::getBottomLeft() {
	return _centralLocation - _size / 2.0;
}
void Camera::setBottomLeft(XYSystem newBottomLeft) {
	_centralLocation = newBottomLeft + _size / 2.0;
}
XYSystem Camera::getTopRight() {
	return _centralLocation + _size / 2.0;
}
void Camera::setTopRight(XYSystem newTopRight) {
	_centralLocation = newTopRight - _size / 2.0;
}
XYSystem Camera::getBottomRight() {
	return XYSystem(getTopRight().x, getBottomLeft().y);
}
XYSystem Camera::getTopLeft() {
	return XYSystem(getBottomLeft().x, getTopRight().y);
}
XYSystem Camera::getSize() {
	return _size;
}
void Camera::setSize(XYSystem newSize) {
	_size = newSize;
}
void Camera::setZoom(double zoom) {
	setZoom(XYSystem(zoom, zoom));
}
void Camera::setZoom(XYSystem zoom) {
	//std::cout << "setting size from " << _size.x << "," << _size.y << " to ";
	_size.x /= zoom.x;
	_size.y /= zoom.y;
	//std::cout << _size.x << "," << _size.y << std::endl;
}

void Camera::followEntity(std::shared_ptr<Entity> followEnt) {
	_followEntity = followEnt;
}
void Camera::stopFollowingEntity() {
	_followEntity = nullptr;
}
bool Camera::isFollowingEntity() {
	return _followEntity != nullptr;
}
void Camera::swapFollowEntity(std::shared_ptr<Entity> followEnt) {
	if (isFollowingEntity()) {
		stopFollowingEntity();
	}
	else {
		followEntity(followEnt);
	}
}

void Camera::keepFollowingFollowEntity() {
	if (isFollowingEntity()) {
		_centralLocation = _followEntity->location;
	}
}

RectangleEntity Camera::getCameraEntity() {
	return RectangleEntity(_centralLocation, getEntityHitbox(), 0, XYSystem(0, 0));
}

XYSystem Camera::convertHudToReal(XYSystem hud)
{
	XYSystem size = Constants::SCREEN_SIZE_LOGICAL.makeDouble();
	return hud.divideComponents(size).multiplyComponents(_size) + getBottomLeft();
}

std::vector<XYSystem> Camera::getEntityHitbox() {
	return _size.getSizeVector();
}