#include "SDLMusicManager.h"
#include "Constants.h"
#include "ErrorHandler.h"
#include "RandomNumberManager.h"


std::shared_ptr<SDLMusicManager> SDLMusicManager::_staticInstance = nullptr;

SDLMusicManager::SDLMusicManager() :
	_currentlyPlaying(""),
	_currentMusicVolume(0),
	_temp(false)
{
}

SDLMusicManager::~SDLMusicManager()
{
}


void SDLMusicManager::init() {
	SDL_InitSubSystem(SDL_INIT_AUDIO);
	// Initialize SDL_mixer
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0) {
		ErrorHandler::error(ErrorCode::MUS_INIT);
	}

	// Load music
	for (int i = 0; i < Constants::MUSIC_COUNT; i++) {
		_musicMap.emplace(std::string(Constants::MUSIC_NAMES[i]), std::make_shared<SDLMusic>(Constants::MUSIC_NAMES[i], Constants::PATH_BASE_LOCATION + Constants::MUSIC_PATH_PREFIX + Constants::MUSIC_PATHS[i] + Constants::MUSIC_PATH_SUFFIX, Constants::MUSIC_BPMS[i], Constants::MUSIC_OFFSETS[i], Constants::MUSIC_START_TIMES[i]));
	}

	// Load sound effects
	for (int i = 0; i < Constants::SOUND_EFFECT_COUNT; i++) {
		_soundEffectMap.emplace(std::string(Constants::SOUND_EFFECT_NAMES[i]), std::make_shared<SDLSoundEffect>(Constants::SOUND_EFFECT_NAMES[i], Constants::PATH_BASE_LOCATION + Constants::SOUND_EFFECT_PATH_PREFIX + Constants::SOUND_EFFECT_PATHS[i] + Constants::SOUND_EFFECT_PATH_SUFFIX));
	}
}


void SDLMusicManager::unload() {
	_soundEffectMap.clear();
	_musicMap.clear();
	Mix_CloseAudio();
}


void SDLMusicManager::playMusic(std::string name) {
	playMusic(name, false);
}

void SDLMusicManager::playMusic(std::string name, bool fromBeginning)
{
	if (_temp)	setMusicVolume(_currentMusicVolume);
	auto search = _musicMap.find(name);
	if (search == _musicMap.end()) {
		ErrorHandler::error(ErrorCode::SDL_MUSIC_MANAGER_MUSIC, name);
	}
	else {
		Mix_PlayMusic(_musicMap[name]->sdlMixMusic, -1);
		if (!fromBeginning) {
			Mix_SetMusicPosition(Constants::MUSIC_START_JUMP + _musicMap[name]->START_TIMES[
				RandomNumberManager::getInstance()->generateRandomInt(0, _musicMap[name]->START_TIMES.size(), RandomType::musicTime)]);
		}
		_currentlyPlaying = name;
	}
}

void SDLMusicManager::playMusicIfNotPlaying(std::string name) {
	if (_temp)	setMusicVolume(_currentMusicVolume);
	if (name != _currentlyPlaying) {
		playMusic(name);
	}
}

void SDLMusicManager::playMusicFromBeginningIfNotPlaying(std::string name)
{
	if (_temp)	setMusicVolume(_currentMusicVolume);
	playMusic(name, name != _currentlyPlaying);
}


void SDLMusicManager::pauseMusic() {
	if (_temp)	setMusicVolume(_currentMusicVolume);
	Mix_PauseMusic();
}
void SDLMusicManager::resumeMusic() {
	if (_temp)	setMusicVolume(_currentMusicVolume);
	Mix_ResumeMusic();
}

void SDLMusicManager::haltMusic()
{
	if (_temp)	setMusicVolume(_currentMusicVolume);
	Mix_HaltMusic();
}

void SDLMusicManager::playSound(std::string name) {
	//std::cout << "playings " << name << std::endl;
	auto search = _soundEffectMap.find(name);
	if (search == _soundEffectMap.end()) {
		ErrorHandler::error(ErrorCode::SDL_MUSIC_MANAGER_SOUND, name);
	}
	else {
		Mix_PlayChannel(-1, _soundEffectMap[name]->sdlMixChunk, 0);
	}
	//first unload current song unless its = to requested
	//then play ewn one
}

void SDLMusicManager::setMusicVolume(int newVolume) {
	setMusicVolume(newVolume, false);
}

void SDLMusicManager::setSoundVolume(int newVolume) {
	Mix_Volume(-1, newVolume);
}

void SDLMusicManager::tempVolumeDown(double mp)
{
	if (mp < 0) mp = 0;
	setMusicVolume((int)(_currentMusicVolume * mp), true);
}

void SDLMusicManager::endTempVolumeDown()
{
	if (_temp)	setMusicVolume(_currentMusicVolume);
}

void SDLMusicManager::setMusicVolume(int newVolume, bool isTemp)
{
	if (isTemp) {
		_temp = true;
	}
	else {
		_currentMusicVolume = newVolume;
		_temp = false;
	}
	Mix_VolumeMusic(newVolume);
}

std::shared_ptr<SDLMusicManager> SDLMusicManager::getInstance() {
	if (!_staticInstance)
		_staticInstance = std::shared_ptr<SDLMusicManager>(new SDLMusicManager);
	//std::cout << "getting used!" << std::endl;
	return _staticInstance;
}
