#include "SettingsManager.h"
#include "ErrorHandler.h"
#include <fstream>
#include <iterator>

const std::unordered_map<std::string, std::regex> SettingsManager::_regexMap = {
	{"shipColors", std::regex("shipColors\\[\\d+\\]\\[\\d+\\]")},
	{"playerCount", std::regex("playerCount")},
	{"controls", std::regex("controls\\[\\d+\\]\\[\\d+\\]")},
	{"sillySounds", std::regex("sillySounds")},
	{"fontColor", std::regex("fontColor\\[\\d+\\]")},
	{"windowMode", std::regex("windowMode")},
	{"windowSize", std::regex("windowSize\\[\\d+\\]")},
	{"desiredFPS", std::regex("desiredFPS")},
	{"sensMultipliers", std::regex("sensMultipliers\\[\\d+\\]")},
	{"backgroundColor", std::regex("backgroundColor\\[\\d+\\]")},
	{"enableVSync", std::regex("enableVSync")},
	{"musicVolume", std::regex("musicVolume")},
	{"soundVolume", std::regex("soundVolume")}
};
Settings SettingsManager::_currentSettings = Constants::SETTINGS_DEFAULT;
Settings SettingsManager::_oldSettings = Constants::SETTINGS_DEFAULT;
Settings SettingsManager::getSettings() {
	return _currentSettings;
}
void SettingsManager::init() {
	readSettings();
	beginEditing();
}
void SettingsManager::readSettings() {

	std::ifstream settingsFile;
	settingsFile.open(Constants::SETTINGS_PATH_PREFIX + Constants::SETTINGS_PATH + Constants::SETTINGS_PATH_SUFFIX);
	bool t = false;
	if (!settingsFile.is_open()) {
		//std::cerr << "ERROR: unable to open/create settings.ini (" + std::string(Constants::SETTINGS_PATH) + ").  Shutting down..." << std::endl;
		t = true;
	}
	std::string name;
	std::string val;
	try {
		SettingsVerification verified = Constants::SETTINGS_UNVERIFIED;
		if (t) throw 3;
		while (!settingsFile.eof()) {
			std::getline(settingsFile, name, Constants::SETTINGS_NAME_VAL_DELIM);
			std::getline(settingsFile, val, Constants::SETTINGS_VAL_NAME_DELIM);

			if (std::regex_match(name, _regexMap.at("shipColors"))) {
				std::vector<std::string> matches;
				std::regex re("\\d+");
				std::sregex_token_iterator begin(name.begin(), name.end(), re), end;
				std::copy(begin, end, std::back_inserter(matches)); //get digits in matches

				_currentSettings.shipColors[stoi(matches[0])][stoi(matches[1])] = stoi(val);
				verified.shipColors++;
			}
			else if (std::regex_match(name, _regexMap.at("playerCount"))) {
				_currentSettings.playerCount = stoi(val);
				verified.playerCount++;
			}
			else if (std::regex_match(name, _regexMap.at("controls"))) {
				std::vector<std::string> matches;
				std::regex re("\\d+");
				std::sregex_token_iterator begin(name.begin(), name.end(), re), end;
				std::copy(begin, end, std::back_inserter(matches)); //get digits in matches

				_currentSettings.controls[stoi(matches[0])][stoi(matches[1])] = stoi(val);
				verified.controls++;
			}
			else if (std::regex_match(name, _regexMap.at("sillySounds"))) {
				_currentSettings.sillySounds = (bool)stoi(val);
				verified.sillySounds++;
			}
			else if (std::regex_match(name, _regexMap.at("fontColor"))) {
				std::vector<std::string> matches;
				std::regex re("\\d+");
				std::sregex_token_iterator begin(name.begin(), name.end(), re), end;
				std::copy(begin, end, std::back_inserter(matches)); //get digits in matches

				_currentSettings.fontColor[stoi(matches[0])] = stoi(val);
				verified.fontColor++;
			}
			else if (std::regex_match(name, _regexMap.at("windowMode"))) {
				_currentSettings.windowMode = (WindowMode)stoi(val);
				verified.windowMode++;
			}
			else if (std::regex_match(name, _regexMap.at("windowSize"))) {
				std::vector<std::string> matches;
				std::regex re("\\d+");
				std::sregex_token_iterator begin(name.begin(), name.end(), re), end;
				std::copy(begin, end, std::back_inserter(matches)); //get digits in matches

				_currentSettings.windowSize[stoi(matches[0])] = stoi(val);
				verified.windowSize++;
			}
			else if (std::regex_match(name, _regexMap.at("desiredFPS"))) {
				_currentSettings.desiredFPS = stoi(val);
				verified.desiredFPS++;
			}
			else if (std::regex_match(name, _regexMap.at("sensMultipliers"))) {
				std::vector<std::string> matches;
				std::regex re("\\d+");
				std::sregex_token_iterator begin(name.begin(), name.end(), re), end;
				std::copy(begin, end, std::back_inserter(matches)); //get digits in matches

				_currentSettings.sensMultipliers[stoi(matches[0])] = stod(val);
				verified.sensMultipliers++;
			}
			else if (std::regex_match(name, _regexMap.at("backgroundColor"))) {
				std::vector<std::string> matches;
				std::regex re("\\d+");
				std::sregex_token_iterator begin(name.begin(), name.end(), re), end;
				std::copy(begin, end, std::back_inserter(matches)); //get digits in matches

				_currentSettings.backgroundColor[stoi(matches[0])] = stoi(val);
				verified.backgroundColor++;
			}
			else if (std::regex_match(name, _regexMap.at("enableVSync"))) {
				_currentSettings.enableVSync = (bool)stoi(val);
				verified.enableVSync++;
			}
			else if (std::regex_match(name, _regexMap.at("musicVolume"))) {
				_currentSettings.musicVolume = stoi(val);
				verified.musicVolume++;
			}
			else if (std::regex_match(name, _regexMap.at("soundVolume"))) {
				_currentSettings.soundVolume = stoi(val);
				verified.soundVolume++;
			}
			else if (name == "") {}
			else {
				std::cout << "Settings file error: " << name << " cannot be " << val << std::endl;
				throw 1;
			}
		}
		if (verified != Constants::SETTINGS_VERIFIED) throw 2;
		settingsFile.close();
	}
	catch (...) { // If any of the string to int/doubles failed, just scrap it all and remake the file
		settingsFile.close();
		/*settingsFile.open(Constants::SETTINGS_PATH, std::ios::trunc);
		settingsFile << getDefaultSettings().c_str();
		*/
		_currentSettings = Constants::SETTINGS_DEFAULT;
		saveSettings();
	}

}
void SettingsManager::writeSettings(Settings settings) {
	std::ofstream settingsFile;
	settingsFile.open(Constants::SETTINGS_PATH_PREFIX + Constants::SETTINGS_PATH + Constants::SETTINGS_PATH_SUFFIX, std::ios::out | std::ios::trunc);
	if (!settingsFile.is_open()) {
		ErrorHandler::error(ErrorCode::SETTINGS_CANT_CREATE);
	}
	std::cout << "writing to settings.ini..." << std::endl;
	std::string settingsToWrite = "";

	int shipColorsSize = sizeof(Constants::SETTINGS_DEFAULT.shipColors) / sizeof(Constants::SETTINGS_DEFAULT.shipColors[0]);
	int shipColors = sizeof(Constants::SETTINGS_DEFAULT.shipColors[0]) / sizeof(Constants::SETTINGS_DEFAULT.shipColors[0][0]);
	for (int i = 0; i < shipColorsSize; i++) {
		for (int j = 0; j < shipColors; j++) {
			settingsToWrite += "shipColors[" + std::to_string(i) + "][" + std::to_string(j) + "]=" + std::to_string(settings.shipColors[i][j]) + "\n";
		}
	}

	settingsToWrite += "playerCount=" + std::to_string(settings.playerCount) + "\n";

	int amountOfPlayers = sizeof(Constants::SETTINGS_DEFAULT.controls) / sizeof(Constants::SETTINGS_DEFAULT.controls[0]);
	int directions = sizeof(Constants::SETTINGS_DEFAULT.controls[0]) / sizeof(Constants::SETTINGS_DEFAULT.controls[0][0]);
	for (int i = 0; i < amountOfPlayers; i++) {
		for (int j = 0; j < directions; j++) {
			settingsToWrite += "controls[" + std::to_string(i) + "][" + std::to_string(j) + "]=" + std::to_string(settings.controls[i][j]) + "\n";
		}
	}

	settingsToWrite += "sillySounds=" + std::to_string((int)settings.sillySounds) + "\n";


	int colorSize = sizeof(Constants::SETTINGS_DEFAULT.fontColor) / sizeof(Constants::SETTINGS_DEFAULT.fontColor[0]);
	for (int i = 0; i < colorSize; i++) {
		settingsToWrite += "fontColor[" + std::to_string(i) + "]=" + std::to_string(settings.fontColor[i]) + "\n";
	}

	settingsToWrite += "windowMode=" + std::to_string((int)settings.windowMode) + "\n";

	int screenDimensions = sizeof(Constants::SETTINGS_DEFAULT.windowSize) / sizeof(Constants::SETTINGS_DEFAULT.windowSize[0]);
	for (int i = 0; i < screenDimensions; i++) {
		settingsToWrite += "windowSize[" + std::to_string(i) + "]=" + std::to_string(settings.windowSize[i]) + "\n";
	}

	settingsToWrite += "desiredFPS=" + std::to_string(settings.desiredFPS) + "\n";

	int amountOfPlayers2 = sizeof(Constants::SETTINGS_DEFAULT.sensMultipliers) / sizeof(Constants::SETTINGS_DEFAULT.sensMultipliers[0]);
	for (int i = 0; i < amountOfPlayers2; i++) {
		settingsToWrite += "sensMultipliers[" + std::to_string(i) + "]=" + std::to_string(settings.sensMultipliers[i]) + "\n";
	}

	int colorSize2 = sizeof(Constants::SETTINGS_DEFAULT.backgroundColor) / sizeof(Constants::SETTINGS_DEFAULT.backgroundColor[0]);
	for (int i = 0; i < colorSize2; i++) {
		settingsToWrite += "backgroundColor[" + std::to_string(i) + "]=" + std::to_string(settings.backgroundColor[i]) + "\n";
	}

	settingsToWrite += "enableVSync=" + std::to_string((int)settings.enableVSync) + "\n";

	settingsToWrite += "musicVolume=" + std::to_string(settings.musicVolume) + "\n";

	settingsToWrite += "soundVolume=" + std::to_string(settings.soundVolume) + "\n";

	settingsFile << settingsToWrite;
	settingsFile.close();
}
bool SettingsManager::needsRestart(Settings s1, Settings s2) {
	if (s1.windowMode != s2.windowMode) {
		return true;
	}
	if (!std::equal(std::begin(s1.windowSize), std::end(s1.windowSize), std::begin(s2.windowSize))) {
		return true;//If you change windowSize
	}
	return false;
}
bool SettingsManager::saveSettings() {
	bool gameNeedsRestart = needsRestart(_currentSettings, _oldSettings);

	_oldSettings = _currentSettings;
	writeSettings(_currentSettings);
	return gameNeedsRestart;
}
void SettingsManager::beginEditing() {
	_oldSettings = _currentSettings;
}
void SettingsManager::resetDefault() {
	_currentSettings = Constants::SETTINGS_DEFAULT;
}
void SettingsManager::discardChanges() {
	_currentSettings = _oldSettings;
}

/*
Setting SettingsManager::findSetting(std::string name) {
	Setting results;
	try {
		results.stringS = _stringSettings.at(name);
	}
	catch (std::out_of_range) {}
	try {
		results.doubleS = _doubleSettings.at(name);
	}
	catch (std::out_of_range) {}
	try {
		results.intS = _intSettings.at(name);
	}
	catch (std::out_of_range) {}
	return results;
}

std::string SettingsManager::getDefaultSettings() { //each setting seperated by a newline
	std::string settingsDefault = "";

	int shipColorsSize = sizeof(Constants::SHIP_COLORS_DEFAULT) / sizeof(Constants::SHIP_COLORS_DEFAULT[0]);
	int shipColors = sizeof(Constants::SHIP_COLORS_DEFAULT[0]) / sizeof(Constants::SHIP_COLORS_DEFAULT[0][0]);
	for (int i = 0; i < shipColorsSize; i++) {
		for (int j = 0; j < shipColors; j++) {
			settingsDefault += "int shipColors[" + std::to_string(i) + "][" + std::to_string(j) + "]=" + std::to_string(Constants::SHIP_COLORS_DEFAULT[i][j]) + "\n";
		}
	}

	settingsDefault += "int starCount=" + std::to_string(Constants::STAR_COUNT_DEFAULT) + "\n";

	settingsDefault += "int playerCount=" + std::to_string(Constants::PLAYER_COUNT_DEFAULT) + "\n";

	int amountOfPlayers = sizeof(Constants::CONTROLS_DEFAULT) / sizeof(Constants::CONTROLS_DEFAULT[0]);
	int directions = sizeof(Constants::CONTROLS_DEFAULT[0]) / sizeof(Constants::CONTROLS_DEFAULT[0][0]);
	for (int i = 0; i < amountOfPlayers; i++) {
		for (int j = 0; j < directions; j++) {
			settingsDefault += "int controls[" + std::to_string(i) + "][" + std::to_string(j) + "]=" + std::to_string(Constants::CONTROLS_DEFAULT[i][j]) + "\n";
		}
	}

	settingsDefault += "double fontSize=" + std::to_string(Constants::REGULAR_HEIGHT_DEFAULT) + "\n";

	settingsDefault += "int windowMode=" + std::to_string((int)Constants::WINDOW_MODE_DEFAULT) + "\n";

	return settingsDefault;
}*/
/*
const std::string SettingsManager::DEFAULT_SETTINGS = //don't talk to me or my code ever again
"int shipColors[0][0]=" + std::to_string(Constants::SHIP_COLORS_DEFAULT[0][0]) +
"int shipColors[0][1]=" + std::to_string(Constants::SHIP_COLORS_DEFAULT[0][1]) +
"int shipColors[0][2]=" + std::to_string(Constants::SHIP_COLORS_DEFAULT[0][2]) +
"int shipColors[1][0]=" + std::to_string(Constants::SHIP_COLORS_DEFAULT[1][0]) +
"int shipColors[1][1]=" + std::to_string(Constants::SHIP_COLORS_DEFAULT[1][1]) +
"int shipColors[1][2]=" + std::to_string(Constants::SHIP_COLORS_DEFAULT[1][2]) +
"int shipColors[2][0]=" + std::to_string(Constants::SHIP_COLORS_DEFAULT[2][0]) +
"int shipColors[2][1]=" + std::to_string(Constants::SHIP_COLORS_DEFAULT[2][1]) +
"int shipColors[2][2]=" + std::to_string(Constants::SHIP_COLORS_DEFAULT[2][2]) +
"int shipColors[3][0]=" + std::to_string(Constants::SHIP_COLORS_DEFAULT[3][0]) +
"int shipColors[3][1]=" + std::to_string(Constants::SHIP_COLORS_DEFAULT[3][1]) +
"int shipColors[3][2]=" + std::to_string(Constants::SHIP_COLORS_DEFAULT[3][2]) +
"int starCount=" + std::to_string(Constants::STAR_COUNT_DEFAULT) +
"int playerCount=" + std::to_string(Constants::PLAYER_COUNT_DEFAULT) +
"int controls[0][0]=" + std::to_string(Constants::CONTROLS_DEFAULT[0][0]) +
"int controls[0][1]=" + std::to_string(Constants::CONTROLS_DEFAULT[0][1]) +
"int controls[0][2]=" + std::to_string(Constants::CONTROLS_DEFAULT[0][2]) +
"int controls[0][3]=" + std::to_string(Constants::CONTROLS_DEFAULT[0][3]) +
"int controls[1][0]=" + std::to_string(Constants::CONTROLS_DEFAULT[1][0]) +
"int controls[1][1]=" + std::to_string(Constants::CONTROLS_DEFAULT[1][1]) +
"int controls[1][2]=" + std::to_string(Constants::CONTROLS_DEFAULT[1][2]) +
"int controls[1][3]=" + std::to_string(Constants::CONTROLS_DEFAULT[1][3]) +
"int controls[2][0]=" + std::to_string(Constants::CONTROLS_DEFAULT[2][0]) +
"int controls[2][1]=" + std::to_string(Constants::CONTROLS_DEFAULT[2][1]) +
"int controls[2][2]=" + std::to_string(Constants::CONTROLS_DEFAULT[2][2]) +
"int controls[2][3]=" + std::to_string(Constants::CONTROLS_DEFAULT[2][3]) +
"int controls[3][0]=" + std::to_string(Constants::CONTROLS_DEFAULT[3][0]) +
"int controls[3][1]=" + std::to_string(Constants::CONTROLS_DEFAULT[3][1]) +
"int controls[3][2]=" + std::to_string(Constants::CONTROLS_DEFAULT[3][2]) +
"int controls[3][3]=" + std::to_string(Constants::CONTROLS_DEFAULT[3][3]) +
"double fontSize=" + std::to_string(Constants::REGULAR_HEIGHT_DEFAULT) +
"int windowMode=" + std::to_string((int)Constants::WINDOW_MODE_DEFAULT);*/
