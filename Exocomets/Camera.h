#pragma once
#include "stdafx.h"
#include "RectangleEntity.h"
class Camera
{
public:
	Camera();
	Camera(XYSystem centralLocation);
	Camera(XYSystem centralLocation, XYSystem size);
	~Camera();
	XYSystem getCameraCenter();
	void setCameraCenter(XYSystem newCenter);
	void moveCamera(XYSystem movement);
	XYSystem getBottomLeft();
	void setBottomLeft(XYSystem newBottomLeft);
	XYSystem getTopRight();
	void setTopRight(XYSystem newTopRight);
	XYSystem getBottomRight();
	XYSystem getTopLeft();
	XYSystem getSize();
	void setSize(XYSystem newSize);
	void setZoom(double zoom);
	void setZoom(XYSystem zoom);

	void followEntity(std::shared_ptr<Entity> followEnt);
	void stopFollowingEntity();
	bool isFollowingEntity();
	// if isFollowingEntity will stop, if isn't, will start
	void swapFollowEntity(std::shared_ptr<Entity> followEnt);
	void keepFollowingFollowEntity();

	RectangleEntity getCameraEntity();

	XYSystem convertHudToReal(XYSystem hud);
private:
	std::shared_ptr<Entity> _followEntity;
	XYSystem _centralLocation;
	XYSystem _size;
	std::vector<XYSystem> getEntityHitbox();
};

