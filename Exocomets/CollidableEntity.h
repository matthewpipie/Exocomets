#pragma once
#include "Entity.h"
class Comet;
class Player;
class Powerup;
class BulletEntity;
class CollidableEntity :
	public Entity
{
public:
	CollidableEntity(XYSystem location, double radius, double mass, XYSystem speed);
	CollidableEntity(XYSystem location, std::vector<XYSystem> relativeHitbox, double mass, XYSystem speed);

	bool isCurrentlyColliding(const CollidableEntity &other) const;
	bool isPotentiallyColliding(const CollidableEntity &other) const;
	virtual void resolvePotentialCollision(CollidableEntity &other);
// DEFINE ALL INTERACTIONS BELOW, INCLUDE EVERY CollidableEntity CHILD
	virtual void resolvePotentialCollision(Comet &other);
	virtual void resolvePotentialCollision(Player &other);
	virtual void resolvePotentialCollision(Powerup &other);
	virtual void resolvePotentialCollision(BulletEntity &other);
private:

	bool isCircleAndCircleColliding(const double circle1Radius, const XYSystem &circle1Location, const double circle2Radius, const XYSystem &circle2Location) const;
	bool isCircleAndPolygonColliding(const std::vector<XYSystem> &relativePolygonHitbox, const XYSystem &polygonLocation, const double circleRadius, const XYSystem &circleLocation) const;
	bool isPolygonAndPolygonColliding(const std::vector<XYSystem> &relativePolygon1Hitbox, const XYSystem &polygon1Location, const std::vector<XYSystem> &relativePolygon2Hitbox, const XYSystem &polygon2Location) const;

	bool testSATAxis(XYSystem axisVector, const std::vector<XYSystem> &relativePolygon1Hitbox, const XYSystem &polygon1Location, const std::vector<XYSystem> &relativePolygon2Hitbox, const XYSystem &polygon2Location) const;
	bool testSATAxis(XYSystem axisVector, const std::vector<XYSystem> &relativePolygonHitbox, const XYSystem &polygonLocation, double circleRadius, const XYSystem &circleLocation) const;

	/*bool pointInPolygon(const XYSystem &point, const std::vector<XYSystem> &fakeHitboxes) const;
	bool lineIntersectsCircle(const XYSystem &p1, const XYSystem &p2, const double circleRadius, const XYSystem &circleLocation) const;
	bool satTestAxisOverlaps(const XYSystem &polygon1Location, const std::vector<XYSystem> &polygon1RelativeHitbox, const XYSystem &polygon2Location, const std::vector<XYSystem> &polygon2RelativeHitbox, const XYSystem &axisPoint1, const XYSystem &axisPoint2) const;
	*/
};

/*
how to add an entity subclass: (double dispatchment)
1. make subclass of collidableentity
2. add to list in // DEFINE...
3. add definition in CollidableEntity.cpp that is blank
4. in new entity.h add: 
	virtual void resolvePotentialCollision(CollidableEntity &other) override;
4. in new entity.cpp add: void <replace>::resolvePotentialCollision(CollidableEntity &other) {
	other.resolvePotentialCollision(*this);
}
5. in any other entity you want an interaction with, add a override (in both)
*/
