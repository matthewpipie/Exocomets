#pragma once
#include "stdafx.h"
#include "CollidableEntity.h"
#include "Camera.h"
#include "Slider.h"
#include "KeyOptionEntity.h"

class EntityBatch
{
public:
	EntityBatch();
	~EntityBatch();
	std::vector<std::shared_ptr<CollidableEntity>> entities;
	std::vector<std::shared_ptr<Entity>> unCollidableEntities;
	//std::shared_ptr<CollidableEntity> findOldestEntityNotInCamera(const RectangleEntity &cameraRect);
	void calculatePotentialLocations();
	void doCollisions(std::shared_ptr<Camera> camera);
	//int isTooFull();
	bool finishMovementsAndKillFlaggedAndWhatNeedsToDie(std::shared_ptr<Camera> camera, bool killOffscreen, std::vector<std::shared_ptr<Slider>> sliders);

	int *resolveActions(int currentScore, Difficulty diff);

	XYSystem nonEssentialSpeedMod;
private:

};

