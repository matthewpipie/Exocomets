#include "Sprite.h"
#include "SDLTextCreator.h"

Sprite::Sprite(const std::string filepath, XYSystem size) :
	Sprite(filepath, false, XYSystem(), size, Direction(), false, false, Constants::FULL_COLOR, false, []() {})
{
}

Sprite::Sprite(const std::string filepath, XYSystem size, std::function<void()> everyRender) :
	Sprite(filepath, false, XYSystem(), size, Direction(), false ,false, Constants::FULL_COLOR, false, everyRender)
{
}

Sprite::Sprite(const std::string text, XYSystem relativeLocation, double height, Direction rotation, bool horizontalFlip, bool verticalFlip, const Uint8 rgbaColorMod[4], bool isHudElement) :
	Sprite(text, true, relativeLocation, SDLTextCreator::getTextSize(text.c_str(), height), rotation, horizontalFlip, verticalFlip, rgbaColorMod, isHudElement, []() {})
{
}

Sprite::Sprite(const std::string filepath, XYSystem relativeLocation, XYSystem size, Direction rotation, bool horizontalFlip, bool verticalFlip, const Uint8 rgbaColorMod[4], bool isHudElement) :
	Sprite(filepath, false, relativeLocation, size, rotation, horizontalFlip, verticalFlip, rgbaColorMod, isHudElement, []() {})
{
}

Sprite::Sprite(const std::string filepath, XYSystem relativeLocation, XYSystem size, Direction rotation, bool horizontalFlip, bool verticalFlip, const Uint8 rgbaColorMod[4], bool isHudElement, Direction colorModD) :
	Sprite(filepath, false, relativeLocation, size, rotation, horizontalFlip, verticalFlip, rgbaColorMod, isHudElement, []() {}, Direction::makeFromDegs(360))
{
}

Sprite::Sprite(const std::string filepath, bool renderFilePathNameInsteadOfFile, XYSystem relativeLocation, XYSystem size, Direction rotation, bool horizontalFlip, bool verticalFlip, const Uint8 rgbaColorMod[4], bool isHudElement, std::function<void()> everyRender) :
	Sprite(filepath, renderFilePathNameInsteadOfFile, relativeLocation, size, rotation, horizontalFlip, verticalFlip, rgbaColorMod, isHudElement, everyRender, Direction::makeFromDegs(360))
{

}

Sprite::Sprite(const std::string filepath, bool renderFilePathNameInsteadOfFile, XYSystem relativeLocation, XYSystem size, Direction rotation, bool horizontalFlip, bool verticalFlip, const Uint8 rgbaColorMod[4], bool isHudElement, std::function<void()> everyRender, Direction colorModD) :
	relativeLocation(relativeLocation),
	size(size),
	sizeMod(XYSystem(1)),
	filepathOrText(filepath),
	rotation(rotation),
	horizontalFlip(horizontalFlip),
	verticalFlip(verticalFlip),
	renderFilepathNameInsteadOfFile(renderFilePathNameInsteadOfFile),
	isHudElement(isHudElement),
	everyRender(everyRender),
	colorModD(colorModD)
{
	for (int i = 0; i < (sizeof(RGBAColorMod) / sizeof(RGBAColorMod[0])); i++) {
		RGBAColorMod[i] = rgbaColorMod[i];
	}
	if (isHudElement) {
		int a = 0;
	}
}


Sprite::~Sprite()
{
}

void Sprite::updateText(std::string newText, double fontSize) {
	filepathOrText = newText;
	size = SDLTextCreator::getTextSize(newText.c_str(), fontSize);
}