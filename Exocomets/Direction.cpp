#include "Direction.h"
#include "ErrorHandler.h"

Direction::Direction() :
	Direction(0)
{
}
Direction::Direction(double rads) :
	radians(fixRadians(rads))
{
}
Direction::Direction(XYSystem start, XYSystem end) :
	Direction(getAngleRadians(start, end))
{
}

double Direction::degrees() {
	double ret = radians * 180.0 / M_PI;
	while (ret < 0 || ret >= 360) {
		if (ret < 0) {
			ret += 360;
		}
		else {
			ret -= 360;
		}
	}
	return ret;
}

Direction Direction::makeFromDegs(double degs)
{
	return Direction(degs * M_PI / 180.0);
}

Direction Direction::opposite() {
	return rotateLeftRadians(M_PI);
}

Direction Direction::rotateLeftDegrees(double degs) {
	return rotateLeftRadians(rad(degs));
}
Direction Direction::rotateRightDegrees(double degs) {
	return rotateLeftRadians(-rad(degs));
}
Direction Direction::rotateLeftRadians(double rads) {
	return Direction(radians + rads);
}
Direction Direction::rotateRightRadians(double rads) {
	return rotateLeftRadians(-rads);
}
Direction Direction::rotateLeft(Direction amount) {
	return rotateLeftRadians(amount.radians);
}
Direction Direction::rotateRight(Direction amount) {
	return rotateLeftRadians(-amount.radians);
}
XYSystem Direction::polarToCartesian(double distance) {
	XYSystem r;
	r.x = distance * std::cos(radians);
	r.y = distance * std::sin(radians);
	return r;
}

/*Direction Direction::operator=(const Direction &O) {
	return *this = Direction(O.radians);
}*/

Direction::~Direction()
{
}

double Direction::getAngleRadians(XYSystem start, XYSystem end) {
	if (start == end) {
		ErrorHandler::error(ErrorCode::DIRECTION_PTS_EQUAL);
	}
	return atan2(end.y - start.y, end.x - start.x);
}
double Direction::rad(double degs) {
	return M_PI * degs / 180.0;
}

double Direction::fixRadians(double rads) {
	int changeDirection = 0;
	while ((changeDirection = getRadState(rads)) != 0) {
		rads += 2.0 * M_PI * changeDirection;
	}
	return rads;
}
int Direction::getRadState(double rads) {
	if (rads <= -1.0 * M_PI) {
		return 1;
	}
	else if (rads > M_PI) {
		return -1;
	}
	else {
		return 0;
	}
}
