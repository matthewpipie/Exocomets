#include "InputManager.h"
#include "Constants.h"
#include "SettingsManager.h"

std::shared_ptr<InputManager> InputManager::_staticInstance = nullptr;

InputManager::InputManager() :
	_quit(false),
	_mouseMovement(XYIntSystem()),
	_lastPressedKey(0),
	_mouseDown(KeyState::KEY_UP) {
	for (int i = 0; i < sizeof(_keysDown) / sizeof(_keysDown[0]); i++) {
		_keysDown[i] = KeyState::KEY_UP;
	}
}


InputManager::~InputManager()
{
}

const char* InputManager::getKeyName(int key) {
	return SDL_GetKeyName(key);
}

std::shared_ptr<InputManager> InputManager::getInstance() {
	if (!_staticInstance)
		_staticInstance = std::shared_ptr<InputManager>(new InputManager());
	return _staticInstance;
}

bool InputManager::isKeyDown(int key) {
	if (key == -1) return isMouseDown();
	rectifyKey(key);
	return _keysDown[key] == KeyState::KEY_DOWN || _keysDown[key] == KeyState::KEY_JUST_DOWN;
}

bool InputManager::isKeyJustDown(int key) {
	if (key == -1) return isMouseJustDown();
	rectifyKey(key);
	return _keysDown[key] == KeyState::KEY_JUST_DOWN;
}

void InputManager::pollInput() {
	setButtonsDown();

	SDL_Event ev;
	int key = 0;
	while (SDL_PollEvent(&ev)) {
		switch (ev.type) {
		case SDL_QUIT:
			_quit = true;
			break;
		case SDL_KEYDOWN:
			key = ev.key.keysym.sym;
			_lastPressedKey = key;
			rectifyKey(key);
			if (_keysDown[key] == KeyState::KEY_LOCK) break;
			if (_keysDown[key] == KeyState::KEY_UP) {
				_keysDown[key] = KeyState::KEY_JUST_DOWN;
			}
			break;
		case SDL_MOUSEBUTTONDOWN:
			if (_mouseDown == KeyState::KEY_UP) {
				_mouseDown = KeyState::KEY_JUST_DOWN;
				_lastPressedKey = -1;
			}
			break;
		case SDL_MOUSEBUTTONUP:
			_mouseDown = KeyState::KEY_UP;
			break;
		case SDL_KEYUP:
			key = ev.key.keysym.sym;
			rectifyKey(key);
			_keysDown[key] = KeyState::KEY_UP;
			break;
		case SDL_MOUSEMOTION:
			//SDL_GetRelativeMouseState(&_mouseMovement.x, &_mouseMovement.y);
			_mouseMovement.x += ev.motion.xrel;
			_mouseMovement.y -= ev.motion.yrel;
			//std::cout << "polerino" << std::endl;
			//std::cout << ((int)_mouseMovement.x) << std::endl;
			break;
		}
	}
}

int InputManager::rectifyKey(int &key) {
	if (key > 127) {
		key -= Constants::KEYBOARD_SDL_CONTROL_NUMBER;
	}
	return key;
}

int InputManager::unRectifyKey(int &key) {
	if (key > 127 && key < numberOfSDLKeys) {
		key += Constants::KEYBOARD_SDL_CONTROL_NUMBER;
	}
	return key;
}

std::string InputManager::getDisplayNameFromKey(int key) {
	if (key == -1) {
		return "Mouse";
	}
	return SDL_GetKeyName(unRectifyKey(key));
}

void InputManager::eatKey(int key)
{
	if (key == -1) {
		for (int i = 0; i < numberOfSDLKeys; i++) {
			if (_keysDown[i] != KeyState::KEY_UP) {
				_keysDown[i] = KeyState::KEY_LOCK;
			}
		}
	}
	else {
		rectifyKey(key);
		_keysDown[key] = KeyState::KEY_LOCK;
	}
}

int InputManager::getLatestPressedKey() {
	return unRectifyKey(_lastPressedKey);
}

void InputManager::setButtonsDown() {
	for (int i = 0; i < numberOfSDLKeys; ++i) {
		if (_keysDown[i] == KeyState::KEY_JUST_DOWN) {
			_keysDown[i] = KeyState::KEY_DOWN;
		}
	}
	if (_mouseDown == KeyState::KEY_JUST_DOWN) {
		_mouseDown = KeyState::KEY_DOWN;
	}
	_mouseMovement = XYIntSystem();
	_lastPressedKey = 0;
}

bool InputManager::isMouseDown() {
	return _mouseDown == KeyState::KEY_DOWN || _mouseDown == KeyState::KEY_JUST_DOWN;
}

bool InputManager::isMouseJustDown() {
	return _mouseDown == KeyState::KEY_JUST_DOWN;
}

int InputManager::getMouseMovementX() {
	return _mouseMovement.x;
}
int InputManager::getMouseMovementY() {
	return _mouseMovement.y;
}
XYIntSystem InputManager::getMouseMovement() {
	return _mouseMovement;
}
bool InputManager::shouldQuit() {
	return _quit;
}