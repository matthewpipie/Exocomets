#include "GameScene.h"
#include "SaveDataManager.h"
#include "RandomNumberManager.h"
#include "MainGame.h"
#include "SettingsManager.h"
#include "InputManager.h"
#include "SDLTextCreator.h"
#include "StarPowerup.h"

GameScene::GameScene(std::shared_ptr<SDLRenderer> rnd) :
	GameScene(rnd, Difficulty::MAIN_MENU)
{
}

GameScene::GameScene(std::shared_ptr<SDLRenderer> rnd, Difficulty diff) :
	GameScene(rnd, diff, true)
{
}

GameScene::GameScene(std::shared_ptr<SDLRenderer> rnd, Difficulty diff, bool endWithoutPlayers) :
	Scene(rnd, diff)
{
	for (int i = 0; i < 4; i++) {
		playerScores[i] = 0;
	}
	spriteBatches.push_back(std::make_shared<SpriteBatch>());
	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>(std::to_string(_score), XYSystem(0, 0), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, true));
	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::COMET_PATH_PREFIX + Constants::COMET_PATH + Constants::COMET_PATH_SUFFIX, XYSystem(960, 1080 - Constants::REGULAR_HEIGHT * 2.0), 64, Direction(), false, false, Constants::TIMER_INCOMPLETE, true, 0));
	//spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>("", XYSystem(0, 0), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, false));
	//spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>("", XYSystem(0, 0), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, true));
	//spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>("", XYSystem(0, 0), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, true));
	//spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>("", XYSystem(0, 0), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, true));
}

GameScene::~GameScene()
{
}

void GameScene::init() {
	//std::cout << "d=" << (int)difficulty << std::endl;
	_score = 0;

	for (int i = 0; i < SettingsManager::getSettings().playerCount; i++) {
		entityBatches[0]->entities.push_back(std::make_shared<Player>(i, camera->getCameraCenter(), difficulty));
	}
	//camera->followEntity(entityBatches[0]->entities[0]);
	for (int i = 0; i < Constants::COMET_COUNT_INIT[(int)difficulty]; i++) {
		addComet();
	}

	SaveData sd = SaveDataManager::getCurrentSaveData();
	_powerupsToSpawn[(int)PowerupType::STAR] = true; //sd.highScores[0] >= Constants::SCORE_LIMIT;
	_powerupsToSpawn[(int)PowerupType::SHIELD] = true; //sd.highScores[0] >= Constants::SCORE_LIMIT;
	_powerupsToSpawn[(int)PowerupType::BOMB] = sd.highScores[0] >= Constants::SCORE_LIMIT;
	_powerupsToSpawn[(int)PowerupType::TIMER] = sd.highScores[1] >= Constants::SCORE_LIMIT;
	_powerupsToSpawn[(int)PowerupType::GUN] = sd.highScores[2] >= Constants::SCORE_LIMIT;

	updateScoreSprites();
	SDLMusicManager::getInstance()->playMusicFromBeginningIfNotPlaying(Constants::MUSIC_NAMES[(int)difficulty]);

	camera = std::make_shared<Camera>();
	camera->setZoom(Constants::ZOOM_LEVELS[(int)difficulty]);

	_numOfFireworksCompleted = 0;
	_numOfFireworksTotal = -1;

}

void GameScene::customEveryTick() {
	//if (InputManager::getInstance()->isKeyDown(SDLK_x)) {
		//_score += 200;
	//}
	//if (InputManager::getInstance()->isKeyJustDown(SDLK_c)) {
		//entityBatches[0]->entities.push_back(std::make_shared<Powerup>(camera, PowerupType::REPEL));
	//}
	entityBatches[0]->nonEssentialSpeedMod = XYSystem(.7 + .3 * std::log((((double)_score / Constants::TICK_RATE * Constants::SCORE_MULTIPLIER) / (double)Constants::SCORE_LIMIT * 10.0) * .4 + 1));
	//entityBatches[0]->nonEssentialSpeedMod = XYSystem(-2.4 + .0003 * std::pow(((double)_score / Constants::TICK_RATE * Constants::SCORE_MULTIPLIER) / (double)Constants::SCORE_LIMIT * 10.0 + 100, 2.0));
	//if (entityBatches[0]->nonEssentialSpeedMod.x > 1.3) {
		//entityBatches[0]->nonEssentialSpeedMod = XYSystem(1.3);
	//}
	if (MainGame::getTickCount() % (int)(std::ceil(Constants::COMET_SPAWN_RATES[(int)difficulty] / entityBatches[0]->nonEssentialSpeedMod.x)) == 0) {

//		if (!entityBatches[0]->isTooFull())
			addComet();
		//std::cout << entityBatches[0]->entities.size() << "wow" <<std::endl;
	}
	if (Constants::POWERUP_RATES[(int)difficulty] != 0) {
		if (MainGame::getTickCount() % Constants::POWERUP_RATES[(int)difficulty] == 0) {
			tryToSpawn(PowerupType::STAR);
			tryToSpawn(PowerupType::STAR);
			tryToSpawn(PowerupType::SHIELD);
			tryToSpawn(PowerupType::BOMB);
			tryToSpawn(PowerupType::TIMER);
			tryToSpawn(PowerupType::GUN);
			if (difficulty > Difficulty::LEVEL_3) {
				tryToSpawn(PowerupType::STAR);
			}
		}
	}
	//std::cout << camera->getBottomLeft().x << " " << camera->getBottomLeft().y << " " << camera->getTopRight().x << " " << camera->getTopRight().y << std::endl;

	//if (InputManager::getInstance()->isKeyJustDown(SDLK_f)) {
		//camera->swapFollowEntity(entityBatches[0]->entities[0]);
	//}
	if (InputManager::getInstance()->isKeyJustDown(SDLK_ESCAPE)) {
		for (unsigned int i = 0; i < entityBatches[0]->entities.size(); i++) {
			if (entityBatches[0]->entities[i]->isEssentialEntity()) {
				entityBatches[0]->entities[i]->flagForDeath = true;
				entityBatches[0]->entities[i]->actionFlags = EntityActionFlags::ATTACH_DEATH_SCORE;
				eatKeys = false;
				//_score--;
			}
		}
		SDLMusicManager::getInstance()->playSound("click");
	}
	int a = 0;
	updateScoreSprites();
	if (_score / Constants::TICK_RATE * Constants::SCORE_MULTIPLIER >= Constants::SCORE_LIMIT * .903 && _score / Constants::TICK_RATE * Constants::SCORE_MULTIPLIER < Constants::SCORE_LIMIT * .993) {
		//SDLMusicManager::getInstance()->tempVolumeDown(.3);
		//SDLMusicManager::getInstance()->tempVolumeDown((Constants::SCORE_LIMIT - _score / Constants::TICK_RATE * Constants::SCORE_MULTIPLIER) * 0.0006 + 0.2);
		if ((_score + 15) % (int)Constants::TICK_RATE == 0) {
			SDLMusicManager::getInstance()->playSound("heartbeat");
		}
		if (_score % (int)Constants::TICK_RATE == 0) {
			camera->setZoom((Constants::SCORE_LIMIT - _score / Constants::TICK_RATE * Constants::SCORE_MULTIPLIER) * -0.0000175 + 1.03);
			SDLMusicManager::getInstance()->tempVolumeDown((Constants::SCORE_LIMIT - _score / Constants::TICK_RATE * Constants::SCORE_MULTIPLIER) * 0.0006 + 0.2);
		}
		if ((_score - 15) % (int)Constants::TICK_RATE == 0) {
			//camera->setZoom(1.0 / 1.1);
		}
	}
	if (_score == 6000) {
		camera = std::make_shared<Camera>();
		camera->setZoom(Constants::ZOOM_LEVELS[(int)difficulty]);
		SDLMusicManager::getInstance()->endTempVolumeDown();
		_nextFirework = generateFirework(true);
	}
	if (_score == _nextFirework) {
		_nextFirework = generateFirework(false);
	}
	//if (_score / Constants::TICK_RATE * Constants::SCORE_MULTIPLIER < Constants::SCORE_LIMIT * .9 || _score / Constants::TICK_RATE * Constants::SCORE_MULTIPLIER > Constants::SCORE_LIMIT) {
		//SDLMusicManager::getInstance()->endTempVolumeDown();
	//}

	//if (InputManager::getInstance()->isKeyJustDown(SDLK_DOWN)) {
		//camera->setZoom(.5);
	//}
	//if (InputManager::getInstance()->isKeyJustDown(SDLK_UP)) {
		//camera->setZoom(2);
	//}




}

void GameScene::deinit() {
	entityBatches[0]->entities.clear();
	updateScoreSprites();
}

bool GameScene::doBackground()
{
	return false;
}

void GameScene::addComet() {
	std::shared_ptr<Comet> comet = nullptr;
	double radius;
	double mass;
	XYSystem mod;
	for (int j = 0; j < Constants::COMET_SPAWN_ATTEMPTS_PER_TICK; j++) {
		radius = Constants::COMET_RADIUS;
		mass = Constants::COMET_MASS;
		mod = XYSystem(1);
		if (_score / Constants::TICK_RATE * Constants::SCORE_MULTIPLIER >= Constants::SCORE_LIMIT / 2.0) {
			if (RandomNumberManager::getInstance()->generateRandomDouble(0, 1000, RandomType::isGiantComet) <= Constants::COMET_GIANT_CHANCE) {
				radius = Constants::COMET_RADIUS_GIANT;
				mass = Constants::COMET_MASS_GIANT;
				mod = XYSystem(Constants::COMET_GIANT_SPEED_MP);
			}
		}
		comet = std::make_shared<Comet>(camera, difficulty, radius, mass, mod);
		for (unsigned int i = 0; i < entityBatches[0]->entities.size() - 1; i++) {
			if (entityBatches[0]->entities[i]->isCurrentlyColliding(*comet)) {
				comet = nullptr;
				break;
			}
		}
		if (comet != nullptr) {
			entityBatches[0]->entities.push_back(comet);
			return;
		}
	}
}

void GameScene::updateScoreSprites() {

	


	// Score
	std::shared_ptr<Sprite> spr = spriteBatches[0]->sprites[0];
	spr->updateText(std::to_string((int)(_score++ / Constants::TICK_RATE * Constants::SCORE_MULTIPLIER)), Constants::REGULAR_HEIGHT);
	spr->relativeLocation.x = Constants::SCREEN_SIZE_LOGICAL.makeDouble().x / 2.0;
	spr->relativeLocation.y = Constants::SCREEN_SIZE_LOGICAL.makeDouble().y - spr->size.y / 2.0;

	// Timer circle
	spr = spriteBatches[0]->sprites[1];
	double a = ((double)_score / Constants::TICK_RATE * Constants::SCORE_MULTIPLIER) / (double)Constants::SCORE_LIMIT;
	if (a < 1) {
		spr->colorModD = Direction(a * 2.0 * M_PI);
		for (int i = 0; i < 4; i++) {
			spr->RGBAColorMod[i] = Constants::TIMER_INCOMPLETE[i];
		}
	}
	else {
		for (int i = 0; i < 3; i++) {
			spr->RGBAColorMod[i] = Constants::TIMER_COMPLETE[i];
		}
		spr->colorModD = Direction::makeFromDegs(360);
		Uint8 &AColorMod = spr->RGBAColorMod[3];
		spr->everyRender = [&AColorMod]() {if (AColorMod > 0) AColorMod -= 1; };
	}
	spr->everyRender();


	/*
	spriteBatches[0]->sprites[1]->RGBAColorMod[3] = 0;
	spriteBatches[0]->sprites[2]->RGBAColorMod[3] = 0;
	spriteBatches[0]->sprites[3]->RGBAColorMod[3] = 0;
	spriteBatches[0]->sprites[4]->RGBAColorMod[3] = 0;
	for (auto entity : entityBatches[0]->entities) {
		if (entity->isEssentialEntity()) {
			if (auto c = std::dynamic_pointer_cast<Player>(entity)) {
				std::shared_ptr<Sprite> spr;
				switch (c->getPlayerNumber()) {
				case 0:
					spr = spriteBatches[0]->sprites[1];
					spr->updateText(std::to_string((int)std::ceil((c->starCount))), Constants::REGULAR_HEIGHT / 2.0);
					spr->relativeLocation = XYSystem(0, 0);
					spr->relativeLocation.x += spr->size.x / 2.0;
					spr->relativeLocation.y += spr->size.y / 4.0;
					spr->RGBAColorMod[3] = 255;
					//spr->relativeLocation = XYSystem(c->potentialLocation.x, c->potentialLocation.y + Constants::PLAYER_RADIUS * 3);
					break;
				case 1:
					spr = spriteBatches[0]->sprites[2];
					spr->updateText(std::to_string((int)std::ceil((c->starCount))), Constants::REGULAR_HEIGHT);
					spr->relativeLocation = XYSystem(0, Constants::SCREEN_SIZE_LOGICAL.makeDouble().y);
					spr->relativeLocation.x += spr->size.x / 2.0;
					spr->relativeLocation.y -= spr->size.y / 4.0;
					spr->RGBAColorMod[3] = 255;
					break;
				case 2:
					spr = spriteBatches[0]->sprites[3];
					spr->updateText(std::to_string((int)std::ceil((c->starCount))), Constants::REGULAR_HEIGHT);
					spr->relativeLocation = Constants::SCREEN_SIZE_LOGICAL.makeDouble();
					spr->relativeLocation.x -= spr->size.x / 2.0;
					spr->relativeLocation.y -= spr->size.y / 4.0;
					spr->RGBAColorMod[3] = 255;
					break;
				case 3:
					spr = spriteBatches[0]->sprites[4];
					spr->updateText(std::to_string((int)std::ceil((c->starCount))), Constants::REGULAR_HEIGHT);
					spr->relativeLocation = XYSystem(Constants::SCREEN_SIZE_LOGICAL.makeDouble().x, 0);
					spr->relativeLocation.x -= spr->size.x / 2.0;
					spr->relativeLocation.y += spr->size.y / 4.0;
					spr->RGBAColorMod[3] = 255;
					break;
				}
			}
		}
	}*/
}

void GameScene::tryToSpawn(PowerupType powerup)
{
	if (_powerupsToSpawn[(int)powerup]) {
		if (powerup == PowerupType::STAR) {
			entityBatches[0]->entities.push_back(std::make_shared<StarPowerup>(camera, difficulty));
		}
		else {
			entityBatches[0]->entities.push_back(std::make_shared<Powerup>(camera, powerup, difficulty));
		}
	}
}

int GameScene::generateFirework(bool first)
{
	if (first) {
		_numOfFireworksTotal = RandomNumberManager::getInstance()->generateRandomInt(2, 5, RandomType::fireworkCount);
	}

	if (_numOfFireworksCompleted == _numOfFireworksTotal || _numOfFireworksTotal == -1) return -1;

	SDLMusicManager::getInstance()->playSound(Constants::SOUND_EFFECT_NAMES[RandomNumberManager::getInstance()->generateRandomInt(3, 6, RandomType::fireworkType)]);


	return ++_numOfFireworksCompleted == _numOfFireworksTotal ? -1 : RandomNumberManager::getInstance()->generateRandomInt(15, 120, RandomType::fireworkTime) + _score;

}

bool GameScene::killOffscreen() {
	return false;//true;
}
