#pragma once
#include <string>

enum class ErrorCode {
	SDL_INIT = 1,
	SDL_WINDOW,
	SDL_RENDERER,
	SDL_MUSIC,
	SDL_MUSIC_MANAGER_MUSIC,
	SDL_MUSIC_MANAGER_SOUND,
	SDL_SOUND_EFFECT,
	SDL_TEXTURE,
	TTF_INIT,
	TTF_FONT,
	MUS_INIT,
	BMP_INIT,
	BAD_FILE_PATH,
	DIRECTION_PTS_EQUAL,
	DUMMY_CIRCLE_COLLISION,
	ENTITY_HITBOX_TOO_SMALL,
	NO_POWERUP_UNLOCKED,
	SETTINGS_CANT_CREATE,
	SAVE_DATA_CANT_CREATE,
	STEAM
};

class ErrorHandler
{
public:
	static void error(ErrorCode e);
	static void error(ErrorCode e, std::string extraInfo);

};