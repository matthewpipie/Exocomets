#pragma once
#include "SDLManager.h"
#include "Scene.h"
#include "BackgroundScene.h"
class MainGame
{
public:
	MainGame();
	~MainGame();
	void run();
	static int getTickCount();

private:
	static int _tick;
	SDLManager _sdlManager;
	std::vector<std::shared_ptr<Scene>> _sceneStack; //since you cant access second-to-last thing in a stack i have to use a vector
	std::shared_ptr<BackgroundScene> _backgroundScene;
	void gameLoop();
	void everyTick();
	void render();
	bool _shouldQuit = false;
};

