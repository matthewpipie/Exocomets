#include "SDLRenderer.h"
#include "SettingsManager.h"
#include "SDLTextureManager.h"
#include "MainGame.h"

SDLRenderer::SDLRenderer(SDL_Renderer* renderer) :
	_renderer(renderer)
{
}


SDLRenderer::~SDLRenderer()
{
}

void SDLRenderer::renderSprite(std::shared_ptr<Camera> camera, XYSystem centralLoc, XYSystem size, Direction rotation, bool horizontalFlip, bool verticalFlip, std::string filepath, bool renderFilepathNameInsteadOfFile, Uint8 RGBAColorMod[4], Direction colorModD, bool isHudElement, int zindex) {
	//TODO: implement zindex
	SDL_Rect renderRect;
	renderRect.w = (int)std::round(size.x * (!isHudElement ? ((double)Constants::SCREEN_SIZE_LOGICAL.x / camera->getSize().x) : 1));
	renderRect.h = (int)std::round(size.y * (!isHudElement ? ((double)Constants::SCREEN_SIZE_LOGICAL.y / camera->getSize().y) : 1));
	renderRect.x = (int)std::round((!isHudElement ? ((centralLoc.x - size.x / 2.0 - camera->getBottomLeft().x) / camera->getSize().x * (double)Constants::SCREEN_SIZE_LOGICAL.x) : (centralLoc.x - size.x / 2.0)));
	renderRect.y = (int)std::round((!isHudElement ? ((1.0 - ((centralLoc.y + size.y / 2.0 - camera->getBottomLeft().y) / camera->getSize().y)) * (double)Constants::SCREEN_SIZE_LOGICAL.y) : ((double)Constants::SCREEN_SIZE_LOGICAL.y - centralLoc.y - size.y / 2.0)));

	SDL_Texture* texture = SDLTextureManager::getTexture(filepath, renderFilepathNameInsteadOfFile, _renderer);

	const SDL_RendererFlip flip = (SDL_RendererFlip)((horizontalFlip ? SDL_FLIP_HORIZONTAL : SDL_FLIP_NONE) | (verticalFlip ? SDL_FLIP_VERTICAL : SDL_FLIP_NONE));

	if (colorModD.degrees() < 0.0001) {
		SDL_SetTextureColorMod(texture, RGBAColorMod[0], RGBAColorMod[1], RGBAColorMod[2]);
		SDL_SetTextureAlphaMod(texture, RGBAColorMod[3]);
		SDL_RenderCopyEx(_renderer, texture, NULL, &renderRect, -rotation.degrees(), NULL, flip);
	}
	else {
		
		SDL_Texture *finalTexture = SDL_CreateTexture(_renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, (int)std::round(size.x), (int)std::round(size.y));
		SDL_Texture *tempTexture = SDL_CreateTexture(_renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, (int)std::round(size.x), (int)std::round(size.y));
		SDL_SetRenderTarget(_renderer, tempTexture);
		SDL_RenderClear(_renderer);
		SDL_RenderCopy(_renderer, texture, NULL, NULL);
		SDL_SetRenderTarget(_renderer, finalTexture);
		SDL_SetRenderDrawColor(_renderer, 0, 0, 0, 255);
		SDL_RenderClear(_renderer);
		XYSystem copyRect;
		XYSystem copyRectSize;

		//double tanAngle = std::tan(colorModD.radians);
		if (colorModD.degrees() > 180) {
			copyRect.x = size.x / 2.0;
			copyRect.y = 0;
			copyRectSize.x = size.x / 2.0;
			copyRectSize.y = size.y;
			SDL_Rect sdlCopyRect;
			sdlCopyRect.x = (int)std::round(copyRect.x);
			sdlCopyRect.y = (int)std::round(copyRect.y);
			sdlCopyRect.w = (int)std::round(copyRectSize.x);
			sdlCopyRect.h = (int)std::round(copyRectSize.y);
			SDL_RenderCopy(_renderer, tempTexture, &sdlCopyRect, &sdlCopyRect);

			copyRectSize.x = 1.0 / (double)Constants::ANGLE_STEPS * size.x / 2.0;

			for (int i = 0; i < Constants::ANGLE_STEPS; i++) {
				copyRect.x = (double)(-i) / (double)Constants::ANGLE_STEPS * size.x / 2.0 + size.x / 2.0 - copyRectSize.x;
				copyRectSize.y = size.y / 2.0 - std::tan(M_PI / 2.0 + colorModD.radians) * (copyRect.x + copyRectSize.x / 2.0 - size.x / 2.0);
				copyRect.y = size.y - copyRectSize.y;
				if (copyRectSize.y <= 0) continue;
				if (copyRect.y < 0) {
					copyRect.y = 0;
					copyRectSize.y = size.y;
				}
				SDL_Rect sdlCopyRect;
				sdlCopyRect.x = (int)std::round(copyRect.x);
				sdlCopyRect.y = (int)std::round(copyRect.y);
				sdlCopyRect.w = (int)std::round(copyRectSize.x);
				sdlCopyRect.h = (int)std::round(copyRectSize.y);
				SDL_RenderCopy(_renderer, tempTexture, &sdlCopyRect, &sdlCopyRect); // copies to finalTexture
			}
		}
		else {
			copyRect.y = 0; //upper left
			copyRectSize.x = 1.0 / (double)Constants::ANGLE_STEPS * size.x / 2.0;
			for (int i = 0; i < Constants::ANGLE_STEPS; i++) {
				copyRect.x = (double)i / (double)Constants::ANGLE_STEPS * size.x / 2.0 + size.x / 2.0;
				copyRectSize.y = size.y / 2.0 - std::tan(M_PI / 2.0 - colorModD.radians) * (copyRect.x + copyRectSize.x / 2.0 - size.x / 2.0);
				if (copyRectSize.y <= 0) continue;
				if (copyRect.y + copyRectSize.y > size.y) copyRectSize.y = size.y - copyRect.y;
				//double a = (size.x - (copyRect.x + copyRectSize.x / 2.0));
				//copyRectSize.y = std::sqrt(std::pow(a / std::sin(colorModD.radians), 2.0) - std::pow(a, 2.0));
				//std::cout << copyRect.x << std::endl;
				//std::cout << copyRect.y << std::endl;
				//std::cout << copyRectSize.x << std::endl;
				//std::cout << copyRectSize.y << std::endl;
				SDL_Rect sdlCopyRect;
				sdlCopyRect.x = (int)std::round(copyRect.x);
				sdlCopyRect.y = (int)std::round(copyRect.y);
				sdlCopyRect.w = (int)std::round(copyRectSize.x);
				sdlCopyRect.h = (int)std::round(copyRectSize.y);
				SDL_RenderCopy(_renderer, tempTexture, &sdlCopyRect, &sdlCopyRect); // copies to finalTexture
			}
		}


		SDL_SetRenderTarget(_renderer, NULL);
		SDL_SetRenderDrawColor(_renderer, SettingsManager::getSettings().backgroundColor[0], SettingsManager::getSettings().backgroundColor[1], SettingsManager::getSettings().backgroundColor[2], SettingsManager::getSettings().backgroundColor[3]);
		SDL_SetTextureColorMod(finalTexture, RGBAColorMod[0], RGBAColorMod[1], RGBAColorMod[2]);
		SDL_SetTextureAlphaMod(finalTexture, RGBAColorMod[3]);
		SDL_RenderCopyEx(_renderer, finalTexture, NULL, &renderRect, -rotation.degrees(), NULL, flip);
		SDL_DestroyTexture(finalTexture);
		SDL_DestroyTexture(tempTexture);
	}
	/*const char* e;
	if (strcmp((e = SDL_GetError()),"")) {
		std::cout << "error: " << e << std::endl;
		int asdf = 0;
	}*/

}
void SDLRenderer::renderEntity(Uint8 extraColorMod[4], std::shared_ptr<Camera> camera, std::shared_ptr<Entity> entity) {
	renderSpriteBatch(extraColorMod, camera, entity->spriteBatch, entity->location);
}
void SDLRenderer::renderSpriteBatch(Uint8 extraColorMod[4], std::shared_ptr<Camera> camera, std::shared_ptr<SpriteBatch> spriteBatch) {
	renderSpriteBatch(extraColorMod, camera, spriteBatch, XYSystem(0, 0));
}
void SDLRenderer::renderSpriteBatch(Uint8 extraColorMod[4], std::shared_ptr<Camera> camera, std::shared_ptr<SpriteBatch> spriteBatch, XYSystem extraLocation) {
	double colorMP[4] = { (double)Constants::FULL_COLOR[0], (double)Constants::FULL_COLOR[1], (double)Constants::FULL_COLOR[2], (double)Constants::FULL_COLOR[3] };
	int size = sizeof(colorMP) / sizeof(colorMP[0]);

	for (int i = 0; i < size; i++) {
		colorMP[i] = spriteBatch->RGBAColorMod[i] * extraColorMod[i] / (colorMP[i] * colorMP[i]);
	}
	Uint8 realColorMod[4];
	for (auto it : spriteBatch->sprites) {
		for (int i = 0; i < size; i++) {
			realColorMod[i] = (Uint8)((double)it->RGBAColorMod[i] * colorMP[i]);
		}
		if ((!it->renderFilepathNameInsteadOfFile || it->filepathOrText != "") && realColorMod[3] != 0) {
			renderSprite(camera, it->relativeLocation + extraLocation + spriteBatch->location, it->size.multiplyComponents(it->sizeMod), it->rotation.rotateLeft(spriteBatch->rotation), it->horizontalFlip, it->verticalFlip, it->filepathOrText, it->renderFilepathNameInsteadOfFile, realColorMod, it->colorModD, it->isHudElement, spriteBatch->zindex);
		}
	}
}
