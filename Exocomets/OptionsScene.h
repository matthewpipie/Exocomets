#pragma once
#include "Scene.h"
class OptionsScene :
	public Scene
{
public:
	OptionsScene(std::shared_ptr<SDLRenderer> rnd);
	~OptionsScene();
	void init() override;
	void deinit() override;
	bool doBackground() override;

protected:
	void customEveryTick() override;
private:
	void loadSettingsToSliderValues();
	void loadSliderValuesToSettings();
	std::vector<std::shared_ptr<KeyOptionEntity>> keyOptionEntities;

};
