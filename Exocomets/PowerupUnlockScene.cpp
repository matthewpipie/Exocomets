#include "PowerupUnlockScene.h"
#include "SettingsManager.h"
#include "InputManager.h"
#include "SaveDataManager.h"
#include "SDLTextCreator.h"
#include "SDLMusicManager.h"
#include "Player.h"
#include "Powerup.h"
#include "ErrorHandler.h"

PowerupUnlockScene::PowerupUnlockScene(std::shared_ptr<SDLRenderer> renderer) : 
	Scene(renderer, Difficulty::MAIN_MENU),
	_unlockedPowerup(getUnlockedPowerup())
{
	entityBatches.push_back(std::make_shared<EntityBatch>());
	entityBatches.push_back(std::make_shared<EntityBatch>());
	spriteBatches.push_back(std::make_shared<SpriteBatch>());

	entityBatches[1]->entities.push_back(std::make_shared<RectangleEntity>(XYSystem(960, 70), SDLTextCreator::getTextSize("Continue", Constants::REGULAR_HEIGHT).getSizeVector(), 0, 0));
	entityBatches[1]->entities[0]->spriteBatch->sprites.push_back(std::make_shared<Sprite>("Continue", XYSystem(), Constants::REGULAR_HEIGHT, Direction(), false, false, Constants::FULL_COLOR, true));

	entityBatches[2]->entities.push_back(std::make_shared<RectangleEntity>(XYSystem(192, 70), Constants::ARROW_SIZE.getSizeVector(), 0, 0));
	entityBatches[2]->entities[0]->spriteBatch->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::ARROW_PATH_PREFIX + Constants::ARROW_PATH + Constants::ARROW_PATH_SUFFIX, XYSystem(), Constants::ARROW_SIZE, Direction(), false, false, Constants::FULL_COLOR, true));

	entityBatches[2]->entities.push_back(std::make_shared<RectangleEntity>(XYSystem(1728, 70), Constants::ARROW_SIZE.getSizeVector(), 0, 0));
	entityBatches[2]->entities[1]->spriteBatch->sprites.push_back(std::make_shared<Sprite>(Constants::PATH_BASE_LOCATION + Constants::ARROW_PATH_PREFIX + Constants::ARROW_PATH + Constants::ARROW_PATH_SUFFIX, XYSystem(), Constants::ARROW_SIZE, Direction(), true, false, Constants::FULL_COLOR, true));

	displayDesc(_unlockedPowerup);

}


PowerupUnlockScene::~PowerupUnlockScene()
{
}

void PowerupUnlockScene::init()
{
	SDLMusicManager::getInstance()->pauseMusic();
}

bool PowerupUnlockScene::doBackground()
{
	return false;
}

void PowerupUnlockScene::customEveryTick()
{
	if (entityBatches[0]->entities[0]->isPotentiallyColliding(*entityBatches[1]->entities[0])) {
		if (SettingsManager::getSettings().controls[0][0] != -1 || InputManager::getInstance()->isMouseDown()) {
			action = SceneAction::DESTROY_SCENE;
		}
	}
	if (InputManager::getInstance()->isKeyJustDown(SDLK_ESCAPE)) {
		action = SceneAction::DESTROY_SCENE;
		eatKeys = false;
	}
	if (entityBatches[0]->entities[0]->isPotentiallyColliding(*entityBatches[2]->entities[1])) {
		if (SettingsManager::getSettings().controls[0][0] != -1 || InputManager::getInstance()->isMouseDown()) {
			if (_unlockedPowerup != getUnlockedPowerup()) {
				displayDesc((PowerupType)((int)_unlockedPowerup + 1));
			}
		}
	}
	if (entityBatches[0]->entities[0]->isPotentiallyColliding(*entityBatches[2]->entities[0])) {
		if (SettingsManager::getSettings().controls[0][0] != -1 || InputManager::getInstance()->isMouseDown()) {
			if (_unlockedPowerup != (PowerupType)(0)) {
				displayDesc((PowerupType)((int)_unlockedPowerup - 1));
			}
		}
	}
}

void PowerupUnlockScene::displayDesc(PowerupType pType)
{
	_unlockedPowerup = pType;

	entityBatches[0]->entities.clear();
	entityBatches[0]->entities.push_back(std::make_shared<Player>(0, 300, camera->getCameraCenter(), difficulty));
	entityBatches[0]->entities.push_back(std::make_shared<Powerup>(XYSystem(960, 700), XYSystem(), _unlockedPowerup));
	spriteBatches[0]->sprites.clear();

	std::vector<std::string> desc(getDesc());

	for (unsigned int i = 0; i < desc.size(); i++) {
		spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>(desc[i], XYSystem(960, 320 - 50 * i), Constants::REGULAR_HEIGHT * 1.25, Direction(), false, false, Constants::FULL_COLOR, false));
	}
	
	spriteBatches[0]->sprites.push_back(std::make_shared<Sprite>("Congratulations!  You unlocked a new powerup.", XYSystem(960, 810), Constants::REGULAR_HEIGHT * 1.5, Direction(), false, false, Constants::FULL_COLOR, false));

	if (_unlockedPowerup == getUnlockedPowerup()) {
		for (int i = 0; i < 4; i++) {
			entityBatches[2]->entities[1]->spriteBatch->RGBAColorMod[i] = Constants::DIM_COLOR_MENU[i];
		}
	}
	else {
		for (int i = 0; i < 4; i++) {
			entityBatches[2]->entities[1]->spriteBatch->RGBAColorMod[i] = Constants::FULL_COLOR[i];
		}
	}
	if (_unlockedPowerup == (PowerupType)(0)) {
		for (int i = 0; i < 4; i++) {
			entityBatches[2]->entities[0]->spriteBatch->RGBAColorMod[i] = Constants::DIM_COLOR_MENU[i];
		}
	}
	else {
		for (int i = 0; i < 4; i++) {
			entityBatches[2]->entities[0]->spriteBatch->RGBAColorMod[i] = Constants::FULL_COLOR[i];
		}
	}
}

PowerupType PowerupUnlockScene::getUnlockedPowerup()
{
	SaveData sd = SaveDataManager::getCurrentSaveData();
	if (sd.highScores[2] >= Constants::SCORE_LIMIT) return PowerupType::GUN;
	if (sd.highScores[1] >= Constants::SCORE_LIMIT) return PowerupType::TIMER;
	if (sd.highScores[0] >= Constants::SCORE_LIMIT) return PowerupType::BOMB;
	//if (sd.highScores[0] >= Constants::SCORE_LIMIT) return PowerupType::SHIELD;
	//ErrorHandler::error(ErrorCode::NO_POWERUP_UNLOCKED);
	return PowerupType::SHIELD;
}

std::vector<std::string> PowerupUnlockScene::getDesc()
{
	std::vector<std::string> desc;
	switch (_unlockedPowerup) {
	case PowerupType::STAR:
		desc.push_back("The star is very simple: it gives you 100 star power on pickup.");
		desc.push_back("If another player has died, collecting enough star power in the same game can yeild strange surprises...");
		break;
	case PowerupType::SHIELD:
		desc.push_back("The shield is a one-time extra life that slowly consumes star power.");
		desc.push_back("If it gets hit, it will destroy the comet that hit it but it will also go away.");
		break;
	case PowerupType::BOMB:
		desc.push_back("The bomb creates an explosion whose size is based on the amount of star power you have.");
		desc.push_back("However, it instantly uses all of your star power.");
		desc.push_back("You've unlocked Level 2!");
		break;
	case PowerupType::TIMER:
		desc.push_back("The timer allows you to slow down time for anything in the circle created.");
		desc.push_back("The circle's size, as with the bomb, depends on the amount of star power you have.");
		desc.push_back("If things get too crowded, you can cancel the powerup instantly with a bomb.");
		desc.push_back("You've unlocked Level 3!");
		break;
	case PowerupType::GUN:
		desc.push_back("The gun is a powerup that allows you to destroy comets around you, including giant comets.");
		desc.push_back("It uses star power very quickly, however.");
		desc.push_back("You've unlocked Level 4!");
		desc.push_back("");
		desc.push_back("You've unlocked all of the powerups!  Good luck beating levels 4, 5, and 6!");
		break;
	}
	return desc;

}

