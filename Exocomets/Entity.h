#pragma once
#include "stdafx.h"
#include "SpriteBatch.h"
enum class EntityType {
	CIRCLE,
	POLYGON
};
enum class EntityActionFlags {
	NOTHING = 1 << 0,
	CREATE_EXPLOSION = 1 << 1,
	CREATE_SLOWMO = 1 << 2,
	REVIVE_PLAYER = 1 << 3,
	PLAYER_REVIVED_SUCCESSFULLY = 1 << 4,
	CREATE_BULLETS = 1 << 5,
	ATTACH_DEATH_SCORE = 1 << 6
	//CREATE_REPEL = 1 << 7
};
inline EntityActionFlags operator|(EntityActionFlags a, EntityActionFlags b)
{
	return static_cast<EntityActionFlags>(static_cast<int>(a) | static_cast<int>(b));
}
inline EntityActionFlags operator&(EntityActionFlags a, EntityActionFlags b)
{
	return static_cast<EntityActionFlags>(static_cast<int>(a) & static_cast<int>(b));
}
class Entity
{
public:
	//friend class CollidableEntity;
	//friend class MovableEntity;
	// There are lots of ways to make an entity!
	// Assume invisible=false, location=0,0, radius=0, type=circle
	/*Entity();
	Entity(bool invisible);
	Entity(XYSystem location);
	Entity(double radius);
	Entity(std::vector<XYSystem> relativeHitbox);
	Entity(bool invisible, XYSystem location);
	Entity(XYSystem location, double radius);
	Entity(XYSystem location, std::vector<XYSystem> relativeHitbox);*/
	Entity(const XYSystem location, const double radius, const double mass, const XYSystem speed);
	Entity(const XYSystem location, const std::vector<XYSystem> relativeHitbox, const double mass, const XYSystem speed);

	~Entity();
	/*void move(XYSystem movement);
	void stride();
	void setLocation(XYSystem location);
	void setSpeed(XYSystem speed);*/
	// Although nothing should have a general collision with a general entity, i need something pure virtual in this object
	// Empty

	// Called every frame
	// First calculate potential for collisions
	void calculatePotentialLocation();
	// Then check for collisions in EntityManager
	// Then move
	void completePotentialLocation();
	void failPotentialLocation();
	virtual bool isEssentialEntity();
	bool isFlaggedForDeath();
	const EntityType type;
	// Sprites
	std::shared_ptr<SpriteBatch> spriteBatch;
	bool flagForDeath = false;
	// Central location
	XYSystem location;
	XYSystem potentialLocation;
	XYSystem speed;
	XYSystem speedMod;
	double radius;
	double mass;
	// Only does anything if polygonal
	const std::vector<XYSystem> relativeHitbox;
	
	EntityActionFlags actionFlags;
	virtual void clearAction();

	const int createdTick;
protected:
	// Only if things don't move at a constant speed (ie players)
	virtual void calculateCustomPotentialLocation();
};
