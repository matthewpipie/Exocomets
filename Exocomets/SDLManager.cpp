#include "SDLManager.h"
#include "SettingsManager.h"
#include "SDLTextCreator.h"
#include "SDLTextureManager.h"
#include "SDLMusicManager.h"
#include "ErrorHandler.h"

SDLManager::SDLManager() :
	customRenderer(nullptr),
	_window(nullptr),
	_renderer(nullptr),
	_fullscreenMode(WindowMode::FULLSCREEN_BORDERLESS)
{
}


SDLManager::~SDLManager()
{
}


// Starts all SDL functions
void SDLManager::initAll()
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		ErrorHandler::error(ErrorCode::SDL_INIT);
	}

	//std::cout << "Finished SDL_Init" << std::endl;
	//SDL_GL_SetSwapInterval(SettingsManager::getSettings().enableVSync);

	SDL_SetHint(SDL_HINT_VIDEO_MINIMIZE_ON_FOCUS_LOSS, "0");


	// Make the window
	std::string buf;
	buf.append(Constants::GAME_NAME);
	buf.append(" v");
	buf.append(Constants::GAME_VERSION);
	_window = SDL_CreateWindow(buf.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SettingsManager::getSettings().windowSize[0], SettingsManager::getSettings().windowSize[1], SDL_WINDOW_SHOWN | SDL_WINDOW_INPUT_GRABBED);
	if (_window == nullptr) {
		ErrorHandler::error(ErrorCode::SDL_WINDOW);
	}
	SDL_ShowCursor(0);
	SDL_SetRelativeMouseMode(SDL_TRUE);
	SDL_SetWindowGrab(_window, SDL_TRUE);

	//std::cout << "Finished SDL_Window" << std::endl;
	
	// Make the renderer
	_renderer = SDL_CreateRenderer(_window, -1, SettingsManager::getSettings().enableVSync ? SDL_RENDERER_PRESENTVSYNC : 0);
	if (_renderer == nullptr) {
		SDL_DestroyWindow(_window);
		ErrorHandler::error(ErrorCode::SDL_RENDERER);
	}
	setRenderDrawColor(SettingsManager::getSettings().backgroundColor);
	SDL_RenderSetLogicalSize(_renderer, Constants::SCREEN_SIZE_LOGICAL.x, Constants::SCREEN_SIZE_LOGICAL.y);

	SDL_SetWindowFullscreen(_window, (Uint32)SettingsManager::getSettings().windowMode);

	std::cout << "Loading audio..." << std::endl;
	SDLMusicManager::getInstance()->init();
	SDLMusicManager::getInstance()->setMusicVolume(SettingsManager::getSettings().musicVolume);
	SDLMusicManager::getInstance()->setSoundVolume(SettingsManager::getSettings().soundVolume);
	std::cout << "Loading fonts..." << std::endl;
	SDLTextCreator::init();
	std::cout << "Loading textures..." << std::endl;
	SDLTextureManager::init();
	customRenderer = std::make_shared<SDLRenderer>(_renderer);
}

void SDLManager::deinitAll() {
	//std::cout << "unloaderino 1" << std::endl;
	SDLMusicManager::getInstance()->unload();
	//std::cout << "unloaderino 2" << std::endl;
	SDLTextCreator::unload();
	//std::cout << "unloaderino 3" << std::endl;
	SDLTextureManager::unload();
	//std::cout << "unloaderino 4" << std::endl;
	SDL_DestroyRenderer(_renderer);
	//std::cout << "unloaderino 5" << std::endl;
	_renderer = nullptr;
	//std::cout << "unloaderino 6" << std::endl;
	SDL_DestroyWindow(_window);
	//std::cout << "unloaderino 7" << std::endl;
	_window = nullptr;
	//std::cout << "unloaderino 8" << std::endl;
	SDL_Quit();
	//std::cout << "unloaderino 9" << std::endl;
}

void SDLManager::setRenderDrawColor(int color[4]) {
	SDL_SetRenderDrawColor(_renderer, color[0], color[1], color[2], color[3]);
}

double SDLManager::getSDLTicks() {
	return SDL_GetTicks();
}

void SDLManager::beginRender() {
	SDL_RenderClear(_renderer);
}

void SDLManager::finishRender() {
	SDL_RenderPresent(_renderer);
}
