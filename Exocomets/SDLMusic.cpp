#include "SDLMusic.h"
#include "ErrorHandler.h"



SDLMusic::SDLMusic(std::string name, std::string path, double bpm, double firstBeatOffset, std::vector<double> startTimes) :
	NAME(name),
	PATH(path),
	BPM(bpm),
	FIRST_BEAT_OFFSET(firstBeatOffset),
	START_TIMES(startTimes),
	sdlMixMusic(generateMixMusic(path))
{
	//std::cout << "I am a music! path=" << path << std::endl;
}


SDLMusic::~SDLMusic()
{
	if (sdlMixMusic != nullptr) {
		Mix_FreeMusic(sdlMixMusic);
	}
}

Mix_Music *SDLMusic::generateMixMusic(std::string path) {
	Mix_Music *temp = Mix_LoadMUS(path.c_str());
	if (temp == nullptr) {
		ErrorHandler::error(ErrorCode::SDL_MUSIC);
	}
	return temp;
}
