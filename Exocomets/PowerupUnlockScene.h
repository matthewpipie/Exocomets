#pragma once
#include "Scene.h"
class PowerupUnlockScene :
	public Scene
{
public:
	PowerupUnlockScene(std::shared_ptr<SDLRenderer> renderer);
	~PowerupUnlockScene();
	void init() override;
	bool doBackground() override;
protected:
	void customEveryTick() override;
private:
	void displayDesc(PowerupType pType);
	PowerupType _unlockedPowerup;
	PowerupType getUnlockedPowerup();
	std::vector<std::string> getDesc();
};
