#pragma once
#include "CollidableEntity.h"
class BulletEntity :
	public CollidableEntity
{
public:
	BulletEntity(XYSystem location, double rad);
	~BulletEntity();
	virtual void resolvePotentialCollision(CollidableEntity &other) override;
	virtual void resolvePotentialCollision(Player &other) override;
	virtual void resolvePotentialCollision(Comet &other) override;
private:
	const std::vector<XYSystem> generateHitbox(double rad);

};

