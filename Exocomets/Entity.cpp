#include "Entity.h"
#include "MainGame.h"
#include "ErrorHandler.h"

/*Entity::Entity() :
	Entity(false)
{	
}
Entity::Entity(bool invisible) :
	Entity(invisible, XYSystem())
{
}
Entity::Entity(XYSystem location) :
	Entity(false, location)
{
}
Entity::Entity(double radius) :
	Entity(XYSystem(), radius)
{
}
Entity::Entity(std::vector<XYSystem> relativeHitbox) :
	Entity(XYSystem(), relativeHitbox)
{
}
Entity::Entity(bool invisible, XYSystem location) :
	Entity(invisible, location, 0)
{
}
Entity::Entity(XYSystem location, double radius) :
	Entity(false, location, radius)
{
}
Entity::Entity(XYSystem location, std::vector<XYSystem> relativeHitbox) :
	Entity(false, location, relativeHitbox)
{
}*/
Entity::Entity(XYSystem location, double radius, double mass, XYSystem speed) :
	location(location),
	potentialLocation(location),
	radius(radius),
	type(EntityType::CIRCLE),
	mass(mass),
	speed(speed),
	spriteBatch(std::make_shared<SpriteBatch>()),
	createdTick(MainGame::getTickCount()),
	actionFlags(EntityActionFlags::NOTHING),
	speedMod(XYSystem(1))
{
}
Entity::Entity(XYSystem location, std::vector<XYSystem> relativeHitbox, double mass, XYSystem speed) :
	location(location),
	potentialLocation(location),
	radius(0),
	type(EntityType::POLYGON),
	relativeHitbox(relativeHitbox),
	mass(mass),
	speed(speed),
	spriteBatch(std::make_shared<SpriteBatch>()),
	createdTick(MainGame::getTickCount()),
	actionFlags(EntityActionFlags::NOTHING),
	speedMod(XYSystem(1))
{
	if (relativeHitbox.size() < 3) {
		ErrorHandler::error(ErrorCode::ENTITY_HITBOX_TOO_SMALL);
	}
}

Entity::~Entity()
{
}

/*void Entity::move(XYSystem movement) {
	setLocation(_location + movement);
}
void Entity::stride() {
	move(_speed);
}
void Entity::setLocation(XYSystem location) {
	_location = location;
}
void Entity::setSpeed(XYSystem speed) {
	_speed = speed;
}*/

void Entity::calculatePotentialLocation() {
	potentialLocation = location + speed.multiplyComponents(speedMod);
	calculateCustomPotentialLocation();
}

void Entity::completePotentialLocation() {
	location = potentialLocation;
}

void Entity::failPotentialLocation() {
	potentialLocation = location;
}

bool Entity::isEssentialEntity() {
	return false;
}


bool Entity::isFlaggedForDeath() {
	return flagForDeath;
}

void Entity::clearAction()
{
}

void Entity::calculateCustomPotentialLocation() {}
