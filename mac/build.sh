#!/bin/bash

read -p "Pressing enter will delete ../mac-output and ../../ExocometsMac"

killall Xcode
rm -rf ../mac-output/
mkdir ../mac-output
mkdir ../mac-output/code


cp ../Exocomets/*.h ../mac-output/code/
cp ../Exocomets/*.cpp ../mac-output/code/
cp -r ../Exocomets/steam ../mac-output/code/
cp libsteam_api.dylib ../mac-output/code/
cp -r ../Release/resources ../mac-output/code/

rm -rf ../../ExocometsMac
mkdir ../../ExocometsMac

open /Applications/Xcode.app
read -p "Create a new Xcode project using Objective-C with the name Exocomets in ~/programming/ExocometsMac"

cd ../../ExocometsMac/Exocomets/Exocomets

cp -r ../../../Exocomets/mac-output/code/* .

rm AppDelegate.*
rm ViewController.*
rm main.m

iconutil -c icns ../../../Exocomets/mac/icon.iconset
mv ../../../Exocomets/mac/icon.icns .

read -p "Now delete all of the old files and include all of the files in the directory ~/programming/ExocometsMac/Exocomets/Exocomets EXCEPT the first two weird folders"

read -p "Go to build something and include the 4 SDL framework files in /Library/Frameworks"
read -p "Then go below it to Copy Bundle Resources and MINUS OUT steam"
read -p "Now go to build settings and set \"C++ Language Dialect\" to C++14 (not gnu)"

read -p "Now go to Info.plist and add \"Icon file\" as \"icon.icns\""
#read -p "Drag and drop icon.icns in 



read -p "Go to Product->Archive and click Export, Export as a macOS App to ~/programming/ExocometsMac/ExocometsApp"

killall Xcode

exo="../../../../Exocomets"

cd ../../ExocometsApp/Exocomets.app/Contents
mkdir Frameworks
cp -r $exo/mac/*.framework Frameworks/
cp -r $exo/mac/*.dylib MacOS/
cp $exo/Release/steam_appid.txt MacOS/


cd ../..
cp -r . ../../Exocomets/mac-output

