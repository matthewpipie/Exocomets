#!/bin/bash

rm -rf ../linux-output/
mkdir ../linux-output

function copy() {
  cp $1 ../linux-output
}

function copyR() {
  cp -r $1 ../linux-output
}

function copyC() {
  cp ../Exocomets/$1 ../linux-output/code/
}

function copyCR() {
  cp -r ../Exocomets/$1 ../linux-output/code/
}

copy libsteam_api.so
copy libSDL2-2.0.so.0
copy libSDL2_ttf-2.0.so.0
copy libSDL2_mixer-2.0.so.0
copy libSDL2_image-2.0.so.0

mkdir ../linux-output/code
cp ../linux-output/* ../linux-output/code/

copyR ../Release/resources
copy ../Release/steam_appid.txt

copyC *.cpp
copyC *.h

copyCR steam

read -p "Ready to build on enter..."

cd ../linux-output/code
g++ `sdl2-config --libs --cflags` *.cpp -o ../Exocomets -std=c++14 -fpermissive -lSDL2main -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer -L. -lsteam_api -Wl,-rpath,. -O3

read -p "Ready to delete code..."

rm -rf ../linux-output/code/
